module types 
  ! Global constants and variables, definitions of the derived data types
  implicit none

  SAVE


  integer option

  ! real kind 
  integer,  parameter :: rk=selected_real_kind(p=15)  ! double prec
  !integer,  parameter :: rk=selected_real_kind(p=7)  ! single prec

  ! parameters
  real(rk),parameter :: pi=3.14159265358979312_rk
  real(rk),parameter :: deg2rad = pi/180.0_rk
  real(rk),parameter :: rad2deg = 1.0_rk/deg2rad
  real(rk),parameter ::  epsilon=10.0_rk**(-15)

  ! static memory allocation parameters (to modify if necessary)
  integer, parameter :: n_spec_max=20,at_per_mol_max=3000,n_mol_max=20000,nb_max=30000


  ! format specifications
  character(20)     :: linefmt="(a80)",resnamefmt="(17x,a4)"
  character(20)     :: idfmt  ="(22x,i5)",coorfmt="(30x,3(f8.3))"
  character(20)     :: atnamefmt="(12x,a4)",elnamefmt="(76x,a2)"
  character(20)     :: occfmt="(54x,f6.2)",tfcfmt="(60x,f6.2)"

  ! intermolecular connectivity (neighbor) list
  integer,allocatable,dimension(:,:)  :: connectivity 
  integer    :: n_conn

  logical    :: pdb_ok=.false.
  logical    :: connectivity_ok=.false.
  logical    :: mask_loaded=.false.
  logical    :: is_pdx=.false.
  logical    :: cell_ok=.false.

! Definition of derived data types

  type sys_str 
     integer  :: n_spec     ! number of chemical spec 
     integer  :: nat_tot    ! total number of atoms
     integer  :: nmol_tot   ! total number of molecules
     integer  :: at_per_mol_max
     character(len=4),dimension(n_spec_max) :: mol_names  ! residue name in pdb
     integer,dimension(n_spec_max) :: nat_spec   ! number of atoms of each molecule
     integer,dimension(n_spec_max) :: nmol_spec  ! number of molecules of each type
     integer,dimension(n_mol_max)  :: spec_idx   ! specie index 
     integer  :: active_pbc(3)
     real(rk) :: box(3,3),cell(6)
     character(len=50)  :: fname_pdb
     character(len=50)  :: fname_root
     integer            :: mask(at_per_mol_max,n_spec_max)
  end type sys_str

  type(sys_str) :: sys


  type mol_str
     integer          :: spec_idx ! specie index (redundant)
     character(len=4) :: name     ! residue name in pdb (redundant)
     integer          :: n_at     ! number of atoms  (redundant)
     real(rk)         :: r_at(at_per_mol_max,3) ! atomic coordinates
     character(len=4),dimension(at_per_mol_max) :: name_at,name_el  ! atom and element name
     integer          :: idx_pdb ! mol idx read from pdb
     real(rk)         :: r_cm(3)
     real(rk)         :: occ(at_per_mol_max),tfc(at_per_mol_max)
     real(rk)         :: q0(at_per_mol_max),V_pc(at_per_mol_max),F_pc(at_per_mol_max,3)
  end type mol_str

  type(mol_str),allocatable,dimension(:) :: mols


end module types


