program MrWolf
  use types
  use io
  implicit none
 
!!!!!!!!!!!!!!!!!!!!!!!!  


call start_banner

option=1000


do while (option.ne.999)

   select case (option) 
   
   case(1)
      call read_pdb
      option=1000

   case(2)
      call system_log
      option=1000

   case(3)
      call write_xyz_frags
      option=1000

   case(4)
      call load_masks
      option=1000

   case(5)
      call write_pdb_frags
      option=1000

   case(6)
      call write_supercell
      option=1000
      
   case(7)
      call cut_selection
      option=1000

   case(8)
      call input_cell
      option=1000

   case(9)
      call embed_cluster_cubic
      option=1000

   case(10)
      call write_connectivity
      option=1000

   case(11)
!      call ctisetup_orca_nopbc
      call ctisetup_orca_pbc
      option=1000

   case(12)
      call gulp_input
      option=1000

   case(13)
      call field_pot_gulp
      option=1000

   case(14)
      call wrap_box
      option=1000

   case(15)
      call rotate_molfrag
      option=1000

   case(16)
      call write_pdb_selection 
      option=1000

   case(17)
      call add_connections 
      option=1000

   case(18)
      call pdb_Tc
      option=1000

   case(19)
      call ctisetup_orca_xyz
      option=1000

   case(20)
      call read_pdx
      option=1000

   case(21)
      call rotate_to_z
      option=1000

   case(22)
      call write_fractional
      option=1000

   case(23)
      call translate_rotate_pdb
      option=1000

   case(24)
      call reorder_cell
      option=1000


!   case(21)
!      call mescal_rst_larger
!      option=1000




! hidden options (too specific)
   case(300)
      call cut_pbttt
      option=1000

   case(301)
      call disk_pen_pfp
      option=1000

   case(302)
      call write_connectivity_mod
      option=1000


   case(1000)
      call ask_option
     
   case default 
        
      write(*,*) '  E/ORROR: this input value is not contemplated!!! '
      call stop_exec

   end select

end do

call stop_banner


contains

subroutine ask_option
  implicit none

write(*,"(80a)") ' '
write(*,"(80a)") '  What should I do, Sir? (cit.)'
write(*,"(80a)") ' '
write(*,"(80a)") '     1. Read pdb file and load data into memory'
write(*,"(80a)") '     2. Write system log files '
write(*,"(80a)") '     3. Write xyz files of molecular units'
write(*,"(80a)") '     4. Load masks to exclude some atoms'
write(*,"(80a)") '     5. Write pdb files of molecular units'
write(*,"(80a)") '     6. Create supercell'
write(*,"(80a)") '     7. Extract selection from pdb file within cutoff'
write(*,"(80a)") '     8. Specify unit cell'
write(*,"(80a)") '     9. Embed cluster in a mesh of polarizable points'
write(*,"(80a)") '    10. Determine intermolecular connectivity'
write(*,"(80a)") '    11. Create input for couplings calculation with ORCA'
write(*,"(80a)") '    12. Create input for GULP calculation of electrostatic sums'
write(*,"(80a)") '    13. Compute fields and potentials form GULP output'
write(*,"(80a)") '    14. Wrap residues in the simulation box'
write(*,"(80a)") '    15. Rotate molecular fragment around a given bond'
write(*,"(80a)") '    16. Extract selected molecules from pdb file'
write(*,"(80a)") '    17. Create vmd-tcl script to modify tpg files'
write(*,"(80a)") '    18. Substitute column in pdb file'
write(*,"(80a)") '    19. Create input for couplings calculation with ORCA (from xyz files)'
write(*,"(80a)") '    20. Read pdx file and load data into memory'
write(*,"(80a)") '    21. Rotate the pdb to bring a given axis parallel to z'
write(*,"(80a)") '    22. Convert to fractional coordinates'
write(*,"(80a)") '    23. Translate and rotate the system and write pdb'
write(*,"(80a)") '    24. Reorder pdb by separating molecules'

!write(*,"(80a)") '    21. Create MESCal restart file for bigger system'

write(*,"(80a)") ' '
write(*,"(80a)") ' 999. EXIT the program'
write(*,"(80a)") ' '

read(*,*)  option
write(*,"(80a)") ' '

end subroutine ask_option


end program MrWolf

!!
