module io
  ! Subroutines for input-output
  use types
  use math
  implicit none

!  save  sys,mols
!  type(sys_str) :: sys
!  type(mol_str),allocatable,dimension(:) :: mols

contains

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


!subroutine mescal_rst_larger
!  implicit none 
!  integer             :: i,j,m
!  character(len=80)   :: fn_rst,fn_rst2,fn_pdb2
!
!write(*,*) 
!write(*,*) ' -------------------------------------------'
!write(*,*) ' You choose option ', option
!write(*,*) ' Create MESCal restart file for bigger system'
!write(*,*) ' -------------------------------------------'
!write(*,*) 
!write(*,*)
!call stop_pdb
!
!
!write(*,*) ' Insert the filename of the restart file of the small system, i.e.'
!write(*,*) ' the one loaded with option 1:'
!read(*,*) fn_rst
!write(*,*) ' Value read: ',fn_rst
!write(*,*) 
!open(file=fn_rst,unit=20,status="old")
!! 1.i_mol 2.i_at 3-5.xyz 6.q_0 7.dq  8-10.mu 11.V 12-14.F 13.V0 14-16.F0
!
!
!write(*,*) ' Insert the filename of the big pdb file:'
!read(*,*) fn_pdb2
!write(*,*) ' Value read: ',fn_pdb2
!write(*,*) 
!
!write(*,*) ' Insert the filename of the restart file for the big system:'
!read(*,*) fn_rst2
!write(*,*) ' Value read: ',fn_rst2
!write(*,*) 
!
!end subroutine mescal_rst_larger


subroutine  reorder_cell
! GD 31/05/16
  implicit none 
  integer  :: i,j,k,ki,kj,Nat,mol_index,ii,iter
  real(rk) :: r0(3),r1(3),r2(3),t(3),r_test(3),r_max,tv(3),cc(3),small
  logical  :: found,changed
  character(len=4)    :: molid
  character(len=50)   :: fn,fn_out
  character(len=80)   :: line
  integer,allocatable,dimension(:)   :: gr_size
  integer,allocatable,dimension(:,:) :: gr_idx
  real(rk),allocatable,dimension(:,:) :: coor_new


write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Reorder pdb with crystalline cell '
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*)
call stop_pdb
call stop_pbc

! checks
if (sys%nmol_tot.ne.1) then
   write(*,*) 'E/ORROR: only one residue should be present in the pdb!'
   call stop_exec
endif
Nat=mols(1)%n_at

allocate(gr_size(Nat))
allocate(gr_idx(Nat,Nat))
allocate(coor_new(Nat,3))


write(*,*) 'Insert the maximum bond length:'
read(*,*)  r_max
write(*,*) 'Value read: ',r_max
write(*,*)


! initialize: one atom per group
gr_size=0
gr_idx=0  
!found0=.false.
coor_new=0.0_rk
do i=1,Nat
   coor_new(i,:)=mols(1)%r_at(i,:)
   gr_size(i)=1
   gr_idx(i,1)=i
enddo

write(*,*) ' ITER 0'
do ii=1,Nat
!   write(99,"(i3,a2,50(i3,1x))") gr_size(i),' :',gr_idx(i,1:10)
   if (gr_size(ii).gt.0)    write(*,"(i5,a2,50(i5,1x))") gr_size(ii),' :',gr_idx(ii,1:20)
enddo
write(*,*) 
write(*,*) 


! loop until no more groups are found
changed=.true.
iter=0
do while (changed)
   iter=iter+1
   changed=.false.

   do i=1,Nat
   if (gr_size(i).ne.0) then ! loop on the groups (to enlarge)

      do j=i+1,Nat
      if (gr_size(j).ne.0) then ! loop on the groups (possibly to join to others)

         ! search for bound elements in groups i and j 
         do kj=1,gr_size(j)
            r_test=coor_new(gr_idx(j,kj),:)

            do ki=1,gr_size(i)
               call bondtest(coor_new(gr_idx(i,ki),:),r_test,found,r_max)
            
               if (found) then
                  tv=r_test-coor_new(gr_idx(j,kj),:)
                  changed=.true.

                  ! join groups j and i
                  do k=1,gr_size(j)
                     gr_size(i)=gr_size(i)+1
                     gr_idx(i,gr_size(i))=gr_idx(j,k)
                     coor_new(gr_idx(j,k),:)=coor_new(gr_idx(j,k),:) +tv
                  enddo
               
                  ! then remove group j and exit
                  gr_size(j)=0
                  gr_idx(j,:)=0
                  exit
                  exit
               endif

            enddo

         enddo

      endif
      enddo

   endif
   enddo

   write(*,*) ' ITER ',iter
   write(*,*)
   do ii=1,Nat
!      if (gr_size(ii).gt.0)  write(*,"(i3,a2,50(i3,1x))") gr_size(ii),' :',gr_idx(ii,1:gr_size(ii))
      if (gr_size(ii).gt.0) then
         write(*,"(a12,i5)") 'Group size: ',gr_size(ii)
         write(*,*) 'Atoms in:'
         write(*,"(20(i5,1x))") gr_idx(ii,1:gr_size(ii))
         write(*,*)      
      endif

   enddo
   write(*,*) 
   write(*,*)

enddo


! write output
write(*,*) ' Insert the filename of the output pdb:' 
read(*,*) fn_out
write(*,*) ' Value read: ',fn_out
write(*,*) 
open(file=fn_out,unit=10,status="replace")


mol_index=0
do i=1,Nat
if (gr_size(i).ne.0) then ! loop on the groups found
   mol_index=mol_index+1

   ! molecule in the box
   cc=0.0_rk 
   do k=1,gr_size(i)
      ki=gr_idx(i,k)
      cc=cc + coor_new(ki,:)
   enddo
   cc=cc/gr_size(i)
   cc=cart2frac(cc) 


!   useful??
!   if ( norm3(sys%box(1,:)).ne.sys%box(1,1)) cc=1.0_rk 
!   if ( (norm3(sys%box(1,:)) -sys%box(1,1)).gt.1e-4 )  cc=1.0_rk       
!   do k=1,3
!      if ( isnan(cc(k)) ) cc(k)=0.0_rk
!   enddo

!   tv=floor(cc(1))* sys%box(1,:) + floor(cc(2))* sys%box(2,:) + &
!      floor(cc(3))* sys%box(3,:) 
!   write(*,*) 'cc',cc

   small=1.0e-6
   tv= nint(cc(1)+small)* sys%box(1,:) + nint(cc(2)+small)* sys%box(2,:) + &
       nint(cc(3)+small)* sys%box(3,:) 


   write(molid,"(i0.4)") mol_index
   write(*,*) ' Found molecule ',mol_index,' with ',gr_size(i),' atoms'

   fn="mol"//molid//'.xyz'
   open(file=fn,unit=77,status="replace")
   write(77,*) gr_size(i)
   write(77,*) ' test'

   fn="mol"//molid//'.info'
   open(file=fn,unit=78,status="replace")

   do k=1,gr_size(i)
      ki=gr_idx(i,k)
      write(88,*) ki, mols(1)%name_el(ki)
      write(77,"(a3,3(f14.5))") mols(1)%name_el(ki),coor_new(ki,:)-tv
!      write(77,"(a3,3(f14.5))") 'C' ,coor_new(ki,:)-tv

      write(78,"(i5,1x,3(f16.6))") ki,coor_new(ki,:)-tv - mols(1)%r_at(ki,:) 

      line=''
      write(line(1:6),"(a6)") "ATOM  "
      write(line(7:11),"(i5)") k
      write(line(13:16),"(a4)") mols(1)%name_el(ki) ! ioppp
      write(line(18:20),"(a3)") 'UNK' !mols(1)%name(1:3)
      write(line(23:26),"(i4)") mol_index
      write(line(31:54),"(3(f8.3))") coor_new(ki,:)-tv
      write(line(55:60),"(f6.2)") mols(1)%occ(ki)
      write(line(61:66),"(f6.2)") mols(1)%tfc(ki)
      write(line(77:78),"(a2)")  adjustr(trim(mols(1)%name_el(ki)))
      write(10,"(a80)") line

   enddo

   close(77)
   close(78)
endif
enddo

close(10)
write(*,*)
write(*,*) " pdb with splitted molecules wrote to ",adjustl(fn_out)
write(*,*)


contains

subroutine bondtest(r1,r2,res,l)
! This subroutine tests if r2 is bond to r1, accounting for PBC.
! If the test is successful, res is set to true and r2 gives in 
! output the new (wrapped) coordinates of the tested atom.
  implicit none
  logical  :: res
  real(rk) :: r1(3),r2(3),r_test(3),t(3),l
  integer  :: i1, i2,i3


res=.false.

do i1=-2,2
do i2=-2,2
do i3=-2,2
         
   t=i1*sys%box(1,:) + i2*sys%box(2,:) + i3*sys%box(3,:)
   r_test=t+r2 ! candidate atom

   if (norm3(r1-r_test).le.l) then
      res=.true.
      r2=r_test
      exit
      exit
      exit
   endif

enddo
enddo
enddo

end subroutine bondtest


end subroutine reorder_cell



! ! failed, too hard!
! ! Needs for a routine to find the connected componets of the associated graph
! subroutine  reorder_cell
! ! GD 24/05/16
!   implicit none 
!   integer  :: i,i_mol,i_at,i1,i2,i3,j,n_found,n_mol,idx_mol(20),nat_mol(20)
!   integer  :: idx_at(200,20),i1_at(200,20),i2_at(200,20),i3_at(200,20)
!   logical  :: already_found
!   real(rk) :: r0(3),r1(3),r2(3),t(3),tol
! !  integer,allocatable,dimension(:) :: idx_mol
! 
! write(*,*) 
! write(*,*) ' -------------------------------------------'
! write(*,*) ' You choose option ', option
! write(*,*) ' Reorder pdb with crystalline cell '
! write(*,*) ' -------------------------------------------'
! write(*,*) 
! write(*,*)
! call stop_pdb
! call stop_pbc
! 
! ! checks
! if (sys%nmol_tot.ne.1) then
!    write(*,*) 'E/ORROR: only one residue should be present in the pdb!'
!    call stop_exec
! endif
! 
! idx_mol=0 
! idx_at=0 
! nat_mol=0 
! i1_at=0 
! i2_at=0 
! i3_at=0 
! 
! tol=2.80_rk
! 
! ! the 1st atom belongs to the 1st molecule 
! n_mol=1
! idx_mol(1)=1
! nat_mol(1)=1
! idx_at(1,1)=1
! write(*,*)
! write(*,*) ' Atom',1,' in molecule ',1
! 
! !n_found=1
! !open(77+) 
! 
! 
! !mols(1)%r_at(idx_mol(i_mol),:)
! 
! 
! do i=2,mols(1)%n_at ! candidate atom
!    r0=mols(1)%r_at(i,:)
! 
!    already_found=.false.
!    do i_mol=1,n_mol
! 
!       do i_at=1,nat_mol(i_mol)
! 
!          ! tested atoms & molecule
!          r2=mols(1)%r_at(idx_at(i_at,i_mol),:) + i1_at(i_at,i_mol)*sys%box(1,:)  &
!               + i2_at(i_at,i_mol)*sys%box(2,:)  + i3_at(i_at,i_mol)*sys%box(3,:) 
! 
!          do i1=-1,1
!          do i2=-1,1
!          do i3=-1,1
!                   
!             t=i1*sys%box(1,:) + i2*sys%box(2,:) + i3*sys%box(3,:)
!             r1=t+r0 ! candidate atom
! 
!             if (norm3(r1-r2).le.tol) then
!                already_found=.true.
!                idx_mol(i)=i_mol
!                nat_mol(i_mol)=nat_mol(i_mol)+1
!                idx_at(nat_mol(i_mol),i_mol)=i
!                i1_at(nat_mol(i_mol),i_mol)=i1
!                i2_at(nat_mol(i_mol),i_mol)=i2
!                i3_at(nat_mol(i_mol),i_mol)=i3
!                exit
!                exit
!                exit
!                exit
!                exit
!             endif
! 
!          enddo
!          enddo
!          enddo
!       enddo
! 
!    enddo
! 
!    if (.not.already_found) then
!       n_mol=n_mol+1
!       i_mol=n_mol
!       idx_mol(i)=i_mol
!       nat_mol(i_mol)=nat_mol(i_mol)+1
!       idx_at(nat_mol(i_mol),i_mol)=i
!       i1_at(nat_mol(i_mol),i_mol)=0
!       i2_at(nat_mol(i_mol),i_mol)=0
!       i3_at(nat_mol(i_mol),i_mol)=0
!    endif
! 
!    write(*,*) ' Atom',i,' in molecule ',i_mol
!    
! 
! enddo
! write(*,*)
! 
! end subroutine reorder_cell

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine  write_fractional
! GD 14/03/16
  implicit none 
  integer  :: i,j
  real(rk) :: coor_frac(3),r0(3)
  character(len=50)   :: fn_frac

write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Convert pdb to fractional coordinates'
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*)
call stop_pdb
call stop_pbc

write(*,*) 'Insert the name of the output file:' 
read(*,*)  fn_frac
write(*,*) 'Value read: ',trim(adjustl(fn_frac))
write(*,*) 
open(file=fn_frac,unit=10,status="replace")

r0=0.0_rk
do j=1,mols(1)%n_at
   r0= r0 + mols(1)%r_at(j,:)
enddo
r0= r0 / mols(1)%n_at
!write(*,*) r0

do i=1,sys%nmol_tot
!   is=mols(i)%spec_idx

   do j=1,mols(i)%n_at
      
      coor_frac=cart2frac(mols(i)%r_at(j,:)-r0)
      write(10,"(a3,2x,3(f10.4))") mols(i)%name_el(j),coor_frac
         

   enddo
enddo

close(10)



end subroutine write_fractional

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine rotate_to_z  
! GD 19/02/19 
! This routine rotate a given structure bringing a given axis parallel to z
  implicit none 
  character(len=50)   :: fn_pdb
  real(rk)            :: chi(3),kappa(3),axis(3),theta,U(3,3)

write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Rotate the pdb coordinates bringing a given axis parallel to z'
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*)
call stop_pdb

write(*,*) 'Insert the name of the output pdb file:' 
read(*,*)  fn_pdb
write(*,*) 'Value read: ',trim(adjustl(fn_pdb))
write(*,*) 
open(file=fn_pdb,unit=10,status="replace")

write(*,*) ' Insert the vector you want to rotate along z:'
read(*,*)  chi
write(*,*) 'Values read: ',chi
write(*,*) ' '
chi=chi/norm3(chi)

kappa=0.0_rk
kappa(3)=1.0_rk

theta=acos(DotProd3(chi,kappa))
axis=VectorProd3(chi,kappa)
U=rotmat3D(theta,axis)

call write_pdb_rot(10,U)

close(10)
write(*,*) " Rotated pdb wrote to ",adjustl(fn_pdb)
write(*,*)


end subroutine rotate_to_z

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine ctisetup_orca_xyz
  implicit none 
  integer             :: i,j,stat,i_col,n1,n2,bau(4)
  integer             :: idx_m1,idx_m2,n_a,n_b,n_c
  character(len=80)   :: fn_log,keywords,fn_xyz1,fn_xyz2,fn_out
  character(len=3)    :: ele
  character(len=5)    :: molid1,molid2
  real(rk)            :: d_min,d_max,gnu,d,tv(3),xyz(3)
  logical             :: write_xyz_dim,write_MO_S


write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Create input for couplings calculation with ORCA '
write(*,*) ' from xyz files (coordinates) and system logs     '
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*)

write(*,*) ' Insert the filename root of the system:'
read(*,*) sys%fname_root
write(*,*) ' Value read: ',sys%fname_root
write(*,*) 

! retrieve info from system log
fn_log=trim(sys%fname_root)//"_sys.log"
open(file=fn_log,unit=20,status="old")
read(20,*)
read(20,*)  sys%nmol_tot,sys%nat_tot !,sys%n_spec
read(20,*)  
read(20,*) 
read(20,*) sys%cell
read(20,*) 
read(20,*) 
read(20,*) sys%box(1,:)
read(20,*) sys%box(2,:)
read(20,*) sys%box(3,:)
read(20,*)
close(20)


write(*,*) ' Insert the keyword line ' 
write(*,*) '   Examples: '
write(*,*) '   B3LYP  6-31G*  RIJCOSX  LOOSESCF '
write(*,*) '   ZINDO/S '
read(*,"(a80)")  keywords
write(*,*) ' Value read: ',trim(adjustl(keywords))
write(*,*) 


write(*,*) ' ORCA inputs will be written only for dimers with closest'
write(*,*) ' contact between d_min and d_max.'
write(*,*) ' Specify values for d_min and d_max:'
read(*,*) d_min,d_max
write(*,*) ' Values read: ',d_min,d_max
write(*,*) 

write(*,*) ' Would you like to write also dimers xyz? T/F'
read(*,*) write_xyz_dim
write(*,*) ' Value read: ', write_xyz_dim
write(*,*) 

write(*,*) ' Print MO coefficients and S matrix in the ORCA log?'
read(*,*) write_MO_S
write(*,*) ' Value read: ', write_MO_S
write(*,*) 


call system ("mkdir -p _cti_orca")
if (write_xyz_dim) call system ("mkdir -p _xyz_dim")


!  SINGLE FRAGMENTS
open(file="_cti_orca/frag_list.txt",unit=11,status="replace")


do i=1,sys%nmol_tot 

   write(molid1,"(i0.5)") i
   write(11,"(a)") molid1

   fn_out="_cti_orca/"//molid1//'.inp'
   open(file=fn_out,unit=10,status="replace")
   
   fn_xyz1="_xyz_frag/"//molid1//'.xyz'
   open(file=fn_xyz1,unit=21,status="old")
   read(21,*) n1
   read(21,*)

   ! header
   write(10,"(a2,a)") '! ',trim(adjustl(keywords))
   if (write_MO_S) then   
      write(10,"(a)") '%output'
      write(10,"(a)") ' Print[P_MOs] 1'
      write(10,"(a)") ' Print[P_Overlap] 1'
      write(10,"(a)") 'end'
   endif
   write(10,"(a)") '* xyz 0 1'
   do j=1,n1
      read(21,*) ele,xyz 
      write(10,"(a,3x,3(f10.4))") ele,xyz 
   enddo
   write(10,"(a)") '*'

   close(21)
   close(10)

enddo

close(11)
write(*,*) "List of fragment jobs wrote in _cti_orca/frag_list.txt"
write(*,*) 



!     DIMERS

! retrieve  connectivity
fn_log=trim(sys%fname_root)//"_con.log"
open(file=fn_log,unit=20,status="old",iostat=stat)
! % 1.idx_1  2.idx_2  3.rep_1  4.rep_2  5.rep_3  6.specie_1  7.specie_2'
! %  8.resid_pdb_1  9.resid_pdb_2  10.dist_cm  11.dist_min'

open(file="_cti_orca/dimer_list.txt",unit=11,status="replace")

read(20,*,iostat=stat)
read(20,*,iostat=stat)
read(20,*,iostat=stat) idx_m1,idx_m2,n_a,n_b,n_c,bau,gnu,d

do while(stat==0)
   if ( (d.ge.d_min).and.(d.le.d_max) ) then
   
      write(molid1,"(i0.5)") idx_m1
      write(molid2,"(i0.5)") idx_m2
      write(11,"(a)") molid1//'_'//molid2

      fn_out="_cti_orca/"//molid1//'_'//molid2//'.inp'
      open(file=fn_out,unit=10,status="replace")

      if (write_xyz_dim) then
         fn_out="_xyz_dim/"//molid1//'_'//molid2//'.xyz'
         open(file=fn_out,unit=12,status="replace")
      endif
   
      fn_xyz1="_xyz_frag/"//molid1//'.xyz'
      open(file=fn_xyz1,unit=21,status="old")
      read(21,*) n1
      read(21,*)
   
      fn_xyz2="_xyz_frag/"//molid2//'.xyz'
      open(file=fn_xyz2,unit=22,status="old")
      read(22,*) n2
      read(22,*)

      ! header
      write(10,"(a2,a)") '! ',trim(adjustl(keywords))
      if (write_MO_S) then   
         write(10,"(a)") '%output'
         write(10,"(a)") ' Print[P_MOs] 1'
         write(10,"(a)") ' Print[P_Overlap] 1'
         write(10,"(a)") 'end'
      endif
      write(10,"(a)") '* xyz 0 1'

      if (write_xyz_dim) write(12,*) n1+n2
      if (write_xyz_dim) write(12,*) 

      !coordinates
      do j=1,n1
         read(21,*) ele,xyz 
         write(10,"(a,3x,3(f10.4))") ele,xyz 
         if (write_xyz_dim)  write(12,"(a,3x,3(f10.4))") ele,xyz 
      enddo

      tv=n_a*sys%box(1,:) + n_b*sys%box(2,:) + n_c*sys%box(3,:) 
      do j=1,n2
         read(22,*) ele,xyz 
         write(10,"(a,3x,3(f10.4))") ele,xyz+tv
         if (write_xyz_dim)  write(12,"(a,3x,3(f10.4))") ele,xyz+tv 
      enddo

      write(10,"(a)") '*'
      close(10)
      if (write_xyz_dim) close(12)
      close(21)
      close(22)

   endif

   read(20,*,iostat=stat) idx_m1,idx_m2,n_a,n_b,n_c,bau,gnu,d
enddo

close(11)
write(*,*) "List of dimer jobs wrote in _cti_orca/dimer_list.txt"
write(*,*)


end subroutine ctisetup_orca_xyz


subroutine pdb_Tc
  implicit none 
  integer             :: i,j,is,i_col
  character(len=80)   :: fn_out,line
  real(rk)            :: newval 

write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Substitute column in pdb file. '
write(*,*) ' Generally used to create T-coupling file for namd.'
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*)
call stop_pdb

write(*,*) ' Which column do you want to substitute? '
write(*,*) ' 1=X, 2=Y, 3=Z, 4=Occ, 5=T-factor'
read(*,*)  i_col
write(*,*) ' Value read: ',i_col
write(*,*)
write(*,*) ' Insert the value you want to substitute:' 
read(*,*)  newval
write(*,*) ' Value read: ',newval
write(*,*)
write(*,*) ' Insert the filename for the output pdb:' 
read(*,*) fn_out
write(*,*) ' Value read: ',fn_out
write(*,*) 
open(file=fn_out,unit=10,status="replace")
  

do i=1,sys%nmol_tot 

   is=mols(i)%spec_idx

   do j=1,mols(i)%n_at
      
      if (sys%mask(j,is).eq.1) then
                  

!COLUMNS        DATA  TYPE    FIELD        DEFINITION
!-------------------------------------------------------------------------------------
! 1 -  6        Record name   "ATOM  "
! 7 - 11        Integer       serial       Atom  serial number.
!13 - 16        Atom          name         Atom name.
!18 - 20        Residue name  resName      Residue name.
!23 - 26        Integer       resSeq       Residue sequence number.
!31 - 38        Real(8.3)     x            Orthogonal coordinates for X in Angstroms.
!39 - 46        Real(8.3)     y            Orthogonal coordinates for Y in Angstroms.
!47 - 54        Real(8.3)     z            Orthogonal coordinates for Z in Angstroms.
!55 - 60        Real(6.2)       Occupancy.                            
!61 - 66        Real(6.2)       Temperature factor (Default = 0.0).                   
!77 - 78        LString(2)    element      Element symbol, right-justified.

         line=''   
         write(line(1:6),"(a6)") "ATOM  "
         write(line(7:11),"(i5)") j
         write(line(13:16),"(a4)")  mols(i)%name_at(j)
         write(line(18:20),"(a3)")  mols(i)%name(1:3)
         write(line(23:26),"(i4)") i
         write(line(31:54),"(3(f8.3))") mols(i)%r_at(j,:)
         write(line(55:60),"(f6.2)") mols(i)%occ(j)
         write(line(61:66),"(f6.2)") mols(i)%tfc(j)
         write(line(77:78),"(a2)")  adjustr(trim(mols(i)%name_el(j)))

         
         select case (i_col)
           case(1)
              write(line(31:38),"(f8.3)") newval
           case(2)
              write(line(39:46),"(f8.3)") newval
           case(3)
              write(line(47:54),"(f8.3)") newval
           case(4)
              write(line(55:60),"(f6.2)") newval
           case(5)
              write(line(61:66),"(f6.2)") newval
         end select

         write(10,"(a80)") line

      endif

   enddo

enddo

close(10)
write(*,*) " column-substituted pdb wrote to ",adjustl(fn_out)
write(*,*)


end subroutine pdb_Tc




subroutine add_connections 
  implicit none 
  integer      :: i,j,k,is,i1,i2,nat1,nat2,resid1,resid2
  real(rk)     :: r0,r1(3),r2(3),tv(3)

write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Create vmd-tcl script to modify tpg files ' 
write(*,*) ' to connect atom pairs. '
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*) 
call stop_pdb



write(*,*) ' Insert the index of the two atoms: '
read(*,*)  nat1,nat2
write(*,*) 'Values read: ',nat1,nat2
write(*,*) ' '
write(*,*) ' Insert the maximum bonding distance: '
read(*,*)  r0
write(*,*) 'Value read: ',r0
write(*,*) ' '
write(*,*) ' Insert tranlation vector (if you want to bond across PBC, otherwise 0 0 0): '
read(*,*)  tv(1),tv(2),tv(3)
write(*,*) 'Values read: ',tv
write(*,*) ' '

open(file='add_bonds.tcl',unit=10,status="replace")

k=0
do i=1,sys%nmol_tot 
   is=mols(i)%spec_idx

   r1=mols(i)%r_at(nat1,:)
   resid1=(i-1)*mols(i)%n_at + nat1 -1

   do j=1,sys%nmol_tot
   if (i.ne.j) then

      r2=mols(j)%r_at(nat2,:)

      if ( (norm3(r2-r1).le.r0) .or. (norm3(r2-r1-tv).le.r0) .or. (norm3(r1-r2-tv).le.r0)) then 

         resid2=(j-1)*mols(j)%n_at + nat2  -1

         if (resid1.lt.resid2) then
            write(10,"(a15,i6,i6,a15)") 'topo addbond ',resid1,resid2,' -molid top'            
         else
            write(10,"(a15,i6,i6,a15)") 'topo addbond ',resid2,resid1,' -molid top'
         endif

         exit
      endif

   endif
   enddo

enddo

write(10,"(a30)") "topo guessangles -molid top"
write(10,"(a30)") "topo guessdihedrals -molid top"
write(10,"(a30)") "animate write psf out.psf"

close(10)
write(*,*) " Tcl script wrote to file add_bonds.tcl "
write(*,*)

end subroutine add_connections



subroutine write_pdb_selection  
  implicit none 
  integer             :: i,j,k,is,rl,stat,n_sel,resid
  integer,allocatable,dimension(:) :: sel
  character(len=50)   :: fn_pdb,fn_list,fn_sel
  character(len=80)   :: line
  logical             :: is_in

write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Write selected molecules to pdb file '
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*) 
call stop_pdb

write(*,*) ' WARNING: if one or more atom-masks are loaded via option 4  ' 
write(*,*) '          these will be used to determine the atoms to print!'  
write(*,*) 

write(*,*) 'Insert the name of the output pdb file:' 
read(*,*)  fn_pdb
write(*,*) 'Value read: ',trim(adjustl(fn_pdb))
write(*,*) 
open(file=fn_pdb,unit=10,status="replace")

write(*,*) ' Insert the name of the file with the list of selected molecules.'
write(*,*) ' The accepted file format is one molecular index per row! '
read(*,*)  fn_list
write(*,*) 'Value read: ',trim(adjustl(fn_list))
write(*,*) ' '
open(file=fn_list,unit=17,status="old")


rl=len(trim(adjustl(fn_pdb)))
fn_sel=fn_pdb(1:rl-4)//"_selection.log"
open(file=fn_sel,unit=11,status="replace")
write(11,"(a)") '% 1.idx_new  2.idx_old  3.idx_old_pdb  4.idx_specie  5-7.xyz_cm'


n_sel=0
do while(stat==0)
   line=""
   read(17,*,iostat=stat) j
   if (stat==0) n_sel=n_sel+1
enddo

allocate(sel(n_sel))
rewind(17)
do i=1,n_sel
   read(17,*) sel(i)
enddo
close(17)


resid=0
do i=1,sys%nmol_tot
   is=mols(i)%spec_idx

   is_in=.false.
   do k=1,n_sel
      if (mols(i)%idx_pdb.eq.sel(k)) then
         resid=resid+1
         is_in=.true.
         exit
      endif
   enddo

   if (is_in) then 

      write(11,"(4(i5,1x),3(f10.4,1x))") resid,i,mols(i)%idx_pdb,mols(i)%spec_idx,mols(i)%r_cm

      do j=1,mols(i)%n_at
      
         if (sys%mask(j,is).eq.1) then
                  
            line=''   
            write(line(1:6),"(a6)") "ATOM  "
            write(line(7:11),"(i5)") j
            write(line(13:16),"(a4)")  mols(i)%name_at(j)
            write(line(18:20),"(a3)")  mols(i)%name(1:3)
            write(line(23:26),"(i4)") resid
            write(line(31:54),"(3(f8.3))") mols(i)%r_at(j,:)
            write(line(77:78),"(a2)")  adjustr(trim(mols(i)%name_el(j)))
            write(10,"(a80)") line

         endif

      enddo
   endif

enddo

close(10)
write(*,*) " Selected molecules wrote to file ",trim(adjustl(fn_pdb))
write(*,*)
close(11)
write(*,*) " List of selected molecules  wrote to ",trim(adjustl(fn_sel))
write(*,*)

end subroutine write_pdb_selection




subroutine rotate_molfrag
  implicit none 
  integer  :: i,idx1,idx2
  real(rk) :: axis(3),u,v,w,u2,v2,w2,c,s,R(3,3),tv(3),coor(3),angle
  character(len=80) :: line,fn_pdb

write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Rotate molecular fragment around a given bond.'
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*) 
call stop_pdb

write(*,*) 'INFO: the selection of atoms to be rotated is done with a mask'
write(*,*) 

! some checks
if (sys%nmol_tot.gt.1) then
   write(*,*) 'E/ORROR: this option works for one molecule only!'
   call stop_exec
elseif (.not.mask_loaded) then
   write(*,*) 'E/ORROR: masks for atoms must be loaded before!'
   call stop_exec
endif


write(*,*) 'Insert the followings: ' 
write(*,*) ' -index of atoms 1 of the bond' 
write(*,*) ' -index of atoms 2 of the bond (on the side of the rotated moiety)' 
write(*,*) ' -rotation angle (degrees)'
read(*,*)  idx1,idx2,angle
write(*,*) 'Values read: ',idx1,idx2,angle
write(*,*) 

write(*,*) 'Insert the name of the output pdb file:' 
read(*,*)  fn_pdb
write(*,*) 'Value read: ',trim(adjustl(fn_pdb))
write(*,*) 
open(file=fn_pdb,unit=10,status="replace")

! building rotation matrix and translation vector 
axis=mols(1)%r_at(idx2,:)-mols(1)%r_at(idx1,:)
axis=axis/norm3(axis)
u=axis(1)
v=axis(2)
w=axis(3)
u2=u**2
v2=v**2
w2=w**2
c=cos(deg2rad*angle)
s=sin(deg2rad*angle)

write(*,*) '--------uno'

R=0.0_rk
R(1,1) =  u2 + (v2 + w2)*c;
R(1,2) = u*v*(1-c) - w*s;
R(1,3) = u*w*(1-c) + v*s;
R(2,1) = u*v*(1-c) + w*s;
R(2,2) = v2 + (u2+w2)*c;
R(2,3) = v*w*(1-c) - u*s;
R(3,1) = u*w*(1-c) - v*s;
R(3,2) = v*w*(1-c)+u*s;
R(3,3) = w2 + (u2+v2)*c;

tv=mols(1)%r_at(idx2,:)

write(*,*) '--------due'

! rewrite pdb changing coordinates when necessary
do i=1,mols(1)%n_at
   write(*,*) '-------- tre',i
   coor=mols(1)%r_at(i,:)
   if (sys%mask(i,1).eq.1) then
      coor= tv + matmul(R,(coor-tv))
   endif


   line=''   
   write(line(1:6),"(a6)") "ATOM  "
   write(line(7:11),"(i5)") i
   write(line(13:16),"(a4)")  mols(1)%name_at(i)
   write(line(18:20),"(a3)")  mols(1)%name(1:3)
   write(line(23:26),"(i4)") 1
   write(line(31:54),"(3(f8.3))") coor
   write(line(55:60),"(f6.2)") mols(1)%occ(i)
   write(line(61:66),"(f6.2)") mols(1)%tfc(i)
   write(line(77:78),"(a2)")  adjustr(trim(mols(1)%name_el(i)))
   write(10,"(a80)") line

enddo



close(10)
write(*,*) " Rotated molecule wrote to file ",trim(adjustl(fn_pdb))
write(*,*)




end subroutine rotate_molfrag


subroutine wrap_box
  implicit none 
  integer  :: i,j,is
  real(rk) :: t(3),r(3),a_prj,b_prj,c_prj,coor_frac(3)
  character(len=80) :: line,fn_pdb

write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Rewrite pdb wrapping whole residues within the simulation box.'
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*) 
call stop_pdb
call check_box

write(*,*) 'Insert the name of the output pdb file:' 
read(*,*)  fn_pdb
write(*,*) 'Value read: ',trim(adjustl(fn_pdb))
write(*,*) 
open(file=fn_pdb,unit=10,status="replace")

call write_pdb_cryst1(10) 

write(*,*) norm3(sys%box(1,:)),norm3(sys%box(2,:)),norm3(sys%box(3,:))
write(*,*) sys%cell(1:3)

do i=1,sys%nmol_tot

   is=mols(i)%spec_idx

   ! molecular translation vector
   r=mols(i)%r_cm
   coor_frac=cart2frac(r)
   t=floor(coor_frac(1))*sys%box(1,:) + &
     floor(coor_frac(2))*sys%box(2,:) + &
     floor(coor_frac(3))*sys%box(3,:) 


   do j=1,mols(i)%n_at
      
      if (sys%mask(j,is).eq.1) then

         line=''   
         write(line(1:6),"(a6)") "ATOM  "
         write(line(7:11),"(i5)") j
         write(line(13:16),"(a4)")  mols(i)%name_at(j)
         write(line(18:20),"(a3)")  mols(i)%name(1:3)
         write(line(23:26),"(i4)") i
         write(line(31:54),"(3(f8.3))") mols(i)%r_at(j,:) - t
         write(line(55:60),"(f6.2)") mols(i)%occ(j)
         write(line(61:66),"(f6.2)") mols(i)%tfc(j)
         write(line(77:78),"(a2)") adjustr(trim(mols(i)%name_el(j)))
         write(10,"(a80)") line

      endif

   enddo

enddo

close(10)
write(*,*) " Wrapped pdb wrote to file ",trim(adjustl(fn_pdb))
write(*,*)


end subroutine wrap_box



subroutine field_pot_gulp
  implicit none 
  integer  :: i,j,k,l
  character(len=50)   :: fn_out,fn_gulp,fn_chg,s1,s2,s3,n_str,np2_str
  character(len=120)  :: line,parse_cmd
  logical  :: skip_line
  real(rk) :: V(sys%nat_tot),F(sys%nat_tot,3),V_intra,F_intra(3)
  real(rk) :: chg(sys%nat_tot),q_tot,r_jl(3),r_jl_norm


write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Compute electrostatic fields and potentials at atomic  '
write(*,*) ' sites removing intramolecular Coulomb interactions from'
write(*,*) ' GULP output '
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*) 
call stop_pdb

write(*,*) 'Insert the name of GULP output file' 
read(*,*)  fn_gulp
write(*,*) 'Value read: ',fn_gulp
write(*,*) 

!  parse GULP output and load data
write(n_str,"(i8)") sys%nat_tot
write(np2_str,"(i8)") sys%nat_tot+2

! charges
parse_cmd="grep -"//trim(adjustl(np2_str))//" -m 1 Charge "//trim(adjustl(fn_gulp))//&
" | tail -"//trim(adjustl(n_str))//" | awk '{print $7}' > q.tmp"
call system(parse_cmd)
open(file="q.tmp",unit=10,status="old")
chg=0.0_rk
q_tot=0.0_rk
do i=1,sys%nat_tot
   read(10,*) chg(i)
   q_tot=q_tot + chg(i)
enddo
close(10)
if (abs(q_tot).gt.1e-3) then
   write(*,*) 'E/ORROR: system net charge is not zero: ',q_tot
   call stop_exec   
elseif (abs(q_tot).gt.1e-6) then
   write(*,*) 'WARNING: system net charge is not zero: ',q_tot
endif


! potentials and fields 
parse_cmd="grep -"//trim(adjustl(np2_str))//" Potential "//trim(adjustl(fn_gulp))//&
" | tail -"//trim(adjustl(n_str))//" | awk '{print $4, $5, $6, $7}' > fv.tmp"
call system(parse_cmd)
open(file="fv.tmp",unit=10,status="old")
V=0.0_rk
F=0.0_rk
do i=1,sys%nat_tot
   read(10,*) V(i),F(i,:)
enddo
close(10)
write(*,*) 'Charges, potentials and fields read from '//trim(adjustl(fn_gulp))


! correct field and potential by subtracting
! intramolecular Coulomb interactions

k=0
do i=1,sys%nmol_tot 
!   is=mols(i)%spec_idx

   do j=1,mols(i)%n_at
      k=k+1
      V_intra=0.0_rk
      F_intra=0.0_rk

      ! Compute intramolecular Coulomb interactions
      do l=1,mols(i)%n_at
      if (j.ne.l) then
         r_jl=mols(i)%r_at(l,:)-mols(i)%r_at(j,:)
         r_jl_norm=norm3(r_jl)
         
         V_intra=V_intra + chg(l)/r_jl_norm
         F_intra= F_intra + chg(l)*r_jl/(r_jl_norm**3)
      endif
      enddo
      
      V(k)=V(k) - 14.39964485*V_intra
      F(k,:)=F(k,:) - 14.39964485*F_intra
      write(17,"(4(f12.5))") V(k),F(k,:)

   enddo
enddo






!write(*,*) ' Insert the filename for the GULP input file output pdb:' 
!read(*,*) fn_gulp
!write(*,*) ' Value read: ',fn_gulp
!write(*,*) 
!open(file=fn_gulp,unit=10,status="replace")



end subroutine field_pot_gulp



subroutine  gulp_input 
  implicit none
  integer  :: i,j,sel,is
  character(len=50)   :: fn_chg,fn_gulp
  logical  :: exists
  real(rk) :: q_tot,chg(at_per_mol_max,sys%n_spec)


write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Create input for GULP calculation of electrostatic fields'
write(*,*) ' and potentials in periodic system '
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*) 
call stop_pdb
call check_box



chg=0.0_rk

write(*,*) 'Loading atomic charges for each specie'
write(*,*)
do i=1,sys%n_spec
   write(*,*) 'Insert charge file for specie '//adjustl(sys%mol_names(i))
   read(*,*) fn_chg

   inquire(file=fn_chg,exist=exists)
   if (.not.exists) then
      write(*,*) 'E/ORROR: file not found!'
      call stop_exec
   endif

   open(file=fn_chg,unit=10,status="old")
   q_tot=0.0_rk
   do j=1,sys%nat_spec(i)
      read(10,*) chg(j,i)
         q_tot=q_tot + chg(j,i)
   enddo
   close(10)
   write(*,"(a,f10.6)") 'Total molecular charge: ',q_tot
   write(*,*)
   write(*,*) 'Charge file '//trim(adjustl(fn_chg))//' read correctly! '
   write(*,*)

enddo

write(*,*) 'Specification the system periodicity.'
write(*,*) ' 2 = 2D peridic system (in the xy plane only!)'
write(*,*) ' 3 = 3D peridic system'
read(*,*) sel 
write(*,*) ' Value read: ',sel
write(*,*) 
if ((sel.lt.2).or.(sel.gt.3) )  call wrong_input 

write(*,*) ' Insert the filename for the GULP input file output pdb:' 
read(*,*) fn_gulp
write(*,*) ' Value read: ',fn_gulp
write(*,*) 
open(file=fn_gulp,unit=10,status="replace")


write(10,"(a)") "single  conv  pot "
write(10,*)

if (sel.eq.3) then

   write(10,"(a)") "cell"
   write(10,"(6(f10.4,1x))") sys%cell
   write(10,*) 

elseif (sel.eq.2) then

   write(10,"(a)") "scell"
   write(10,"(3(f10.4,1x))") sys%cell(1),sys%cell(2),sys%cell(6)
   write(10,*) 

endif

write(10,"(a)") "cartesian"
do i=1,sys%nmol_tot 
   is=mols(i)%spec_idx

   do j=1,mols(i)%n_at

      write(10,"(a4,2x,3(f9.3,2x),f12.6)") mols(i)%name_at(j),mols(i)%r_at(j,:),chg(j,is)
!      write(10,*) mols(i)%name_at(j),mols(i)%r_at(j,:),mols(i)%chg(j)
 
   enddo
enddo

write(10,*) 
close(10)
write(*,*) " GULP input file wrote to ",adjustl(fn_gulp)
write(*,*)

end subroutine gulp_input

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine write_pdb_full(fid)
  implicit none
  character(len=80) :: line
  integer           :: i,j,fid,is
  

do i=1,sys%nmol_tot 

   is=mols(i)%spec_idx

   do j=1,mols(i)%n_at
      
      if (sys%mask(j,is).eq.1) then
                  

!COLUMNS        DATA  TYPE    FIELD        DEFINITION
!-------------------------------------------------------------------------------------
! 1 -  6        Record name   "ATOM  "
! 7 - 11        Integer       serial       Atom  serial number.
!13 - 16        Atom          name         Atom name.
!18 - 20        Residue name  resName      Residue name.
!23 - 26        Integer       resSeq       Residue sequence number.
!31 - 38        Real(8.3)     x            Orthogonal coordinates for X in Angstroms.
!39 - 46        Real(8.3)     y            Orthogonal coordinates for Y in Angstroms.
!47 - 54        Real(8.3)     z            Orthogonal coordinates for Z in Angstroms.
!55 - 60        Real(6.2)       Occupancy.                            
!61 - 66        Real(6.2)       Temperature factor (Default = 0.0).                   
!77 - 78        LString(2)    element      Element symbol, right-justified.

         line=''   
         write(line(1:6),"(a6)") "ATOM  "
         write(line(7:11),"(i5)") j
         write(line(13:16),"(a4)")  mols(i)%name_at(j)
         write(line(18:20),"(a3)")  mols(i)%name(1:3)
         write(line(23:26),"(i4)") i
         write(line(31:54),"(3(f8.3))") mols(i)%r_at(j,:)
         write(line(55:60),"(f6.2)") mols(i)%occ(j)
         write(line(61:66),"(f6.2)") mols(i)%tfc(j)
         write(line(77:78),"(a2)")  adjustr(trim(mols(i)%name_el(j)))
         write(fid,"(a80)") line

      endif

   enddo

enddo


end subroutine write_pdb_full

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine write_pdb_rot(fid,U)
  implicit none
  character(len=80) :: line
  integer           :: i,j,fid,is
  real(rk)          :: r_rot(3),U(3,3)

do i=1,sys%nmol_tot 

   is=mols(i)%spec_idx

   do j=1,mols(i)%n_at
      
      if (sys%mask(j,is).eq.1) then

         r_rot=matmul(U,mols(i)%r_at(j,:))

         line=''
         write(line(1:6),"(a6)") "ATOM  "
         write(line(7:11),"(i5)") j
         write(line(13:16),"(a4)")  mols(i)%name_at(j)
         write(line(18:20),"(a3)")  mols(i)%name(1:3)
         write(line(23:26),"(i4)") i
         write(line(31:54),"(3(f8.3))") r_rot
         write(line(55:60),"(f6.2)") mols(i)%occ(j)
         write(line(61:66),"(f6.2)") mols(i)%tfc(j)
         write(line(77:78),"(a2)")  adjustr(trim(mols(i)%name_el(j)))
         write(10,"(a80)") line

      endif

   enddo

enddo

end subroutine write_pdb_rot

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine embed_cluster_cubic
  implicit none 
  integer             :: i,i_mol,i_at,n_rep,i1,i2,i3,n_dummy,sel_aniso,errorflag
  character(len=80)   :: fn_out,line,rst_fn
  real(rk)            :: Rs,a0,Rcc(3),p(3),d0,kappa(3,3),a_iso,chg,zeros(9)
  real(rk)            :: a_tens(3,3),eye(3,3),kp2i(3,3)
  logical             :: is_in,write_rst

write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Embed the cluster in sphere of dummy atoms'
write(*,*) ' placed on a cubic lattice'
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*) 
call stop_pdb

write(*,*) ' Insert the filename for the output pdb:' 
read(*,*) fn_out
write(*,*) ' Value read: ',fn_out
write(*,*) 
open(file=fn_out,unit=10,status="replace")

write(*,*) ' Insert the values for the sphere radius and the '
write(*,*) ' mesh lattice constant (Angstrom)'
read(*,*)  Rs,a0
write(*,*) ' Values read: ',Rs,a0
write(*,*)
write(*,*) ' Insert the value for the minimum distance between '
write(*,*) ' atoms and grid points (half lattice spacing recommended)'
read(*,*)  d0
write(*,*) ' Value read: ',d0
write(*,*)
write(*,*) ' Do you want to create an auxiliary file for restarting from'
write(*,*) ' a previous atomistic calculation?  T/F'
write(*,*) ' This file should be appended to the atomistic restart.'
read(*,*)  write_rst
write(*,*) ' Value read: ',write_rst
write(*,*)
if (write_rst) then
   write(*,*) ' Insert file name: '
   read(*,*)  rst_fn
   write(*,*) ' Value read: ',rst_fn
   write(*,*)
   open(file=rst_fn,unit=11,status="replace")
endif
zeros=0.0_rk

! write original pdb
call write_pdb_full(10)


! compute the centroid of the cluster
Rcc=0.0_rk
do i_mol=1,sys%nmol_tot
   Rcc=Rcc + mols(i_mol)%r_cm
enddo
Rcc=Rcc/sys%nmol_tot

!write(*,*)'///////////',  Rcc

n_rep=ceiling(1.10_rk*Rs/a0)


! GD15/11/17 Warning: possible issue here!
n_dummy=sys%nmol_tot 

! ioppp

do i3=-n_rep,n_rep
do i2=-n_rep,n_rep
do i1=-n_rep,n_rep

   p=Rcc
   p(1)=p(1) + a0*i1
   p(2)=p(2) + a0*i2
   p(3)=p(3) + a0*i3


   is_in=.false.
   ! check first if the point is within the sphere
   if (norm3(p-Rcc).le.Rs) then
      is_in=.true.

   ! check if the mesh point does not overlap with any atom
      do i_mol=1,sys%nmol_tot
         do i_at=1,mols(i_mol)%n_at         
            if (norm3( p- mols(i_mol)%r_at(i_at,:) ).le.d0) then
               is_in=.false.
               exit
               exit
            endif
         enddo
      enddo

   endif

   if (is_in) then
      n_dummy=n_dummy+1
      if (n_dummy.gt.9999) n_dummy=1

      line=''   
      write(line(1:6),"(a6)") "ATOM  "
      write(line(7:11),"(i5)") sys%nat_tot+n_dummy
      write(line(13:16),"(a4)")  'X'
      write(line(18:20),"(a3)")  'DMY'
      write(line(23:26),"(i4)") n_dummy
      write(line(31:54),"(3(f8.3))") p
      write(10,"(a80)") line 
 
      if (write_rst) then 
         write(11,"(i6,i6,14(1x,e16.8))") n_dummy,1,p,zeros
      endif

   endif

enddo 
enddo
enddo

close(10)
write(*,*) " Embedded cluster wrote to ",adjustl(fn_out)
write(*,*)
if (write_rst) then 
   close(11)
   write(*,*) " Restart appendix wrote to ",adjustl(rst_fn)
   write(*,*)
endif

! create mei for pp for given dielectric constant-spacing 

write(*,*) ' Please select:'
write(*,*) ' 1. Isotropic polarizable medium (dielectric constant)'
write(*,*) ' 2. Anisotropic polarizable medium (dielectric tensor)'
write(*,*) 
read(*,*) sel_aniso
write(*,*) ' Value read: ',sel_aniso
write(*,*) 

kappa=0.0_rk
chg=0.0_rk
if (sel_aniso.eq.1) then
   write(*,*) ' Insert the value for the dielectric constant'
   write(*,*) 
   read(*,*) kappa(1,1)
   write(*,*) ' Value read: ',kappa(1,1)
   write(*,*)

   ! Clausius-Mossotti equation 
   a_iso=((3*a0**3)/(4*pi)) * ((kappa(1,1)-1)/(kappa(1,1)+2))
   
   open(file="dummy.mei",unit=10,status="replace")
   write(10,"(a,f6.3)") "1 1 I I   ! kappa CM:",kappa(1,1)
   write(10,"(i1,1x,i1,1x,f12.6,1x,f12.6)") 1,1,chg,a_iso
   write(*,*) " ME input file for dummy atoms wrote to dummy.mei "
   write(*,*)
   close(10)

elseif (sel_aniso.eq.2) then
   write(*,*) ' Insert the elemntsof the dielectric tensor (one line, upper triangle)'
   write(*,*) 
   read(*,*) kappa(1,1),kappa(1,2),kappa(1,3),kappa(2,2),kappa(2,3),kappa(3,3)
   write(*,*) ' Values read: ',kappa(1,1),kappa(1,2),kappa(1,3),kappa(2,2),kappa(2,3),kappa(3,3)
   write(*,*)
   kappa(2,1)=kappa(1,2)
   kappa(3,1)=kappa(1,3)
   kappa(3,2)=kappa(2,3)

   a_tens=0.0_rk
   eye=0.0_rk
   eye(1,1)=1.0_rk
   eye(2,2)=1.0_rk
   eye(3,3)=1.0_rk
   call InvMat( kappa+2.0_rk*eye , kp2i , 3, errorflag)
   a_tens=((3*a0**3)/(4*pi)) * ( kappa - 1.0_rk*eye ) * kp2i 
   
   
   open(file="dummy.mei",unit=10,status="replace")
   write(10,"(a,f6.3)") "1 1 T T"
   write(10,"(i1,1x,i1,7(1x,f12.6))") 1,1,chg,a_tens(1,:),a_tens(2,2:3),a_tens(3,3)
   write(*,*) " ME input file for dummy atoms wrote to dummy.mei "
   write(*,*)
   close(10)

else

   write(*,*) "E/ORROR: input value not contemplated! "
   call stop_exec
   write(*,*)

endif





end subroutine embed_cluster_cubic

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine load_masks
  implicit none 
  integer             :: i,j,sel,is,nat_sel
  character(len=50)   :: fn_mask,fn_xyz
  logical             :: exists

write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Load masks to exclude some atoms in the determination'
write(*,*) ' of the connectivity or in coordinate output '
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*) 
call stop_pdb

write(*,*) 'For how many chemical species do you want to load a mask?'
write(*,*) '1=YES for 1 chemical specie, 2=YES for 2, etc...'
write(*,*) 
read(*,*) sel
write(*,*) ' Value read: ',sel
write(*,*) 
if ( (sel.lt.1).or.(sel.gt.sys%n_spec) ) call wrong_input

sys%mask=1

do i=1,sel
      
   write(*,*) 'Insert the index of the chemical specie:'
   read(*,*) is
   write(*,*) ' Value read: ',is
   write(*,*) 
   write(*,*) 'Insert mask file for specie '//adjustl(sys%mol_names(is))
   read(*,*) fn_mask
   write(*,*) ' Value read: ',fn_mask
   write(*,*)
      
   inquire(file=fn_mask,exist=exists)
   if (.not.exists) then
      write(*,*) 'E/ORROR: file not found!'
      call stop_exec
   endif

   open(file=fn_mask,unit=10,status="old")
   do j=1,sys%nat_spec(is)
      read(10,*) sys%mask(j,is)
   enddo
   close(10)
   write(*,*) 'Mask file '//trim(adjustl(fn_mask))//' read correctly! '
   write(*,*)

enddo

mask_loaded=.true.

end subroutine load_masks

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine write_xyz_frags
  implicit none 
  integer             :: i,j,k,sel,is,nat_sel 
  character(len=50)   :: fn_xyz
  character(len=5)    :: molid,elename
  logical             :: exists
  logical             :: Hsub,Hadd,is2sub
  integer             :: n_Hsub(sys%n_spec),n_Hadd(sys%n_spec)
  integer             :: Hsub_info(100,sys%n_spec),Hadd_info(100,sys%n_spec)
!  integer,allocatable,dimension(:,:) :: Hsub_info,Hadd_info

  real(rk)            :: P0(3),P1(3),P2(3)
  
write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Write coordinates of each molecular unit in xyz format'
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*) 
call stop_pdb

call system ("mkdir -p _xyz_frag")

write(*,*) ' WARNING: if one or more atom-masks are loaded via option 4  ' 
write(*,*) '          these will be used to determine the atoms to print!'  
write(*,*) 

write(*,*) 'From which pdb record you want to take the element name?'
write(*,*) '0=element symbol (recommended)  1=atom name (1st character) '
read(*,*) sel 
write(*,*) ' Value read: ',sel
write(*,*) 
if ((sel.gt.1).or.(sel.lt.0) )  call wrong_input 

write(*,*) 'Would you like to substitute atoms with H?  T/F'
read(*,*)  Hsub
write(*,*) ' Value read: ',Hsub
write(*,*) 
n_Hsub=0
Hsub_info=0

if (Hsub) then

   do i=1,sys%n_spec
      write(*,*) 'How many atoms should be substituted with H for specie '//trim(sys%mol_names(i))//'?'
      read(*,*)  n_Hsub(i)
      write(*,*) ' Value read: ',n_Hsub(i)
      write(*,*) 

      write(*,*) 'For each of them specify indexes of the atom to substitute '
      write(*,*) 'and of the atom to which it is bond.' 
      do j=1,n_Hsub(i)
         read(*,*)  Hsub_info(2*j-1,i),Hsub_info(2*j,i)
         write(*,*) ' Values read: ',Hsub_info(2*j-1,i),Hsub_info(2*j,i)
         write(*,*)
      enddo

   enddo
endif



write(*,*) 'Would you like to add H atoms?  T/F'
read(*,*)  Hadd
write(*,*) ' Value read: ',Hadd
write(*,*) 
n_Hadd=0
Hadd_info=0


if (Hadd) then

   do i=1,sys%n_spec

      write(*,*) 'How many H atoms do you want to add for specie '//trim(sys%mol_names(i))//'?'
      read(*,*)  n_Hadd(i)
      write(*,*) ' Value read: ',n_Hadd(i)
      write(*,*) 

      write(*,*) 'For each of new H specify the indexes of the 3 atoms.' 
      write(*,*) 'The H will be bond to the 1st of them and will be coplanar'
      write(*,*) 'with the other two.'
      do j=1,n_Hadd(i)
         read(*,*)  Hadd_info(3*j-2,i),Hadd_info(3*j-1,i),Hadd_info(3*j,i)
         write(*,*) ' Values read: ',Hadd_info(3*j-2,i),Hadd_info(3*j-1,i),Hadd_info(3*j,i)
         write(*,*)
      enddo

   enddo

endif




do i=1,sys%nmol_tot

   is=mols(i)%spec_idx

   write(molid,"(i0.5)") i
   fn_xyz="_xyz_frag/"//molid//'.xyz'

   open(file=fn_xyz,unit=10,status="replace")

   nat_sel=0
   do j=1,mols(i)%n_at
      if (sys%mask(j,is).eq.1) nat_sel=nat_sel+1
   enddo
   if (n_Hadd(is).gt.0)  nat_sel=nat_sel+n_Hadd(is)

   write(10,"(i4)") nat_sel 
 
   write(10,("(a3,a4,1x,a8,i6,a7,a)")) " - ",mols(i)%name," resid ",mols(i)%idx_pdb," from ",trim(adjustl(sys%fname_pdb))

   do j=1,mols(i)%n_at
      if (sys%mask(j,is).eq.1) then

         is2sub=.false.
         if (Hsub) then
            do k=1,2*n_Hsub(is),2
               if (j.eq.Hsub_info(k,is)) then
                  is2sub=.true.                  
                  exit
               endif
            enddo
         endif
        
         if (.not.is2sub) then 

            if (sel.eq.0) then
               write(10,"(a3,2x,3(f10.4,1x))") mols(i)%name_el(j),mols(i)%r_at(j,:)
            elseif (sel.eq.1) then
               elename=mols(i)%name_at(j)
               write(10,"(a3,2x,3(f10.4,1x))") elename(1:1),mols(i)%r_at(j,:)
            endif
         
         else

            P0=mols(i)%r_at(Hsub_info(k,is),:) 
            P1=mols(i)%r_at(Hsub_info(k+1,is),:)
            P0=P1 + 1.090_rk * (P0-P1)/norm3(P0-P1)

            write(10,"(a3,2x,3(f10.4,1x))") 'H',P0
            
         endif
   
      endif
   enddo
      
   if (n_Hadd(is).gt.0) then
      do j=1,n_Hadd(is)
         
         P0=mols(i)%r_at(Hadd_info(3*j-2,is),:) 
         P1=mols(i)%r_at(Hadd_info(3*j-1,is),:) 
         P2=mols(i)%r_at(Hadd_info(3*j,is),:) 
         
         P1=(P0 + P1 + P2 )/3.0_rk
         P1= (P0-P1)/norm3(P0-P1)
         P1=P0 + 1.090_rk*P1
         write(10,"(a3,2x,3(f10.4,1x))") 'H',P1
         
      enddo
   endif

   close(10)

enddo


end subroutine write_xyz_frags


!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine write_pdb_frags
  implicit none 
  integer             :: i,j,sel,is,nat_sel 
  character(len=50)   :: fn_pdb
  character(len=80)   :: line
  character(len=5)    :: molid
  logical             :: exists

write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Write coordinates of each molecular unit in pdb format'
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*) 
call stop_pdb

call system ("mkdir -p _pdb_frag")

write(*,*) ' WARNING: if one or more atom-masks are loaded via option 4  ' 
write(*,*) '          these will be used to determine the atoms to print!'  
write(*,*) 



do i=1,sys%nmol_tot 

   is=mols(i)%spec_idx

   write(molid,"(i0.5)") i
   fn_pdb="_pdb_frag/"//molid//'.pdb'

   open(file=fn_pdb,unit=10,status="replace")

   nat_sel=0
   do j=1,mols(i)%n_at
      if (sys%mask(j,is).eq.1) nat_sel=nat_sel+1
   enddo

    write(10,("(a7,a4,1x,a8,i6,a7,a)")) "REMARK ",mols(i)%name," resid ",mols(i)%idx_pdb," from ",trim(adjustl(sys%fname_pdb))

   do j=1,mols(i)%n_at
      
      if (sys%mask(j,is).eq.1) then
                  

!COLUMNS        DATA  TYPE    FIELD        DEFINITION
!-------------------------------------------------------------------------------------
! 1 -  6        Record name   "ATOM  "
! 7 - 11        Integer       serial       Atom  serial number.
!13 - 16        Atom          name         Atom name.
!18 - 20        Residue name  resName      Residue name.
!23 - 26        Integer       resSeq       Residue sequence number.
!31 - 38        Real(8.3)     x            Orthogonal coordinates for X in Angstroms.
!39 - 46        Real(8.3)     y            Orthogonal coordinates for Y in Angstroms.
!47 - 54        Real(8.3)     z            Orthogonal coordinates for Z in Angstroms.
!77 - 78        LString(2)    element      Element symbol, right-justified.

         line=''   
         write(line(1:6),"(a6)") "ATOM  "
         write(line(7:11),"(i5)") j
         write(line(13:16),"(a4)")  mols(i)%name_at(j)
         write(line(18:20),"(a3)")  mols(i)%name(1:3)
         write(line(23:26),"(i4)") i
         write(line(31:54),"(3(f8.3))") mols(i)%r_at(j,:)
         write(line(77:78),"(a2)")  adjustr(trim(mols(i)%name_el(j)))
         write(10,"(a80)") line

      endif

   enddo
   close(10)

enddo


end subroutine write_pdb_frags

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine ask_pbc
! This subroutine sets/overwrite pbc
  implicit none 
  integer        :: i,insert_pbc


write(*,*)  ' Do you want to specify/overwrite the cell vectors? '
write(*,*)  ' 0=NO  1=YES(cell parameters)  2=YES(cell vectors)'
read(*,*)  insert_pbc
write(*,*)  ' Value read:',insert_pbc
write(*,*)

if (insert_pbc.eq.0)  then
   write(*,*) ' Cell vectors read from pdb will be used, if any!'
   write(*,*)

elseif (insert_pbc.eq.1)  then

   write(*,*) ' Insert the cell parameters (a,b,c,alpha,beta,gamma): '
   read(*,*) sys%cell
   write(*,*) ' Values read:',sys%cell
   sys%box=cell2box(sys%cell)
   write(*,*)

elseif (insert_pbc.eq.2)  then

   write(*,*) ' Insert the cell vectors (3 lines): '
   do i=1,3
      read(*,*) sys%box(i,:)
   enddo

   write(*,*) ' Values read:'
   do i=1,3
      write(*,"(3(f10.4,1x))") sys%box(i,:)
   enddo
   write(*,*)
   sys%cell=box2cell(sys%box)
   
else
   call wrong_input 
endif

end subroutine ask_pbc

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine check_pbc
  implicit none 
  integer        :: i


if ( (sys%active_pbc(1).eq.0).and.(sys%active_pbc(2).eq.0) &
        .and.(sys%active_pbc(3).eq.0) ) then
   write(*,*) ' WARNING: PBC are disabled!'

else 

   do i=1,3

!      write(*,*) sys%active_pbc(i),norm3(sys%box(i,:))
      if ( (sys%active_pbc(i).eq.1).and.(norm3(sys%box(i,:)).gt.0.0100) )then
          write(*,*)  ' PBC active along axis ',i
       elseif (sys%active_pbc(i).eq.0) then
          write(*,*)  ' PBC disables along axis ',i
       else
          write(*,*) ' E/ORROR: suspicious length of cell vector ',i
          call stop_exec          

     endif
   enddo
endif
write(*,*)


end subroutine check_pbc

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine check_box
  implicit none 
  integer        :: i


do i=1,3

   if ( norm3(sys%box(i,:)) .le.0.0100_rk ) then
      write(*,*) ' E/ORROR: suspicious length of cell vector ',i
      call stop_exec  
   elseif ( norm3(sys%box(i,:)) .le.1.0_rk ) then
      write(*,*) ' WARNING: suspicious length of cell vector ',i
      write(*,*)
     
   endif

enddo


end subroutine check_box

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine system_log
! This subroutine writes system logs
  implicit none 
  integer        :: i 
  character(50)  :: fn_out

write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Write system log files '
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*) 
call stop_pdb


!!!!!! system info log file
fn_out=trim(sys%fname_root)//"_sys.log"
open(file=fn_out,unit=10,status="replace")

write(10,"(a)") " Number of molecules, atoms, chemical species:  "
write(10,"(3(i8,1x))") sys%nmol_tot,sys%nat_tot,sys%n_spec
write(10,*)
write(10,"(a)") " Simulation cell parameters: "
write(10,"(6(1x,f9.3))") sys%cell
write(10,*)
write(10,"(a)") ' Simulation cell vectors: '
write(10,"(3(1x,f10.4))") sys%box(1,:)
write(10,"(3(1x,f10.4))") sys%box(2,:)
write(10,"(3(1x,f10.4))") sys%box(3,:)
write(10,*)
close(10)
write(*,*) " System info log file wrote to ",adjustl(fn_out)
write(*,*)


!!!!!! chemical species log file
fn_out=trim(sys%fname_root)//"_spc.log"
open(file=fn_out,unit=10,status="replace")

write(10,"(80a)") "% 1.idx_specie  2.n_atoms  3.n_molecules   4.name "
do i=1,sys%n_spec
   write(10,"(i8,1x,i8,1x,i8,1x,a10)") i,sys%nat_spec(i),sys%nmol_spec(i),&
                                       "% "//sys%mol_names(i)
enddo
close(10)
write(*,*) " Chemical species log file wrote to ",adjustl(fn_out)
write(*,*)


!!!!!! molecular units log file
fn_out=trim(sys%fname_root)//"_mol.log"
open(file=fn_out,unit=10,status="replace")

write(10,"(80a)") "% 1.idx_mol  2.resid_pdb  3.idx_specie  4-6.r_cm  6.n_at"
do i=1,sys%nmol_tot
   write(10,"(i8,1x,i8,1x,i8,1x,3(f9.4,2x),i8)") i, mols(i)%idx_pdb, mols(i)%spec_idx, &
            mols(i)%r_cm(:), mols(i)%n_at
enddo
close(10)
write(*,*) " Molecular units log file wrote to ",adjustl(fn_out)
write(*,*)


end subroutine system_log

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

! 2clear 
! 4/6/15 only pbc version is maintained
!subroutine ctisetup_orca_nopbc 
!  implicit none
!  integer       :: sel,i,j,is,i_m1,i_m2
!  character(80) :: fn_out, keywords 
!  character(len=5)    :: molid,elename,molid1,molid2
!! improvements:
!! - use masks
!! - create the PBC version (if the monomer appears tranlated do specific calculation!)
!
!write(*,*) 
!write(*,*) ' -------------------------------------------'
!write(*,*) ' You choose option ', option
!write(*,*) ' Create input for couplings calculation with ORCA (no PBC)'
!write(*,*) ' -------------------------------------------'
!write(*,*) 
!write(*,*) 
!call stop_pdb
!
!if (.not.connectivity_ok) then
!   write(*,*) 'E/ORROR: connectivity has to be determined before! '
!   call stop_exec
!endif
!
!
!write(*,*) ' Insert the keyword line ' 
!write(*,*) '   Examples: '
!write(*,*) '   B3LYP  6-31G*  RIJCOSX  LOOSESCF '
!write(*,*) '   ZINDO/S '
!read(*,"(a80)")  keywords
!write(*,*) ' Value read: ',trim(adjustl(keywords))
!write(*,*) 
!
!write(*,*) 'From which pdb record you want to take the element name? '
!write(*,*) '0=element symbol (recommended)  1=atom name (1st character) '
!read(*,*) sel 
!write(*,*) ' Value read: ',sel
!write(*,*) 
!if ((sel.ge.2).or.(sel.le.0) )  call wrong_input 
!
!write(*,*) ' WARNING: if one or more atom-masks have been loaded, these  ' 
!write(*,*) '          will be used to determine the atoms to print!'  
!write(*,*) 
!
!
!call system ("mkdir -p _cti_orca")
!
!
!! single fragments
!open(file="_cti_orca/frag_list.txt",unit=11,status="replace")
!
!do i=1,sys%nmol_tot 
!
!   is=mols(i)%spec_idx
!
!   write(molid,"(i0.5)") i
!   fn_out="_cti_orca/"//molid//'.inp'
!
!   open(file=fn_out,unit=10,status="replace")
!
!   ! header
!   write(10,"(a2,a)") '! ',trim(adjustl(keywords))
!   write(10,"(a)") '%output'
!   write(10,"(a)") ' Print[P_MOs] 1'
!   write(10,"(a)") ' Print[P_Overlap] 1'
!   write(10,"(a)") 'end'
!   write(10,"(a)") '* xyz 0 1'
!
!   do j=1,mols(i)%n_at
!      
!      if (sys%mask(j,is).eq.1) then
!         
!         if (sel.eq.0) then
!            write(10,"(a3,2x,3(f10.4,1x))") mols(i)%name_el(j),mols(i)%r_at(j,:)
!         elseif (sel.eq.1) then
!            elename=mols(i)%name_at(j)
!            write(10,"(a3,2x,3(f10.4,1x))") elename(1:1),mols(i)%r_at(j,:)
!         endif
!      endif
!
!   enddo
!
!   write(10,"(a)") '*'
!   close(10)
!
!   write(11,"(a)") molid
!enddo
!
!close(11)
!write(*,*) "List of fragment jobs wrote in _cti_orca/frag_list.txt"
!write(*,*)
!
!
!! dimers    
!open(file="_cti_orca/dimer_list.txt",unit=11,status="replace")
!
!do i=1,n_conn
!
!   i_m1=connectivity(i,1)
!   i_m2=connectivity(i,2)
!   
!   write(molid1,"(i0.5)") i_m1
!   write(molid2,"(i0.5)") i_m2
!   fn_out="_cti_orca/"//molid1//'_'//molid2//'.inp'
!
!   open(file=fn_out,unit=10,status="replace")
!
!   ! header
!   write(10,"(a2,a)") '! ',trim(adjustl(keywords))
!   write(10,"(a)") '%output'
!   write(10,"(a)") ' Print[P_MOs] 1'
!   write(10,"(a)") ' Print[P_Overlap] 1'
!   write(10,"(a)") 'end'
!   write(10,"(a)") '* xyz 0 1'
!
!   ! mol 1
!   is=mols(i_m1)%spec_idx
!   do j=1,mols(i_m1)%n_at
!      if (sys%mask(j,is).eq.1) then
!         
!         if (sel.eq.0) then
!            write(10,"(a3,2x,3(f10.4,1x))") mols(i_m1)%name_el(j),mols(i_m1)%r_at(j,:)
!         elseif (sel.eq.1) then
!            elename=mols(i_m1)%name_at(j)
!            write(10,"(a3,2x,3(f10.4,1x))") elename(1:1),mols(i_m1)%r_at(j,:)
!         endif
!      endif
!   enddo
!
!   ! mol 2
!   is=mols(i_m2)%spec_idx
!   do j=1,mols(i_m2)%n_at
!      if (sys%mask(j,is).eq.1) then
!         
!         if (sel.eq.0) then
!            write(10,"(a3,2x,3(f10.4,1x))") mols(i_m2)%name_el(j),mols(i_m2)%r_at(j,:)
!         elseif (sel.eq.1) then
!            elename=mols(i_m2)%name_at(j)
!            write(10,"(a3,2x,3(f10.4,1x))") elename(1:1),mols(i_m2)%r_at(j,:)
!         endif
!      endif
!   enddo
!
!   write(10,"(a)") '*'
!   close(10)
!
!   write(11,"(a)") molid1//'_'//molid2
!
!enddo
!
!close(11)
!write(*,*) "List of dimer jobs wrote in _cti_orca/dimer_list.txt"
!write(*,*)
!
!end subroutine ctisetup_orca_nopbc


!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


subroutine ctisetup_orca_pbc 
  implicit none
  integer       :: sel,i,j,is,i_m1,i_m2,idx_ghost(n_spec_max,3)
  real(rk)      :: tv(3),r0(3),r1(3),r2(3),v1(3),v2(3),v3(3)
  character(80) :: fn_out, keywords 
  character(len=5)    :: molid,elename,molid1,molid2
  logical             :: write_xyz_dim,write_ghost
! improvements:
! - use masks
! - create the PBC version (if the monomer appears tranlated do specific calculation!)

write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Create input for couplings calculation with ORCA with PBC'
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*) 
call stop_pdb

if (.not.connectivity_ok) then
   write(*,*) 'E/ORROR: connectivity has to be determined before! '
   call stop_exec
endif


write(*,*) ' Insert the keyword line ' 
write(*,*) '   Examples: '
write(*,*) '   B3LYP  6-31G*  RIJCOSX  LOOSESCF '
write(*,*) '   ZINDO/S '
read(*,"(a80)")  keywords
write(*,*) ' Value read: ',trim(adjustl(keywords))
write(*,*) 

write(*,*) 'From which pdb record you want to take the element name? '
write(*,*) '0=element symbol (recommended)  1=atom name (1st character) '
read(*,*) sel 
write(*,*) ' Value read: ',sel
write(*,*) 
if ((sel.gt.1).or.(sel.lt.0) )  call wrong_input 

write(*,*) ' WARNING: if one or more atom-masks have been loaded, these  ' 
write(*,*) '          will be used to determine the atoms to print!'  
write(*,*) 

write(*,*) ' Would you like to write also dimers xyz? T/F'
read(*,*) write_xyz_dim
write(*,*) ' Value read: ', write_xyz_dim
write(*,*) 

write(*,*) ' Would you like to compute the signed couplings? T/F'
read(*,*) write_ghost
write(*,*) ' Value read: ', write_ghost
write(*,*) 


idx_ghost=0 !idx_ghost(n_spec_max,3)
if (write_ghost) then
   write(*,*) 'Ghost atoms are placed on top of a chosen atoms, displaced by 0.6 Angstrom'
   write(*,*) 'along the normal to the plane defined by the first atom plus other two.'
   write(*,*)

   do i=1,sys%n_spec
      write(*,*) 'Insert the indexes of 3 atoms for specie '//adjustl(sys%mol_names(i))
      read(*,*)  idx_ghost(i,1),idx_ghost(i,2),idx_ghost(i,3)
      write(*,*) ' Values read: ', idx_ghost(i,1),idx_ghost(i,2),idx_ghost(i,3)
      write(*,*) 
   enddo
endif


call system ("mkdir -p _cti_orca")
if (write_xyz_dim) call system ("mkdir -p _xyz_dim")

! single fragments
open(file="_cti_orca/frag_list.txt",unit=11,status="replace")

do i=1,sys%nmol_tot 

   is=mols(i)%spec_idx

   write(molid,"(i0.5)") i
   fn_out="_cti_orca/"//molid//'.inp'

   open(file=fn_out,unit=10,status="replace")

   ! header
   write(10,"(a2,a)") '! ',trim(adjustl(keywords))
   write(10,"(a)") '%output'
   write(10,"(a)") ' Print[P_MOs] 1'
   write(10,"(a)") ' Print[P_Overlap] 1'
   write(10,"(a)") 'end'
   write(10,"(a)") '* xyz 0 1'

   do j=1,mols(i)%n_at
      
      if (sys%mask(j,is).eq.1) then
         
         if (sel.eq.0) then
            write(10,"(a3,2x,3(f10.4,1x))") mols(i)%name_el(j),mols(i)%r_at(j,:)
         elseif (sel.eq.1) then
            elename=mols(i)%name_at(j)
            write(10,"(a3,2x,3(f10.4,1x))") elename(1:1),mols(i)%r_at(j,:)
         endif
      endif

   enddo

   write(10,"(a)") '*'
   close(10)

   write(11,"(a)") molid
enddo

close(11)
write(*,*) "List of fragment jobs wrote in _cti_orca/frag_list.txt"
write(*,*)


! dimers
open(file="_cti_orca/dimer_list.txt",unit=11,status="replace")

do i=1,n_conn

   i_m1=connectivity(i,1)
   i_m2=connectivity(i,2)
   tv=connectivity(i,3)*sys%box(1,:) + connectivity(i,4)*sys%box(2,:) + &
      connectivity(i,5)*sys%box(3,:)
   
   write(molid1,"(i0.5)") i_m1
   write(molid2,"(i0.5)") i_m2
   fn_out="_cti_orca/"//molid1//'_'//molid2//'.inp'
   open(file=fn_out,unit=10,status="replace")
   ! header
   write(10,"(a2,a)") '! ',trim(adjustl(keywords))
   write(10,"(a)") '%output'
   write(10,"(a)") ' Print[P_MOs] 1'
   write(10,"(a)") ' Print[P_Overlap] 1'
   write(10,"(a)") 'end'
   write(10,"(a)") '* xyz 0 1'


   if (write_ghost) then
      fn_out="_cti_orca/"//molid1//'_'//molid2//'.g.inp'
      open(file=fn_out,unit=20,status="replace")
      ! header
      write(20,"(a2,a)") '! ',trim(adjustl(keywords))
      write(20,"(a)") '%scf'
      write(20,"(a)") ' MaxIter 0'
      write(20,"(a)") 'end'
      write(20,"(a)") '%output'
      write(20,"(a)") ' Print[P_Overlap] 1'
      write(20,"(a)") 'end'
      write(20,"(a)") '%basis'
      write(20,"(a)") ' NewGTO He'
      write(20,"(a)") '  S   3'
      write(20,"(a)") '  1         0.1873113696E+02       0.3349460434E-01'
      write(20,"(a)") '  2         0.2825394365E+01       0.2347269535E+00'
      write(20,"(a)") '  3         0.6401216923E+00       0.8137573261E+00'
      write(20,"(a)") ' end'
      write(20,"(a)") 'end'      
      write(20,"(a)") '* xyz 0 1'
   endif

   if (write_xyz_dim) then
      fn_out="_xyz_dim/"//molid1//'_'//molid2//'.xyz'
      open(file=fn_out,unit=12,status="replace")
      write(12,*) mols(i_m1)%n_at+mols(i_m2)%n_at
      write(12,*) 
   endif


   ! mol 1
   is=mols(i_m1)%spec_idx
   do j=1,mols(i_m1)%n_at
      if (sys%mask(j,is).eq.1) then
         
         if (sel.eq.0) then
            write(10,"(a3,2x,3(f10.4,1x))") mols(i_m1)%name_el(j),mols(i_m1)%r_at(j,:)
            if (write_xyz_dim) write(12,"(a3,2x,3(f10.4,1x))") mols(i_m1)%name_el(j),mols(i_m1)%r_at(j,:)
            if (write_ghost)   write(20,"(a3,2x,3(f10.4,1x))") mols(i_m1)%name_el(j),mols(i_m1)%r_at(j,:)

         elseif (sel.eq.1) then
            elename=mols(i_m1)%name_at(j)
            write(10,"(a3,2x,3(f10.4,1x))") elename(1:1),mols(i_m1)%r_at(j,:)
            if (write_xyz_dim) write(12,"(a3,2x,3(f10.4,1x))") elename(1:1),mols(i_m1)%r_at(j,:)
            if (write_ghost)   write(20,"(a3,2x,3(f10.4,1x))") elename(1:1),mols(i_m1)%r_at(j,:)
         endif
      endif
   enddo

   ! mol 2
   is=mols(i_m2)%spec_idx
   do j=1,mols(i_m2)%n_at
      if (sys%mask(j,is).eq.1) then
         
         if (sel.eq.0) then
            write(10,"(a3,2x,3(f10.4,1x))") mols(i_m2)%name_el(j),mols(i_m2)%r_at(j,:)+tv
            if (write_xyz_dim) write(12,"(a3,2x,3(f10.4,1x))") mols(i_m2)%name_el(j),mols(i_m2)%r_at(j,:)+tv
            if (write_ghost)   write(20,"(a3,2x,3(f10.4,1x))") mols(i_m2)%name_el(j),mols(i_m2)%r_at(j,:)+tv
         elseif (sel.eq.1) then
            elename=mols(i_m2)%name_at(j)
            write(10,"(a3,2x,3(f10.4,1x))") elename(1:1),mols(i_m2)%r_at(j,:)+tv
            if (write_xyz_dim) write(12,"(a3,2x,3(f10.4,1x))") elename(1:1),mols(i_m2)%r_at(j,:)+tv
            if (write_ghost)   write(20,"(a3,2x,3(f10.4,1x))") elename(1:1),mols(i_m2)%r_at(j,:)+tv
         endif
      endif
   enddo

   write(10,"(a)") '*'
   close(10)
   if (write_xyz_dim) close(12)


   if (write_ghost) then  ! idx_ghost(n_spec_max,3)
      ! ghost on mol 1
      is=mols(i_m1)%spec_idx
      r0=mols(i_m1)%r_at(idx_ghost(is,1),:)
      r1=mols(i_m1)%r_at(idx_ghost(is,2),:)
      r2=mols(i_m1)%r_at(idx_ghost(is,3),:)
      v1=r1-r0
      v2=r2-r0
      v3=VectorProd3(v1,v2)
      v3=v3/Norm3(v3)
      r0=r0+0.60_rk*v3
      write(20,"(a3,2x,3(f10.4,1x))")  'He:',r0

      ! ghost on mol 2
      is=mols(i_m2)%spec_idx
      r0=mols(i_m2)%r_at(idx_ghost(is,1),:)
      r1=mols(i_m2)%r_at(idx_ghost(is,2),:)
      r2=mols(i_m2)%r_at(idx_ghost(is,3),:)
      v1=r1-r0
      v2=r2-r0
      v3=VectorProd3(v1,v2)
      v3=v3/Norm3(v3)
      r0=r0+0.60_rk*v3 +tv
      write(20,"(a3,2x,3(f10.4,1x))")  'He:',r0

      write(20,"(a)") '*'
      close(20)
   endif

   write(11,"(a)") molid1//'_'//molid2

enddo

close(11)
write(*,*) "List of dimer jobs wrote in _cti_orca/dimer_list.txt"
write(*,*)

end subroutine ctisetup_orca_pbc


!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine write_connectivity
  implicit none
  integer        :: i,conn_mode,stat,sel,i_m1,i_m2,i_at
  real(rk)       :: Rc,dist_cm,tv(3),d_min,Rc2
  character(50)  :: fn_out
  logical        :: write_dim

write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Determine intermolecular connectivity'
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*) 
call stop_pdb

!!!!!! connectivity log file
write(*,"(a)") ' Please specify:'
write(*,"(a)") '   -Criterium to be used (0=center of mass, 1=interatomic distance) ' 
write(*,"(a)") '   -Cutoff distance (Angstrom)'
write(*,"(a)") '   -Coarse cutoff (Angstrom)'
write(*,"(a)") ' Note: The last input is revelant only for interatomic distance criterium. '
write(*,"(a)") '       A safe value is 1.5x the leading molecular dimension'
read(*,*) conn_mode,Rc,Rc2
write(*,*) ' Values read: ',conn_mode,Rc,Rc2
write(*,*)

if ( .not. ((conn_mode.eq.0).or.(conn_mode.eq.1) )) then
   call wrong_input
endif

write(*,"(a)") ' Should I use PBC to determine the connectivity? (0=NO, 1=YES)'
write(*,*)     ' Specify 3 values for the three cell axes (eg 0 0 0 = no PBC)'
read(*,*)   sys%active_pbc
write(*,*)  ' Values read:',sys%active_pbc
write(*,*)

do i=1,3
   if ( .not. ( (sys%active_pbc(i).eq.0).or. (sys%active_pbc(i).eq.1) ) ) then
      call wrong_input
   endif
enddo

call check_pbc

call determine_connectivity(conn_mode,Rc,Rc2)

fn_out=trim(sys%fname_root)//"_con.log"
open(file=fn_out,unit=10,status="replace")
write(10,"(a)") '% 1.idx_1  2.idx_2  3.rep_1  4.rep_2  5.rep_3  6.specie_1  7.specie_2'
write(10,"(a)") '%  8.resid_pdb_1  9.resid_pdb_2  10.dist_cm  11.dist_min'

do i=1,n_conn

   tv=connectivity(i,3)*sys%box(1,:) + connectivity(i,4)*sys%box(2,:) + &
      connectivity(i,5)*sys%box(3,:)
   dist_cm=Norm3( mols(connectivity(i,1))%r_cm - mols(connectivity(i,2))%r_cm - tv)

   d_min=shortest_contact(connectivity(i,1),connectivity(i,2),tv)
   
   write(10,"(9(i8,1x),2(f10.4),1x)") connectivity(i,:), &
        mols(connectivity(i,1))%spec_idx,mols(connectivity(i,2))%spec_idx, &
        mols(connectivity(i,1))%idx_pdb,mols(connectivity(i,2))%idx_pdb,&
        dist_cm,d_min
enddo

close(10)
write(*,*) " Molecular connectivity log file wrote to ",adjustl(fn_out)
write(*,*) 


! intermolecular connectivity pdb

fn_out=trim(sys%fname_root)//"_con.pdb"
open(file=fn_out,unit=10,status="replace")

write(10,"(a)") "REMARKS  connections across pbc are not reported "

call write_pdb_cryst1(10) 

call write_pdb_cm(10)

call write_pdb_conect(10)

close(10)
write(*,*) " Molecular connectivity pdb wrote to ",adjustl(fn_out)
write(*,*) 

connectivity_ok=.true.

end subroutine write_connectivity

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine  translate_rotate_pdb
  implicit none
  integer   :: i,j,is,i2w
  character(80) :: fname_out,line
  real(rk)      :: U(3,3),T(3),r_new(3)



write(*,*) ' Insert the filename for the output pdb:' 
read(*,*) fname_out
write(*,*) ' Value read: ',fname_out
write(*,*) 
open(file=fname_out,unit=10,status="replace")

T=0.0_rk
write(*,*) ' Specify the translation vector (3 reals):'
read(*,*)  T
write(*,*) ' Values read: ',T
write(*,*) 

U=0.0_rk
write(*,*) ' Specify the rotation matrix (3 reals per line):'
do i=1,3
   read(*,*) U(i,:)
enddo
write(*,*) ' Value read: '
do i=1,3
   write(*,"(3(f10.4,1x))") U(i,:)
enddo
write(*,*) 

i2w=0
do i=1,sys%nmol_tot 

   is=mols(i)%spec_idx

   i2w=i2w+1
   if (i2w.eq.9999) i2w=0
   
   do j=1,mols(i)%n_at
      
      if (sys%mask(j,is).eq.1) then

         r_new= matmul( U , (mols(i)%r_at(j,:)-T) )

         

         
         line=''   
         write(line(1:6),"(a6)") "ATOM  "
         write(line(7:11),"(i5)") j
         write(line(13:16),"(a4)")  mols(i)%name_at(j)
         write(line(18:20),"(a3)")  mols(i)%name(1:3)
         write(line(23:26),"(i4)") i2w
         write(line(31:54),"(3(f8.3))") r_new
         write(line(55:60),"(f6.2)") mols(i)%occ(j)
         write(line(61:66),"(f6.2)") mols(i)%tfc(j)
         write(line(77:78),"(a2)")  adjustr(trim(mols(i)%name_el(j)))
         write(10,"(a80)") line

      endif

   enddo

enddo


end subroutine translate_rotate_pdb

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine cut_pbttt
  implicit none
  integer   :: i,i_at,i_mol,rl,idx0,idx_mol,idx_at,sel
  character(80) :: fname_sel,line,fn_list
  real(rk)      :: r0(3),L(3),r1(3)


write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Extract selection from pdb of PBTTT samples'
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*) 
call stop_pdb

write(*,*) ' Insert the filename for the output pdb:' 
read(*,*) fname_sel
write(*,*) ' Value read: ',fname_sel
write(*,*) 
open(file=fname_sel,unit=10,status="replace")

rl=len(trim(adjustl(fname_sel)))
fn_list=fname_sel(1:rl-4)//"_selection.log"
open(file=fn_list,unit=11,status="replace")
write(11,"(a)") '% 1.idx_new  2.idx_old  3.idx_old_pdb  4.idx_specie  5-7.xyz_cm'



sel=2
idx0=1
!write(*,*) ' Specify the index of the molecule at the center of the box'
!read(*,*)  idx0
!write(*,*) ' Value read: ',idx0
!write(*,*) 

write(*,*) ' Specify the box length along the three axes:'
read(*,*)  L
write(*,*) ' Values read: ',L
write(*,*) 
do i=1,3
   L(i)=0.50_rk*L(i)/norm3(sys%box(i,:))
enddo

r0=cart2frac(mols(idx0)%r_cm)
write(*,*) ' ......',idx0,r0

!!!!! Write selection! 
idx_mol=0
idx_at=0

do i_mol=1,sys%nmol_tot

   ! determine if the molecule is inside the selection
   r1=cart2frac(mols(i_mol)%r_at(1,:))
   if ( abs(r1(1)-r0(1)).le.L(1) .and. &
        abs(r1(2)-r0(2)).le.L(2) .and. &
        abs(r1(3)-r0(3)).le.L(3) )  then

      idx_mol=idx_mol+1

      write(11,"(4(i5,1x),3(f10.4,1x))") idx_mol,i_mol,mols(i_mol)%idx_pdb,mols(i_mol)%spec_idx,mols(i_mol)%r_cm

      do i_at=1,mols(i_mol)%n_at

         idx_at=idx_at+1
         line='' 
         write(line(1:6),"(a6)") "ATOM  "
         write(line(7:11),"(i5)")  idx_at
         write(line(13:16),"(a4)") mols(i_mol)%name_at(i_at)
         write(line(18:20),"(a3)") mols(i_mol)%name
         write(line(23:26),"(i4)") idx_mol
         write(line(31:54),"(3(f8.3))") mols(i_mol)%r_at(i_at,:)
         write(line(77:78),"(a2)")  adjustr(trim(mols(i_mol)%name_el(i_at)))
         if (is_pdx) write(line(31:80),"(3(f13.6))") mols(i_mol)%r_at(i_at,:)

         write(10,"(a80)") line
      enddo
   endif

enddo



close(10)
write(*,*) " Selection wrote to ",trim(adjustl(fname_sel))
write(*,*)
close(11)
write(*,*) " List of selected molecules  wrote to ",trim(adjustl(fn_list))
write(*,*)

end subroutine cut_pbttt

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine cut_selection  
  implicit none
  integer   :: i,j,sel,sel2,idx0(2),n_rep(3),idx_mol,idx_at,i_mol,i_at
  integer   :: i1,i2,i3,rl,idx_mol2,null_dim,idx_sel,hkl(3)
  character(80) :: fname_sel,line,fn_list,fname_V0F0_in,fname_V0F0_out,line2
  character(len=4) :: rname_new(2)
  real(rk)      :: r0(3),Rc,t(3),r_new(3),d0(3),Lbox(3)
  real(rk)      :: h,n(3),p1(3),p2(3),p3(3),r1(3),z0,z1,tol
  logical       :: is_in,is_2write,write_V0F0,keep_idx

write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Extract selection from pdb file within cutoff'
write(*,*) ' or box boundary '
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*) 
call stop_pdb

write(*,*) ' Insert the filename for the output pdb:' 
read(*,*) fname_sel
write(*,*) ' Value read: ',fname_sel
write(*,*) 
open(file=fname_sel,unit=10,status="replace")


rl=len(trim(adjustl(fname_sel)))
fn_list=fname_sel(1:rl-4)//"_selection.log"
open(file=fn_list,unit=11,status="replace")
write(11,"(a)") '% 1.idx_new  2.idx_old  3.idx_old_pdb  4.idx_specie  5-7.xyz_cm'


write(*,*) ' Set 0 for spherical 3D cutoff '
write(*,*) ' Set 1/2/3 for 2D cutoff ignoring x/y/z dimension'
write(*,*) ' Set 4 for box-shaped cluster'
write(*,*) ' Set 5 for cylinder with axis perpendicular to an arbitrary lattice plane'
write(*,*) '     (centroid criterium only in the last case)'
read(*,*)  null_dim
write(*,*) ' Value read: ',null_dim
write(*,*)


if ( (null_dim.ge.0) .and. (null_dim.le.3) ) then
   write(*,*) ' Insert the value for the cutoff radius (Angstrom)'
   read(*,*)  Rc
   write(*,*) ' Values read: ',Rc
   write(*,*)
elseif (null_dim.eq.4) then
   write(*,*) ' Insert the box lengths along x,y,z:'
   read(*,*)  Lbox
   write(*,*) ' Values read: ',Lbox
   write(*,*)
elseif (null_dim.eq.5) then
   write(*,*) ' Insert the cylinder radius, height and tolerance (recommended 0.1):'
   read(*,*)  Rc,h,tol
   write(*,*) ' Values read: ',Rc,h,tol
   write(*,*)
   write(*,"(a)") ' Specify the Miller indices of the plane'
   read(*,*)   hkl 
   write(*,*)  ' Values read: ',hkl
   write(*,*)

   p1=( 1.0_rk/(hkl(1)+epsilon) )*sys%box(1,:)
   p2=( 1.0_rk/(hkl(2)+epsilon) )*sys%box(2,:)
   p3=( 1.0_rk/(hkl(3)+epsilon) )*sys%box(3,:)

   n=VectorProd3( (p2-p1) , (p3-p1) )
   n=n/norm3(n)
   if (n(3).lt.0.0_rk) n=-n
   r1=r0 - h * n

   write(*,"(a,3(f10.4))")  ' INFO The cylinder axis versor is: ',n
   write(*,*)   
else
   call wrong_input
endif


write(*,*) ' How do you want to choose the central point of your selection?' 
write(*,*) '  1 = xyz coordinates' 
write(*,*) '  2 = molecular centroid'
write(*,*) '  3 = dimer centroid' 
read(*,*)  sel 
write(*,*) ' Values read: ',sel
write(*,*) 
if ( (sel.lt.1) .or. (sel.gt.3))  call wrong_input

r0=0.0_rk
idx0=0
rname_new=''
if (sel.eq.1) then

  write(*,*) ' Insert the xyz coordinate:' 
  read(*,*) r0
  write(*,*) ' Values read: ',r0

elseif (sel.eq.2) then

  write(*,*) ' Insert index and resname of the central molecule:' 
  read(*,*) idx0(1),rname_new(1)
  write(*,*) ' Values read: ',idx0(1),rname_new(1)
  if ( (idx0(1).le.0) .or. (idx0(1).gt.sys%nmol_tot ))  call wrong_input
  r0=mols(idx0(1))%r_cm

elseif (sel.eq.3) then

   do j=1,2
      write(*,*) ' Insert index and resname of molecule ',j 
      read(*,*) idx0(j),rname_new(j)
      write(*,*) ' Values read: ',idx0(j),rname_new(j)
      if ( (idx0(j).le.0) .or. (idx0(j).gt.sys%nmol_tot ))  call wrong_input
   enddo
   r0=0.5_rk*( mols(idx0(1))%r_cm + mols(idx0(2))%r_cm )
   
   if (idx0(1).eq.idx0(2)) then
      write(*,*) 
      write(*,*) "E/ORROR: the two molecular indexes mst be different!"
      call stop_exec
   endif
endif
write(*,*) 


write(*,*) ' How do you want to determine if a molecule is included in the selection:' 
write(*,*) '  1 = centroid criterium '
write(*,*) '  2 = closest distance '
write(*,*) '  3 = atom coordinate'
read(*,*)   sel2
write(*,*) ' Values read: ',sel2
write(*,*) 
if ( sel2.eq.3) then
   write(*,*)  ' Specify the index of the atom for the selection criterium:'
   read(*,*)   idx_sel
   write(*,*)  ' Values read: ',idx_sel
   write(*,*)
elseif ( (sel2.lt.1) .or. (sel2.gt.3) ) then
   call wrong_input
elseif ( (null_dim.eq.5) .and. (sel2.ne.1) ) then
   call wrong_input
endif

write(*,"(a)") ' Should I consider periodic replica to cut the selection? (0=NO, 1=YES)'
write(*,*)     ' Specify 3 values for the three cell axes (eg 0 0 0 = no replica)'
read(*,*)   sys%active_pbc
write(*,*)  ' Values read: ',sys%active_pbc
write(*,*)
do i=1,3
   if ( .not. ( (sys%active_pbc(i).eq.0).or. (sys%active_pbc(i).eq.1) ) ) then
      call wrong_input
   endif
enddo

call check_pbc

n_rep=0
do i=1,3
   if (sys%active_pbc(i).eq.1) then
      n_rep(i)=ceiling(2.00*Rc/norm3(sys%box(i,:)))
   endif
enddo

! GD 27/6/16 preserve residue index from original pdb
write(*,"(a)") ' Shall I preserve the residue index from the original pdb? (T/F)'
read(*,*)   keep_idx
write(*,*)  ' Values read: ',keep_idx
write(*,*)


! preparation of a file with field and potential due to permament 
! charges to be then used in ME/CR calculations

!write(*,*)  ' Do you want to create a file with potential and field '
!write(*,*)  ' at atomic sites within selection? (T=YES, F=NO) '
!read(*,*)   write_V0F0
!write(*,*)  ' Value read: ',write_V0F0
!write(*,*) 
write_V0F0=.false.

!if (write_V0F0) then 
!   write(*,*) ' Insert the name of the input file with potential and field:' 
!   read(*,*) fname_V0F0_in
!   write(*,*) ' Value read: ',fname_V0F0_in
!   write(*,*) 
!   call read_V0F0(fname_V0F0_in)
!
!   write(*,*) ' Insert the name of the output file with potential and field:' 
!   read(*,*) fname_V0F0_out
!   write(*,*) ' Value read: ',fname_V0F0_out
!   write(*,*) 
!   open(file=fname_V0F0_out,unit=12,status="replace")
!endif



!!!!! Write selection!
idx_mol=0
idx_at=0

! In case of molecule-wise selection write selected molecules first!
if ( (sel.eq.2) .or. (sel.eq.3) ) then 

   do j=1,sel-1

      idx_mol=idx_mol+1
!      if (idx_mol.ge.10000) idx_mol=idx_mol- 10000
      i_mol=idx0(j)
      
      
      write(11,"(4(i5,1x),3(f10.4,1x))") idx_mol,i_mol,mols(i_mol)%idx_pdb,mols(i_mol)%spec_idx,mols(i_mol)%r_cm

      do i_at=1,mols(i_mol)%n_at

         idx_at=idx_at+1
         line='' 
         write(line(1:6),"(a6)") "ATOM  "
         write(line(7:11),"(i5)")  idx_at
         write(line(13:16),"(a4)") mols(i_mol)%name_at(i_at)
         write(line(18:20),"(a3)") adjustl(trim(rname_new(j)))
         if (keep_idx) then
            write(line(23:26),"(i4)")  mols(i_mol)%idx_pdb
         else
            write(line(23:26),"(i4)") idx_mol
         endif

         write(line(31:54),"(3(f8.3))") mols(i_mol)%r_at(i_at,:)
         write(line(77:78),"(a2)")  adjustr(trim(mols(i_mol)%name_el(i_at)))
         if (is_pdx) write(line(31:80),"(3(f13.6))") mols(i_mol)%r_at(i_at,:)

         write(10,"(a80)") line

         if (write_V0F0) then
            write(12,"(i6,1x,i6,1x,f10.6,4(1x,e16.8))") idx_mol,i_at,mols(i_mol)%q0(i_at), &
                 mols(i_mol)%V_pc(i_at),mols(i_mol)%F_pc(i_at,:)
         endif

      enddo
   enddo

endif


do i3=-n_rep(3),n_rep(3)
do i2=-n_rep(2),n_rep(2)
do i1=-n_rep(1),n_rep(1)
   
   t=i1*sys%box(1,:) + i2*sys%box(2,:) + i3*sys%box(3,:)


   do i_mol=1,sys%nmol_tot

      ! determine if the molecule is inside the selection
      is_in=.false.

      if ( (null_dim.ge.0) .and. (null_dim.le.3) ) then ! sphere/disks-shaped cluster

         if (sel2.eq.1) then ! centroid criterium
         
            d0=r0 - (mols(i_mol)%r_cm + t)
            do j=1,3
               if (null_dim.eq.j) d0(j)=0.0_rk
            enddo

            if ( norm3(d0).le.Rc ) is_in=.true.
         
         elseif (sel2.eq.3) then ! atomic coordinates

            d0=r0 - (mols(i_mol)%r_at(idx_sel,:) + t)
            do j=1,3
               if (null_dim.eq.j) d0(j)=0.0_rk
            enddo

            if ( norm3(d0).le.Rc ) is_in=.true.


         elseif (sel2.eq.2) then ! closest contact criterium

            do i_at=1,mols(i_mol)%n_at

               d0=r0 - (mols(i_mol)%r_at(i_at,:) + t)
               do j=1,3
                  if (null_dim.eq.j) d0(j)=0.0_rk
               enddo

               if ( norm3(d0).le.Rc ) then
                  is_in=.true.
                  exit
               endif
            enddo
        
         endif

      elseif (null_dim.eq.4) then ! box-shaped cluster
            
         if (sel2.eq.1) then ! centroid criterium
               
            if (  ( abs(r0(1)-mols(i_mol)%r_cm(1)).le..5*Lbox(1) ) .and. &
                  ( abs(r0(2)-mols(i_mol)%r_cm(2)).le..5*Lbox(2) ) .and. &
                  ( abs(r0(3)-mols(i_mol)%r_cm(3)).le..5*Lbox(3) ) ) is_in=.true.

         elseif (sel2.eq.3) then ! atomic coordinates
            if (  ( abs(r0(1)-mols(i_mol)%r_at(idx_sel,1)).le..5*Lbox(1) ) .and. &
                     ( abs(r0(2)-mols(i_mol)%r_at(idx_sel,2)).le..5*Lbox(2) ) .and. &
                     ( abs(r0(3)-mols(i_mol)%r_at(idx_sel,3)).le..5*Lbox(3) ) ) is_in=.true.

         elseif (sel2.eq.2) then ! closest contact criterium

            do i_at=1,mols(i_mol)%n_at
               if (  ( abs(r0(1)-mols(i_mol)%r_at(i_at,1)).le..5*Lbox(1) ) .and. &
                     ( abs(r0(2)-mols(i_mol)%r_at(i_at,2)).le..5*Lbox(2) ) .and. &
                     ( abs(r0(3)-mols(i_mol)%r_at(i_at,3)).le..5*Lbox(3) ) ) then
                  is_in=.true.
                  exit
               endif
            enddo
            
         endif

      elseif (null_dim.eq.5) then ! CYLINDER WITH ARBITRARY AXIS 

         r_new=mols(i_mol)%r_cm + t
         z0=r0(3) -(n(1)/n(3))*(r_new(1)-r0(1)) -(n(2)/n(3))*(r_new(2)-r0(2))
         z1=r1(3) -(n(1)/n(3))*(r_new(1)-r1(1)) -(n(2)/n(3))*(r_new(2)-r1(2))

         if ( ((r_new(3)-z0).le.tol) .and. ( (r_new(3)-z1).ge.-tol) ) then 

            ! point to line (cylinder axis) distance    !iopppp
            !d0(1)=norm3(VectorProd3(r0-r1, r_new-r1))/norm3(r0-r1)
            d0(1)= (Norm3( r_new-r0 ))**2 - ( DotProd3( (r_new-r0) ,n ) )**2 
            d0(1)=sqrt(d0(1))


            if ( d0(1).le.Rc )  is_in=.true.

         endif

      endif


      ! determine if the molecule has to be written or not
      is_2write=.false.
      if ( (is_in).and.(sel.eq.1) ) then
         is_2write=.true.

      elseif ( (is_in).and.( (sel.eq.2) .or.(sel.eq.3) ) ) then
         is_2write=.true.

         ! the one/two central molecule(s) were already written 
         do j=1,sel-1
            if ( (norm3(t).le.1e-6).and.(i_mol.eq.idx0(j)) ) is_2write=.false.
         enddo 

      endif

!      write(12,*) '===',i_mol,mols(i_mol)%r_cm,is_in,is_2write 

      ! then write it!
      if (is_2write) then

         idx_mol=idx_mol+1
         
         write(11,"(4(i5,1x),3(f10.4,1x))") idx_mol,i_mol,mols(i_mol)%idx_pdb,mols(i_mol)%spec_idx,mols(i_mol)%r_cm
         
         do i_at=1,mols(i_mol)%n_at
            
            idx_at=idx_at+1
            r_new=mols(i_mol)%r_at(i_at,:) + t

            idx_mol2=idx_mol
            if (idx_mol.ge.10000) idx_mol2=idx_mol-10000*floor(real(idx_mol)/10000.0_rk)
            line='' 
            write(line(1:6),"(a6)") "ATOM  "
            write(line(7:11),"(i5)")  idx_at
            write(line(13:16),"(a4)") mols(i_mol)%name_at(i_at)
            write(line(18:20),"(a3)") mols(i_mol)%name(1:3)
            if (keep_idx) then
               write(line(23:26),"(i4)") mols(i_mol)%idx_pdb
            else
               write(line(23:26),"(i4)") idx_mol2
            endif
            write(line(31:54),"(3(f8.3))") r_new
            write(line(77:78),"(a2)")  adjustr(trim(mols(i_mol)%name_el(i_at)))
            if (is_pdx) write(line(31:80),"(3(f13.6))") r_new

            write(10,"(a80)") line

            if (write_V0F0) then
               write(12,"(i6,1x,i6,1x,f10.6,4(1x,e16.8))") idx_mol,i_at,mols(i_mol)%q0(i_at), &
                    mols(i_mol)%V_pc(i_at),mols(i_mol)%F_pc(i_at,:)
            endif

         enddo
      endif
      
   enddo


enddo
enddo
enddo


close(10)
write(*,*) " Selection wrote to ",trim(adjustl(fname_sel))
write(*,*)
close(11)
write(*,*) " List of selected molecules  wrote to ",trim(adjustl(fn_list))
write(*,*)

if (write_V0F0) then
   close(12)
   write(*,*) " Potential and field wrote to ",trim(adjustl(fname_V0F0_out))
   write(*,*)
endif

end subroutine cut_selection

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine read_V0F0(fname)
  implicit none
  integer       :: i_mol,i_at,i_m,i_a
  character(80) :: fname
  real(rk)      :: vals(5)

open(file=fname,unit=20,status="old")

! skip header
read(20,*)

do i_mol=1,sys%nmol_tot
   do i_at=1,mols(i_mol)%n_at
      
      read(20,*) i_m,i_a,vals

      if ( (i_m.eq.i_mol).and.(i_a.eq.i_at) ) then
          mols(i_mol)%q0(i_at)=vals(1)
          mols(i_mol)%V_pc(i_at)=vals(2)
          mols(i_mol)%F_pc(i_at,:)=vals(3:5)
      else
         write(*,*) "E/ORROR: file not matching with pdb! "
         call stop_exec
      endif

   enddo
enddo

close(20)

end subroutine read_V0F0

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine input_cell
  implicit none
write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Specify cell '
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*)
call stop_pdb

call ask_pbc

cell_ok=.true.

end subroutine input_cell

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine write_supercell 
  implicit none
  integer   :: i,n_rep(3),i1,i2,i3,idx_mol,idx_at,i_mol,i_at
  character(80) :: fname_rep,line,line2
  real(rk)      :: t(3),r_new(3),cell_new(6)

write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Write supercell pdb '
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*) 
call stop_pdb
call stop_pbc


write(*,*) ' Insert the filename for the supercell pdb file:' 
read(*,*) fname_rep
write(*,*) ' Values read: ',fname_rep
write(*,*) 
open(file=fname_rep,unit=10,status="replace")

if (is_pdx) then
   fname_rep=adjustl(trim(fname_rep))//'.pdb'
   open(file=fname_rep,unit=11,status="replace")
endif


write(*,"(a)") ' How many times do you want to replicate the sample? '
write(*,*)     ' Specify 3 integers for the three cell axes (1 1 1 = NO replica)'
read(*,*)   n_rep
write(*,*)  ' Values read: ',n_rep
write(*,*) 


cell_new=sys%cell
sys%active_pbc=0
do i=1,3
   if (n_rep(i).gt.0) sys%active_pbc(1)=1
   cell_new(i)=n_rep(i)*sys%cell(i)

enddo

call check_box

line=''
write(line(1:6),"(a6)") "CRYST1"
write(line(7:54),"(3(f9.3),3(f7.2))") cell_new

if (.not.(norm3(sys%box(1,:)).ne.sys%box(1,1))  ) then
   write(10,"(a80)") line
   if (is_pdx) write(11,"(a80)") line
endif

idx_mol=0
idx_at=0

n_rep=n_rep-1  
do i3=0,n_rep(3)
do i2=0,n_rep(2)
do i1=0,n_rep(1)
   
   t=i1*sys%box(1,:) + i2*sys%box(2,:) + i3*sys%box(3,:)

   do i_mol=1,sys%nmol_tot
      idx_mol=idx_mol+1

      do i_at=1,mols(i_mol)%n_at

         idx_at=idx_at+1
         r_new=mols(i_mol)%r_at(i_at,:) + t

         line='' 
         write(line(1:6),"(a6)") "ATOM  "
         write(line(7:11),"(i5)")  idx_at
         write(line(13:16),"(a4)") mols(i_mol)%name_at(i_at)
         write(line(18:20),"(a3)") mols(i_mol)%name(1:3)
         write(line(23:26),"(i4)") idx_mol

         if (.not.is_pdx) then
            write(line(31:54),"(3(f8.3))") r_new
            write(line(77:78),"(a2)")  adjustr(trim(mols(i_mol)%name_el(i_at)))            
         else
            line2=line
            write(line2(31:54),"(3(f8.3))") r_new
            write(line2(77:78),"(a2)")  adjustr(trim(mols(i_mol)%name_el(i_at)))
            write(line(31:80),"(3(f13.6))") r_new
            write(11,"(a80)") line2

         endif
            write(10,"(a80)") line
            

      enddo
   enddo


enddo
enddo
enddo

close(10)
close(11)
write(*,*) " Supercell wrote to ",adjustl(fname_rep)
write(*,*)

end subroutine write_supercell

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine write_pdb_conect(fid) 
  implicit none
  integer   :: fid,i
  character(len=80) :: line
  logical   :: nonrep

!COLUMNS       DATA TYPE       FIELD         DEFINITION
!-------------------------------------------------------
! 1 -  6       Record name     "CONECT"
! 7 - 11       Integer         serial        Atom serial number
!12 - 16       Integer         serial        Serial number of bonded atom
!17 - 21       Integer         serial        Serial number of bonded atom
!22 - 26       Integer         serial        Serial number of bonded atom
!27 - 31       Integer         serial        Serial number of bonded atom

do i=1,n_conn
   line=''
   write(line(1:6),"(a6)") "CONECT"
   write(line(7:16),"(2(i5))") connectivity(i,1:2)

   nonrep=(connectivity(i,3).eq.0).and.(connectivity(i,4).eq.0) &
        .and.(connectivity(i,5).eq.0)

   if  (nonrep)   write(fid,"(a80)") line

enddo

end subroutine write_pdb_conect

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine write_pdb_cm(fid) 
  implicit none
  integer   :: fid,i
  character(len=80) :: line

!COLUMNS        DATA  TYPE    FIELD        DEFINITION
!-------------------------------------------------------------------------------------
! 1 -  6        Record name   "ATOM  "
! 7 - 11        Integer       serial       Atom  serial number.
!13 - 16        Atom          name         Atom name.
!18 - 20        Residue name  resName      Residue name.
!31 - 38        Real(8.3)     x            Orthogonal coordinates for X in Angstroms.
!39 - 46        Real(8.3)     y            Orthogonal coordinates for Y in Angstroms.
!47 - 54        Real(8.3)     z            Orthogonal coordinates for Z in Angstroms.

do i=1,sys%nmol_tot
   line=''
   
   write(line(1:6),"(a6)") "ATOM  "
   write(line(7:11),"(i5)") i 
   write(line(13:16),"(a4)") "C" 
   write(line(18:20),"(a3)") mols(i)%name(1:3)
   write(line(31:54),"(3(f8.3))") mols(i)%r_cm

   write(fid,"(a80)") line

enddo


end subroutine write_pdb_cm

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine write_pdb_cryst1(fid) 
  implicit none
  integer  :: fid
  character(len=80) :: line
!  1 -  6       Record name    "CRYST1"
!  7 - 15       Real(9.3)      a (Angstroms)
! 16 - 24       Real(9.3)      b (Angstroms)     
! 25 - 33       Real(9.3)      c (Angstroms)     
! 34 - 40       Real(7.2)      alpha (degrees)   
! 41 - 47       Real(7.2)      beta (degrees)    
! 48 - 54       Real(7.2)      gamma (degrees) 

line=''
write(line(1:6),"(a6)") "CRYST1"
write(line(7:54),"(3(f9.3),3(f7.2))") sys%cell

write(fid,"(a80)") line

end subroutine write_pdb_cryst1

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine determine_connectivity(mode,r,r2)
  implicit none
  integer    :: mode  !0=center of mass, 1=interatomic distance
  real(rk)   :: r,d_cm,trasl(3),r2
  integer    :: loop,m1,m2,k,i1,i2,i3
  logical    :: is_nb

! number of connections 
n_conn=0
k=0

do loop=0,1 ! 0=compute n_conn, 1 allocate and assign connectivity
      
   do m1=1,sys%nmol_tot
      
      do m2=m1+1,sys%nmol_tot

         do i1=-sys%active_pbc(1),sys%active_pbc(1)
         do i2=-sys%active_pbc(2),sys%active_pbc(2)
         do i3=-sys%active_pbc(3),sys%active_pbc(3)

            trasl=i1*sys%box(1,:) + i2*sys%box(2,:) + i3*sys%box(3,:)

            is_nb=.false.
            d_cm=norm3(mols(m1)%r_cm - mols(m2)%r_cm - trasl)

            if (mode.eq.0) then
               if (d_cm.le.r) is_nb=.true.
            elseif (mode.eq.1) then
               if (d_cm.le.r2) call check_contact(m1,m2,r,trasl,is_nb)  
            endif 

            
            if (is_nb) then

               if (loop.eq.0) then

                  n_conn=n_conn+1
               elseif(loop.eq.1) then
                  k=k+1
                  connectivity(k,1)=m1
                  connectivity(k,2)=m2
                  connectivity(k,3)=i1
                  connectivity(k,4)=i2
                  connectivity(k,5)=i3

               endif
            
            endif
         
         enddo
         enddo
         enddo

      enddo
   enddo


   if (loop.eq.0) then
      allocate(connectivity(n_conn,5) )
      
   endif

enddo


end subroutine determine_connectivity

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine check_contact(m1,m2,r,t,is_nb)
! This subroutine checks if the m1 and m2 are connected according 
! to criterium of intermolecular atom-atom contacts
  implicit none
  real(rk)   :: d,r,t(3)
  integer    :: m1,m2,i1,i2,is1,is2
  logical    :: is_nb

is_nb=.false.

do i1=1,mols(m1)%n_at
   do i2=1,mols(m2)%n_at

      is1=mols(m1)%spec_idx
      is2=mols(m2)%spec_idx

      if ( (sys%mask(i1,is1).eq.1).and.(sys%mask(i2,is2).eq.1) ) then

         d=norm3(mols(m1)%r_at(i1,:) - mols(m2)%r_at(i2,:) -t)

         if (d.le.r) then
            is_nb=.true.
            return
         endif

      endif

   enddo
enddo


end subroutine check_contact

function shortest_contact(m1,m2,t) result(d_min)
! This function returns the shortest atom-atom contact
! between  m1 and m2 
  implicit none
  real(rk)   :: d,r,t(3),d_min
  integer    :: m1,m2,i1,i2,is1,is2

d_min=1000.0

do i1=1,mols(m1)%n_at
   do i2=1,mols(m2)%n_at

      is1=mols(m1)%spec_idx
      is2=mols(m2)%spec_idx

      if ( (sys%mask(i1,is1).eq.1).and.(sys%mask(i2,is2).eq.1) ) then

         d=norm3(mols(m1)%r_at(i1,:) - mols(m2)%r_at(i2,:) -t)

         if (d.le.d_min) then
            d_min=d
         endif

      endif

   enddo
enddo


end function shortest_contact


!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine read_pdb 
! This subroutine reads a pdb file and load data into memory
  implicit none 


write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Read pdb file and load data into memory '
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*) 

write(*,*) ' Insert pdb filename:'
read (*,*) sys%fname_pdb   ! input pdb
write(*,*) ' Value read: ',trim(sys%fname_pdb)
write(*,*) ' '

call system_input 

call molecules_input

pdb_ok=.true.

end subroutine read_pdb

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


subroutine read_pdx
! This subroutine reads a pdb file and load data into memory
  implicit none 
    logical       :: w_pdb
    character(80) :: fn_out

write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Read pdx file and load data into memory '
write(*,*) ' A pdx is the same of a pdx for the first 30 colums but'
write(*,*) ' then cartesian coordinates follow in free format for '
write(*,*) ' higher precision'
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*) 
is_pdx=.true.

write(*,*) ' Insert pdx filename:'
read (*,*) sys%fname_pdb   ! input pdb
write(*,*) ' Value read: ',trim(sys%fname_pdb)
write(*,*) ' '


call system_input 

call molecules_input

pdb_ok=.true.


write(*,*) ' Do you want to write an equivalent pdb file? '
read(*,*)  w_pdb
write(*,*)  ' Value read: ', w_pdb


if (w_pdb) then

   fn_out=trim(adjustl(sys%fname_pdb))//'.pdb'
   open(file=fn_out,unit=10,status="replace")

   call write_pdb_full(10)

   close(10)
   write(*,*) "Equivalent pdb file wrote to ",adjustl(fn_out)
   write(*,*)

endif



end subroutine read_pdx

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine molecules_input
  implicit none 

  integer       :: i,stat,i_mol,i_at,spec_idx,line_length
  logical       :: pdbexists
  character(4)  :: resname,atlab
  real(rk)      :: com(3),m_at,m_mol
  character(len=80) :: line,line2
  character(len=80) :: pdb_atom(sys%nat_tot)

allocate(mols(sys%nmol_tot))
!allocate(pdb_atom(sys%nmol_tot))


! load into memory the ATOM recors of the pdb
open(file=sys%fname_pdb,unit=1,iostat=stat,status="old")
line=''
i=0
do while(stat==0 .and. line(1:3)/="TER" .and. line(1:3)/="END" )
   read(1,linefmt,iostat=stat) line
   if (stat==0 .and. ( line(1:3)=="ATO" .or. line(1:3)=="HET" )) then
      i=i+1
      pdb_atom(i)=line
!      write(*,"(a80)")  pdb_atom(i)
   endif
enddo
close(1)


!write(*,*)  'here'
!write(*,*) sys%nmol_tot

i=0
do i_mol=1,sys%nmol_tot
   spec_idx=sys%spec_idx(i_mol)

   mols(i_mol)%spec_idx=spec_idx           ! redundant
   mols(i_mol)%n_at=sys%nat_spec(spec_idx) ! redundant

   mols(i_mol)%r_at=0.0_rk
   mols(i_mol)%name_at(:)=''
   mols(i_mol)%name_el(:)=''
   mols(i_mol)%r_cm=0.0_rk
   mols(i_mol)%occ=0.0_rk
   mols(i_mol)%tfc=0.0_rk

   do i_at=1,sys%nat_spec(spec_idx)
      i=i+1

      line=pdb_atom(i)
      line_length=len(trim(line))


      if (i_at.eq.1) then

         read(line,idfmt) mols(i_mol)%idx_pdb

         read(line,resnamefmt) mols(i_mol)%name ! redundant
         mols(i_mol)%name=adjustl(mols(i_mol)%name)

         ! check
         if (mols(i_mol)%name.ne.sys%mol_names(spec_idx)) then
            write(*,*) 
            write(*,*) "E/ORROR: molecules names do not match! "
            call stop_exec
         endif
      endif


      read(line,atnamefmt) mols(i_mol)%name_at(i_at)
      mols(i_mol)%name_at(i_at)=adjustl(mols(i_mol)%name_at(i_at))
      
      if (.not.(is_pdx)) then
         read(line,coorfmt) mols(i_mol)%r_at(i_at,:)
      
         if (line_length.ge.78) then
            read(line,elnamefmt) mols(i_mol)%name_el(i_at)
!2clear            write(14,*)  mols(i_mol)%name_el(i_at)
            mols(i_mol)%name_el(i_at)=adjustl(mols(i_mol)%name_el(i_at))

         elseif (line_length.ge.66) then
            read(line,tfcfmt) mols(i_mol)%tfc(i_at)
         elseif (line_length.ge.60) then
            read(line,occfmt) mols(i_mol)%occ(i_at)
         endif

         else
            line2=line(31:80)
            read(line2,*)  mols(i_mol)%r_at(i_at,:)

      endif
      mols(i_mol)%r_cm=mols(i_mol)%r_cm + mols(i_mol)%r_at(i_at,:)
      
   enddo

   mols(i_mol)%r_cm=mols(i_mol)%r_cm/sys%nat_spec(sys%spec_idx(i_mol))

enddo


end subroutine molecules_input

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine  wrong_input
  implicit none 

  write(*,*) 
  write(*,*) "E/ORROR: input value not contemplated! "
  call stop_exec

end subroutine wrong_input

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine system_input
! This subroutine reads the pdb file and extract 
! the info about the system, editing the system structure
  implicit none 
  !type(sys_str),intent(out) :: sys !  needed??? ccc
  !character(30),intent(out)     :: pdbfile ccc
  integer :: i,strlen,j
  character(80) :: line
  logical       :: pdbexists,isnew,isok,cell_found
  integer       :: stat,id,last_id,nat_mol,this_mol_type,nat_chk
  character(4)  :: resname



strlen=len(trim(sys%fname_pdb))
sys%fname_root=adjustl(sys%fname_pdb)
sys%fname_root=sys%fname_root(1:strlen-4)

sys%n_spec=0
sys%nat_tot=0
sys%nmol_tot=0
sys%at_per_mol_max=0
sys%mol_names=""
sys%nat_spec=0
sys%nmol_spec=0
sys%spec_idx=0
sys%cell=0.0_rk
sys%box=0.0_rk
sys%active_pbc=0
sys%mask=1

inquire(file=sys%fname_pdb,exist=pdbexists)
last_id=0
nat_mol=0
this_mol_type=1

if (pdbexists) then
   open(file=sys%fname_pdb,unit=1,iostat=stat,status="old")
   line=""

   ! read pbc box if present
   cell_found=.false.
   do j=1,10
      if (stat==0) read(1,linefmt,iostat=stat) line

      if ( stat==0 .and. line(1:3)=="CRY" ) then
         call read_cell(line)
         cell_found=.true.
      endif
   enddo

   if ( .not.cell_found ) then
      write(*,*) 
      write(*,*) 'WARNING: simulation cell not found!'
      write(*,*)
   endif

      
   ! scan pbc to read atomic/molecular data
   rewind(1)
   stat=0
   line(1:3)='ATO'
   do while(stat==0 .and. line(1:3)/="TER" .and. line(1:3)/="END" )
      read(1,linefmt,iostat=stat) line

      if (stat==0 .and. ( line(1:3)=="ATO" .or. line(1:3)=="HET" )) then
         sys%nat_tot=sys%nat_tot+1
         nat_mol=nat_mol+1
         read(line,idfmt) id

         if (id.ne.last_id) then ! new molecule found
!            write(*,*) 'new ',id
!            write(*,*) line(1:30)           

            last_id=id
            sys%nmol_tot=sys%nmol_tot+1
            sys%nat_spec(this_mol_type)=nat_mol
            nat_mol=0
            
            ! is the molecule a new chemical specie?
            read(line,resnamefmt) resname 

!            write(*,*) id,resname !iop

            isnew=.true.
            do i=1,n_spec_max
               if (adjustl(resname).eq.sys%mol_names(i)) then
                  isnew=.false.
                  this_mol_type=i
                  exit
               endif
            enddo

            if (isnew) then
               sys%n_spec=sys%n_spec+1
               sys%mol_names(sys%n_spec)=adjustl(resname)
               this_mol_type=sys%n_spec

            endif
            sys%nmol_spec(this_mol_type)=sys%nmol_spec(this_mol_type)+1
            sys%spec_idx(sys%nmol_tot)=this_mol_type

!            write(15,*) sys%spec_idx(sys%nmol_tot)
!            write(*,*) this_mol_type,sys%nmol_spec(this_mol_type)
         endif
      else  ! if the last molecule is a new specie the number of atoms
            ! must be set explicitely
         sys%nat_spec(this_mol_type)=nat_mol+1
      endif

   enddo

   close(1)     
else
   write(*,*) "E/ORROR:  pdb file "//sys%fname_pdb//" not found"
   call stop_exec
endif
sys%at_per_mol_max=maxval(sys%nat_spec(:))


! LAST CHECK
isok=.true.
if (sum(sys%nmol_spec(1:sys%n_spec)).ne.sys%nmol_tot ) isok=.false.

nat_chk=0
do i=1,sys%n_spec
   nat_chk=nat_chk+sys%nmol_spec(i)*sys%nat_spec(i)
enddo
if (nat_chk.ne.sys%nat_tot)  isok=.false.

! OUTPUT 
if (.not.isok)  then
   write (*,*) ' E/ORROR in subroutine system_input:'
   write(*,"(3x,a28,i12)") 'Total number of atoms: ',sys%nat_tot
   write(*,"(3x,a28,i12)") 'Total number of molecules: ',sys%nmol_tot   
   write(*,"(a8,a8,2x,a8,2x,a14)") 'MolId','MolName','# mol.','atoms per mol.'
   do i=1,sys%n_spec
      write(*,"(i8,a8,2x,i8,2x,i14)") i,sys%mol_names(i),sys%nmol_spec(i),sys%nat_spec(i)
   enddo
   call stop_exec
else
   write (*,*) " System IO from  "//trim(sys%fname_pdb)//"  terminated correctly"
   write (*,*) ' '
   write(*,"(3x,a28,i12)") 'Total number of atoms: ',sys%nat_tot
   write(*,"(3x,a28,i12)") 'Total number of molecules: ',sys%nmol_tot   
   write (*,*) ' '
   write(*,"(a8,a8,2x,a8,2x,a14)") 'MolId','MolName','# mol.','atoms per mol.'
   do i=1,sys%n_spec
      write(*,"(i8,a8,2x,i8,2x,i14)") i,sys%mol_names(i),sys%nmol_spec(i),sys%nat_spec(i)
   enddo
   write (*,*) ' '
endif

end subroutine system_input

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine  read_cell(l)
  implicit none
  character(80) :: l,bau

!  1 -  6       Record name    "CRYST1"
!  7 - 15       Real(9.3)      a (Angstroms)
! 16 - 24       Real(9.3)      b (Angstroms)     
! 25 - 33       Real(9.3)      c (Angstroms)     
! 34 - 40       Real(7.2)      alpha (degrees)   
! 41 - 47       Real(7.2)      beta (degrees)    
! 48 - 54       Real(7.2)      gamma (degrees) 

read(l,"(6x,3(f9.3),3(f7.2))")  sys%cell
read(l(7:54),*)  sys%cell
!write(*,*) l(1:6)
!write(*,*) sys%cell

sys%box=cell2box(sys%cell)


write(*,*)
write(*,*) ' PBC cell read from pdb! '
write(*,*)
write(*,*) ' Values read (a,b,c,alpha,beta,gama):'
write(*,"(6(1x,f9.3))") sys%cell
write(*,*)
write(*,*) ' Cell vectors (by rows): '
write(*,"(1x,3(1x,f10.4))") sys%box(1,:)
write(*,"(1x,3(1x,f10.4))") sys%box(2,:)
write(*,"(1x,3(1x,f10.4))") sys%box(3,:)
write(*,*)
cell_ok=.true.
  
  !pbc_box(3,3)
end subroutine read_cell
      
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine start_banner
!                0         1         2         3         4         5         6         7         
!                12345678901234567890123456789012345678901234567890123456789012345678901234567890
write(*,"(80a)") ' ' 
write(*,"(80a)") ' '
write(*,"(80a)") '   Welcome to Mr Wolf '
write(*,"(80a)") '                               ...I solve problems!'
write(*,"(80a)") ' '
write(*,"(80a)") ' '
write(*,"(80a)") '  Mr Wolf is a suite of routines for the manipulation of pdb files '
write(*,"(80a)") '  and the input/output management in multiscale simulations. '
write(*,"(80a)") ' '
write(*,"(80a)") ' '
write(*,"(80a)") '  Copyright by Gabriele D'//achar(39)//'Avino 2015'
write(*,"(80a)") '  gabriele.davino@gmail.com'
write(*,"(80a)") ' '
write(*,"(80a)") '  Any kind of use of this code requires the explicit consent by the author.'
write(*,"(80a)") ' '
write(*,"(80a)") ' ======================================================================= '
write(*,"(80a)") ' ======================================================================= '
write(*,"(80a)") ' '

end subroutine start_banner

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine stop_banner

  write(*,"(80a)") ' '
  write(*,"(80a)") '   ==> Execution finished, bye ;) '
  write(*,"(80a)") ' '
  write(*,"(80a)") ' '

end  subroutine stop_banner

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine stop_exec
  write(*,*) 
  write(*,*) " ...execution stopped!  "
  stop
end subroutine stop_exec

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine stop_pdb

if (.not.pdb_ok) then
   write(*,*) 'E/ORROR: pdb has to read before! '
   call stop_exec
endif

end subroutine stop_pdb

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine stop_pbc

if (.not.cell_ok) then

   write(*,*) 'E/ORROR: crystal cell has not been read!'
   call stop_exec

endif

end subroutine stop_pbc

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine disk_pen_pfp  ! hidden (
  implicit none
  integer   :: i,j,sel,sel2,idx0(2),n_rep(3),idx_mol,idx_at,i_mol,i_at,ml
  integer   :: i1,i2,i3,rl,idx_mol2,null_dim,idx_sel
  character(80) :: fname_sel,line,fn_list,out,line2
  character(len=4) :: rname_new(2)
  real(rk)      :: r0(3),Rc,t(3),r_new(3),d0(3),Lbox(3)
  logical       :: is_in,is_2write,write_V0F0,keep_idx
  character(1)     :: ml_char
  character(3)     :: R_char


read(*,*) ml,Rc

write(ml_char,'(i1)') ml
write(R_char,'(i3)') int(Rc)
out='A5F5_HOPG_-1-21x'//ml_char//'_R'//R_char//'.pdb'

open(file=out,unit=10,status="replace")

sel=1 ! molecular centroid
r0= (/ 5.8,  3.2,  31.1 /)

sel2=1 ! centroid criterium

sys%active_pbc(1)=1
sys%active_pbc(2)=1
sys%active_pbc(3)=0
null_dim=3


n_rep=0
do i=1,3
   if (sys%active_pbc(i).eq.1) then
      n_rep(i)=ceiling(2.00*Rc/norm3(sys%box(i,:)))
   endif
enddo


idx_mol=0
idx_at=0

do i_mol=1,sys%nmol_tot

   idx_mol=idx_mol + 1

   do i_at=1,mols(i_mol)%n_at
            
      idx_at=idx_at+1
      r_new=mols(i_mol)%r_at(i_at,:) 

      idx_mol2=idx_mol
      line='' 
      write(line(1:6),"(a6)") "ATOM  "
      write(line(7:11),"(i5)")  idx_at
      write(line(13:16),"(a4)") mols(i_mol)%name_at(i_at)
      write(line(18:20),"(a3)") mols(i_mol)%name(1:3)
      write(line(23:26),"(i4)") idx_mol2
      write(line(31:54),"(3(f8.3))") r_new
      write(line(77:78),"(a2)")  adjustr(trim(mols(i_mol)%name_el(i_at)))

      write(10,"(a80)") line

   enddo

enddo




do i3=-n_rep(3),n_rep(3)
do i2=-n_rep(2),n_rep(2)
do i1=-n_rep(1),n_rep(1)
if ( .not. ( (i1.eq.0) .and. (i2.eq.0) .and. (i3.eq.0) ) ) then
   
   t=i1*sys%box(1,:) + i2*sys%box(2,:) + i3*sys%box(3,:)


   do i_mol=1,sys%nmol_tot

      ! determine if the molecule is inside the selection
      is_in=.false.


      if (sel2.eq.1) then ! centroid criterium
         
         d0=r0 - (mols(i_mol)%r_cm + t)
         do j=1,3
            if (null_dim.eq.j) d0(j)=0.0_rk
         enddo

         if ( norm3(d0).le.Rc ) is_in=.true.
         
      endif


      ! determine if the molecule has to be written or not
      is_2write=.false.
      if ( (is_in).and.(sel.eq.1) ) then
         is_2write=.true.
      
      elseif ( (is_in).and.( (sel.eq.2) .or.(sel.eq.3) ) ) then
         is_2write=.true.
         
         ! the one/two central molecule(s) were already written 
         do j=1,sel-1
            if ( (norm3(t).le.1e-6).and.(i_mol.eq.idx0(j)) ) is_2write=.false.
         enddo 

      endif

!      write(12,*) '===',i_mol,mols(i_mol)%r_cm,is_in,is_2write 

      ! then write it!
      if (is_2write) then

         idx_mol=idx_mol+1
         
!         write(11,"(4(i5,1x),3(f10.4,1x))") idx_mol,i_mol,mols(i_mol)%idx_pdb,mols(i_mol)%spec_idx,mols(i_mol)%r_cm
         
         do i_at=1,mols(i_mol)%n_at
            
            idx_at=idx_at+1
            r_new=mols(i_mol)%r_at(i_at,:) + t

            idx_mol2=idx_mol
            if (idx_mol.ge.10000) idx_mol2=idx_mol-10000*floor(real(idx_mol)/10000.0_rk)
            line='' 
            write(line(1:6),"(a6)") "ATOM  "
            write(line(7:11),"(i5)")  idx_at
            write(line(13:16),"(a4)") mols(i_mol)%name_at(i_at)
            write(line(18:20),"(a3)") mols(i_mol)%name(1:3)
            write(line(23:26),"(i4)") idx_mol2
            write(line(31:54),"(3(f8.3))") r_new
            write(line(77:78),"(a2)")  adjustr(trim(mols(i_mol)%name_el(i_at)))

            write(10,"(a80)") line

         enddo
      endif
      
   enddo


endif
enddo
enddo
enddo

end subroutine disk_pen_pfp

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine write_connectivity_mod
  implicit none
  integer        :: i,conn_mode,stat,sel,i_m1,i_m2,i_at
  real(rk)       :: Rc,dist_cm,tv(3),d_min,Rc2
  character(50)  :: fn_out
  logical        :: write_dim

write(*,*) 
write(*,*) ' -------------------------------------------'
write(*,*) ' You choose option ', option
write(*,*) ' Determine intermolecular connectivity'
write(*,*) ' -------------------------------------------'
write(*,*) 
write(*,*) 
call stop_pdb

!!!!!! connectivity log file
write(*,"(a)") ' Please specify:'
write(*,"(a)") '   -Criterium to be used (0=center of mass, 1=interatomic distance) ' 
write(*,"(a)") '   -Cutoff distance (Angstrom)'
write(*,"(a)") '   -Coarse cutoff (Angstrom)'
write(*,"(a)") ' Note: The last input is revelant only for interatomic distance criterium. '
write(*,"(a)") '       A safe value is 1.5x the leading molecular dimension'
read(*,*) conn_mode,Rc,Rc2
write(*,*) ' Values read: ',conn_mode,Rc,Rc2
write(*,*)

if ( .not. ((conn_mode.eq.0).or.(conn_mode.eq.1) )) then
   call wrong_input
endif

write(*,"(a)") ' Should I use PBC to determine the connectivity? (0=NO, 1=YES)'
write(*,*)     ' Specify 3 values for the three cell axes (eg 0 0 0 = no PBC)'
read(*,*)   sys%active_pbc
write(*,*)  ' Values read:',sys%active_pbc
write(*,*)

do i=1,3
   if ( .not. ( (sys%active_pbc(i).eq.0).or. (sys%active_pbc(i).eq.1) ) ) then
      call wrong_input
   endif
enddo

call check_pbc

call determine_connectivity_mod(conn_mode,Rc,Rc2)

fn_out=trim(sys%fname_root)//"_con.log"
open(file=fn_out,unit=10,status="replace")
write(10,"(a)") '% 1.idx_1  2.idx_2  3.rep_1  4.rep_2  5.rep_3  6.specie_1  7.specie_2'
write(10,"(a)") '%  8.resid_pdb_1  9.resid_pdb_2  10.dist_cm  11.dist_min'

do i=1,n_conn

   tv=connectivity(i,3)*sys%box(1,:) + connectivity(i,4)*sys%box(2,:) + &
      connectivity(i,5)*sys%box(3,:)
   dist_cm=Norm3( mols(connectivity(i,1))%r_cm - mols(connectivity(i,2))%r_cm - tv)

   d_min=shortest_contact(connectivity(i,1),connectivity(i,2),tv)
   
   write(10,"(9(i8,1x),2(f10.4),1x)") connectivity(i,:), &
        mols(connectivity(i,1))%spec_idx,mols(connectivity(i,2))%spec_idx, &
        mols(connectivity(i,1))%idx_pdb,mols(connectivity(i,2))%idx_pdb,&
        dist_cm,d_min
enddo

close(10)
write(*,*) " Molecular connectivity log file wrote to ",adjustl(fn_out)
write(*,*) 


! intermolecular connectivity pdb

fn_out=trim(sys%fname_root)//"_con.pdb"
open(file=fn_out,unit=10,status="replace")

write(10,"(a)") "REMARKS  connections across pbc are not reported "

call write_pdb_cryst1(10) 

call write_pdb_cm(10)

call write_pdb_conect(10)

close(10)
write(*,*) " Molecular connectivity pdb wrote to ",adjustl(fn_out)
write(*,*) 

connectivity_ok=.true.

end subroutine write_connectivity_mod

! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine determine_connectivity_mod(mode,r,r2)
  implicit none
  integer    :: mode  !0=center of mass, 1=interatomic distance
  real(rk)   :: r,d_cm,trasl(3),r2,r_cm(3)
  integer    :: loop,m1,m2,k,i1,i2,i3
  logical    :: is_nb

! number of connections 
n_conn=0
k=0

do loop=0,1 ! 0=compute n_conn, 1 allocate and assign connectivity
      
   do m1=1,sys%nmol_tot
      
      do m2=m1+1,sys%nmol_tot

         do i1=-sys%active_pbc(1),sys%active_pbc(1)
         do i2=-sys%active_pbc(2),sys%active_pbc(2)
         do i3=-sys%active_pbc(3),sys%active_pbc(3)

            trasl=i1*sys%box(1,:) + i2*sys%box(2,:) + i3*sys%box(3,:)

            is_nb=.false.
            r_cm=mols(m1)%r_cm - mols(m2)%r_cm - trasl
            d_cm=norm3(r_cm)
       

            if (mode.eq.0) then

!               if (d_cm.le.r) is_nb=.true.
               if ( (d_cm.le.r) .and. ( abs(r_cm(1)).gt.(0.8000*d_cm) ) ) is_nb=.true.

!               if ( r_cm(1).lt.(0.9000*d_cm) ) write(*,*) r_cm !is_nb=.false.
!               if ( r_cm(1).lt.(0.6000*d_cm) ) then
!                  write(*,"(3(f10.3))") r_cm
!                  is_nb=.false.
!               endif
               

            elseif (mode.eq.1) then
               if (d_cm.le.r2) call check_contact(m1,m2,r,trasl,is_nb)  
            endif 

            
            if (is_nb) then

               if (loop.eq.0) then

                  n_conn=n_conn+1
               elseif(loop.eq.1) then
                  k=k+1
                  connectivity(k,1)=m1
                  connectivity(k,2)=m2
                  connectivity(k,3)=i1
                  connectivity(k,4)=i2
                  connectivity(k,5)=i3

               endif
            
            endif
         
         enddo
         enddo
         enddo

      enddo
   enddo


   if (loop.eq.0) then
      allocate(connectivity(n_conn,5) )
      
   endif

enddo


end subroutine determine_connectivity_mod


end module io
