module math
  ! Mathematic tools
  use types
  implicit none

contains


function rotmat3D(angle,axis) result(R)  
! GD 19/02/16:  adapted from matlab function rotationmat3D (Bileschi 2009)
! 
! creates a rotation matrix such that R * x 
! operates on x by rotating x around the origin r radians around line
! connecting the origin to the point "Axis"
!
! example:
! rotate around a random direction a random amount and then back
! the result should be an Identity matrix
!
!r = rand(4,1);
!rotationmat3D(r(1),[r(2),r(3),r(4)]) * rotationmat3D(-r(1),[r(2),r(3),r(4)])
!
! example2: 
! rotate around z axis 45 degrees
! Rtest = rotationmat3D(pi/4,[0 0 1])
  implicit none
  real(rk)    :: angle,axis(3),R(3,3)
  real(rk)    :: u,v,w,u2,v2,w2,c,s

axis=axis/norm3(axis)
u=axis(1)
v=axis(2)
w=axis(3)
u2=u**2;
v2=v**2;
w2=w**2;
c=cos(angle);
s=sin(angle);

R=0.0_rk
R(1,1)= u2 + (v2 + w2)*c;
R(1,2)= u*v*(1-c) - w*s;
R(1,3)= u*w*(1-c) + v*s;
R(2,1)= u*v*(1-c) + w*s;
R(2,2)= v2 + (u2+w2)*c;
R(2,3)= v*w*(1-c) - u*s;
R(3,1)= u*w*(1-c) - v*s;
R(3,2)= v*w*(1-c)+u*s;
R(3,3)= w2 + (u2+v2)*c;

end function rotmat3D

! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function frac2cart(coor_frac) result(coor_cart)
  implicit none
  real(rk)    :: coor_frac(3),coor_cart(3)
  real(rk)    :: v,a,b,c,alp,bet,gam,M(3,3)

a=sys%cell(1)
b=sys%cell(2)
c=sys%cell(3)
alp=deg2rad*sys%cell(4)
bet=deg2rad*sys%cell(5)
gam=deg2rad*sys%cell(6)

v=sqrt(1-cos(alp)**2-cos(bet)**2-cos(gam)**2 +2*cos(alp)*cos(bet)*cos(gam))

M=0.0_rk

M(1,1)=a
M(1,2)=b*cos(gam) 
M(1,3)=c*cos(bet)
M(2,2)=b*sin(gam)
M(2,3)=(c/sin(gam))*(cos(alp)-cos(bet)*cos(gam))
M(3,3)=c*v/sin(gam)

coor_cart=matmul(M,coor_frac)

end function frac2cart

! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cart2frac(coor_cart) result(coor_frac)
  implicit none
  real(rk)    :: coor_frac(3),coor_cart(3)
  real(rk)    :: v,a,b,c,alp,bet,gam,M(3,3)

a=sys%cell(1)
b=sys%cell(2)
c=sys%cell(3)
alp=deg2rad*sys%cell(4)
bet=deg2rad*sys%cell(5)
gam=deg2rad*sys%cell(6)

v=sqrt(1-cos(alp)**2-cos(bet)**2-cos(gam)**2 +2*cos(alp)*cos(bet)*cos(gam))

M=0.0_rk

M(1,1)=1/a
M(1,2)=-cos(gam)/(a*sin(gam))
M(1,3)=(cos(alp)*cos(gam)-cos(bet))/(a*v*sin(gam))
M(2,2)=1/(b*sin(gam))
M(2,3)=(cos(bet)*cos(gam)-cos(alp))/(b*v*sin(gam))
M(3,3)=sin(gam)/(c*v)

coor_frac=matmul(M,coor_cart)

end function cart2frac

! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function cell2box(c) result(box)
  implicit none
  real(rk)    :: c(6),cell(6),box(3,3)
  
  cell=c
  cell(4:6)=deg2rad*cell(4:6)

  box=0.0_rk

  box(1,1)=cell(1)

  box(2,1)=cell(2)*cos(cell(6));
  box(2,2)=cell(2)*sin(cell(6));

  box(3,1)=cell(3)*cos(cell(5));
  box(3,2)=cell(3)*(cos(cell(4))-cos(cell(6))*cos(cell(5)))/sin(cell(6));
  box(3,3)=sqrt(cell(3)**2-box(3,1)**2-box(3,2)**2);
  
end function cell2box

! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function box2cell(box) result(cell)
  implicit none
  real(rk)    :: cell(6),box(3,3)

cell=0.0_rk

cell(1)=norm3(box(1,:))
cell(2)=norm3(box(2,:))
cell(3)=norm3(box(3,:))


cell(4)=rad2deg*acos(DotProd3(box(2,:),box(3,:))/(cell(2)*cell(3)) );
cell(5)=rad2deg*acos(DotProd3(box(1,:),box(3,:))/(cell(1)*cell(3)));
cell(6)=rad2deg*acos(DotProd3(box(1,:),box(2,:))/(cell(1)*cell(2)));

! GD18/10/17 BUG fixed
!cell(4)=rad2deg*acos(DotProd3(box(2,:),box(3,:))/(cell(3)));
!cell(5)=rad2deg*acos(DotProd3(box(1,:),box(3,:))/(cell(3)));
!cell(6)=rad2deg*acos(DotProd3(box(1,:),box(2,:))/(cell(2)));

end function box2cell


function uptriangle_2_mat3(upt)
! returns full 3x3 symemtric matrix from upper triangle format
  implicit none
  real(rk)    :: upt(6)
  real(rk)    :: uptriangle_2_mat3(3,3)

uptriangle_2_mat3(1,1)=upt(1)
uptriangle_2_mat3(1,2)=upt(2)
uptriangle_2_mat3(2,1)=upt(2)
uptriangle_2_mat3(1,3)=upt(3)
uptriangle_2_mat3(3,1)=upt(3)
uptriangle_2_mat3(2,2)=upt(4)
uptriangle_2_mat3(2,3)=upt(5)
uptriangle_2_mat3(3,2)=upt(5)
uptriangle_2_mat3(3,3)=upt(6)

end function uptriangle_2_mat3

! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function mat3_2_uptriangleup(mat)
! returns  upper triangle format from full 3x3 symemtric matrix
  implicit none
  real(rk)    :: mat(3,3)
  real(rk)    :: mat3_2_uptriangleup(6)

  mat3_2_uptriangleup(1)=mat(1,1)
  mat3_2_uptriangleup(2)=0.50_rk*(mat(1,2)+mat(2,1))
  mat3_2_uptriangleup(3)=0.50_rk*(mat(1,3)+mat(3,1))
  mat3_2_uptriangleup(4)=mat(2,2)
  mat3_2_uptriangleup(5)=0.50_rk*(mat(2,3)+mat(3,2))
  mat3_2_uptriangleup(6)=mat(3,3)

end function mat3_2_uptriangleup

! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine find_rotU(U,O,P1,P2)
  implicit none

  real(rk),intent(in)    :: O(3),P2(3),P1(3)
  real(rk),intent(out)   :: U(3,3)
  
  real(rk)      :: v1(3),v2(3),u1(3),u2(3),u3(3)

  v1=P1-O
  u1=v1/Norm3(v1)

  v2=P2-O
  u3=VectorProd3(u1,v2)
  u3=u3/Norm3(u3)
  u2=VectorProd3(u3,u1)

  U(:,1)=u1
  U(:,2)=u2
  U(:,3)=u3
 end subroutine find_rotU

! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
subroutine Diagonalize3(matrix,evalues,evectors)
  ! simply an interface to RS
  real(rk),intent(in)  :: matrix(3,3)
  real(rk),intent(out) :: evalues(3),evectors(3,3)
  real(rk)             :: w1(3),w2(3)
  integer :: error
    
call RS(3,3,matrix,evalues,1,evectors,w1,w2,error) 
if (error/=0) write(*,*) ' E/Orror in subroutine Diagonalize3: diagonalization failed!!!'

end subroutine Diagonalize3

! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

SUBROUTINE InvMat(matrix, inverse, n, errorflag)
!Subroutine to find the inverse of a square matrix
!Author : Louisda16th a.k.a Ashwith J. Rego
!Reference : Algorithm has been well explained in:
!http://math.uww.edu/~mcfarlat/inverse.htm           
!http://www.tutor.ms.unimelb.edu.au/matrix/matrix_inverse.html
	IMPLICIT NONE
	!Declarations
	INTEGER, INTENT(IN) :: n
	INTEGER, INTENT(OUT) :: errorflag  
                      !Return error status. -1 for error, 0 for normal
	REAL(rk), INTENT(IN), DIMENSION(n,n) :: matrix  !Input matrix
	REAL(rk), INTENT(OUT), DIMENSION(n,n) :: inverse !Inverted matrix
	
	LOGICAL :: FLAG = .TRUE.
	INTEGER :: i, j, k, l
	REAL(rk) :: m
	REAL(rk), DIMENSION(n,2*n) :: augmatrix !augmented matrix
	
	!Augment input matrix with an identity matrix
	DO i = 1, n
		DO j = 1, 2*n
			IF (j <= n ) THEN
				augmatrix(i,j) = matrix(i,j)
			ELSE IF ((i+n) == j) THEN
				augmatrix(i,j) = 1
			Else
				augmatrix(i,j) = 0
			ENDIF
		END DO
	END DO
	
	!Reduce augmented matrix to upper traingular form
	DO k =1, n-1
		IF (augmatrix(k,k) == 0) THEN
			FLAG = .FALSE.
			DO i = k+1, n
				IF (augmatrix(i,k) /= 0) THEN
					DO j = 1,2*n
						augmatrix(k,j) = augmatrix(k,j)+augmatrix(i,j)
					END DO
					FLAG = .TRUE.
					EXIT
				ENDIF
				IF (FLAG .EQV. .FALSE.) THEN
					PRINT*, "Matrix is non - invertible"
					inverse = 0
					errorflag = -1
					return
				ENDIF
			END DO
		ENDIF
		DO j = k+1, n			
			m = augmatrix(j,k)/augmatrix(k,k)
			DO i = k, 2*n
				augmatrix(j,i) = augmatrix(j,i) - m*augmatrix(k,i)
			END DO
		END DO
	END DO
	
	!Test for invertibility
	DO i = 1, n
		IF (augmatrix(i,i) == 0) THEN
			PRINT*, "Matrix is non - invertible"
			inverse = 0
			errorflag = -1
			return
		ENDIF
	END DO
	
	!Make diagonal elements as 1
	DO i = 1 , n
		m = augmatrix(i,i)
		DO j = i , (2 * n)				
			   augmatrix(i,j) = (augmatrix(i,j) / m)
		END DO
	END DO
	
	!Reduced right side half of augmented matrix to identity matrix
	DO k = n-1, 1, -1
		DO i =1, k
		m = augmatrix(i,k+1)
			DO j = k, (2*n)
				augmatrix(i,j) = augmatrix(i,j) -augmatrix(k+1,j) * m
			END DO
		END DO
	END DO				
	
	!store answer
	DO i =1, n
		DO j = 1, n
			inverse(i,j) = augmatrix(i,j+n)
		END DO
	END DO
	errorflag = 0
END SUBROUTINE InvMat

! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function Norm3(r)
  implicit none
  real(rk)  :: r(3)
  real(rk)  :: Norm3

  Norm3=sqrt(r(1)**2+r(2)**2+r(3)**2)
end function Norm3

! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function VectorProd3(p1,p2) RESULT(p3)
  implicit none
  real(rk),dimension(3),intent(in):: p1,p2
  real(rk),dimension(3)           :: p3

  p3(1)=p1(2)*p2(3)-p1(3)*p2(2)
  p3(2)=p1(3)*p2(1)-p1(1)*p2(3)
  p3(3)=p1(1)*p2(2)-p1(2)*p2(1)
end function VectorProd3

! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function DotProd3(p1,p2) RESULT(dp)
  implicit none
  real(rk),dimension(3),intent(in)::p1,p2
  real(rk)   ::dp
  
  dp=p1(1)*p2(1)+p1(2)*p2(2)+p1(3)*p2(3)
end function DotProd3



end  module math
