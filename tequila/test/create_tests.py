#!/usr/bin/env python3
"""
Create test directories from MESCal calculations.
Only requirement: `mescal` executable must be available in $PATH.
"""
import os
import subprocess as sp
from os.path import join, abspath, dirname, isdir
from shutil import copyfile, rmtree

bash = os.popen('command -v bash').read().rstrip()

_alpha = 10.
_chg = 1.0

def popen(cmd):
    return sp.check_output(cmd,shell=True,executable=bash).decode('unicode_escape')

def grep(expr, file):
    return popen(f"grep {expr} {file}")

def write_mescal_mei(name,q,a):
    with open(name,'w') as file:
        file.write('1 1 I I\n')
        file.write(f'1 1 {q} {a}\n')

def write_mescal_input(charge,field,epsilon,pbc,fulldiag,zeta,field_direction='x'):

    write_mescal_mei('neutral.mei',0.0,_alpha)
    write_mescal_mei('charged.mei',_chg,_alpha)

    name = 'mescal.inp'

    with open('mescal.inp','w') as file:
        file.write('JOBTYPE  ME\n')
        file.write('PDB lattice.pdb\n')

        file.write('INPUT_MOL\n')
        file.write(' AAA neutral.mei\n')

        if charge:
            file.write(' XXX charged.mei\n')
        else:
            file.write(' XXX neutral.mei\n')

        file.write('END\n')

        if field or zeta:
            if field_direction == 'x':
                file.write('FEXT_UNI 0.1 0. 0.\n')
            elif field_direction == 'y':
                file.write('FEXT_UNI 0. 0.1 0.\n')
            else:
                file.write('FEXT_UNI 0. 0. 0.1\n')


        if pbc or zeta:
            file.write('PBC 3\n')
            file.write('PBC_BOX\n100. 0. 0.\n0. 100. 0.\n0. 0. 100.\n')
            file.write('END\n')
            file.write('PBC_CUTOFF 500.\n')

        file.write('MAX_ITER 250\n')
        file.write('TOL_ENE 1e-8\n')



def write_tequila_data(charge,isotropic=True):
    x = np.loadtxt('lattice.xyz',skiprows=2,usecols=(1,2,3))
    q = np.zeros(x.shape[0])
    if isotropic:
        a = _alpha * np.ones(x.shape[0])
    else:
        one = np.ones(x.shape[0])
        zero = np.zeros(x.shape[0])
        a = _alpha * np.column_stack([one,zero,zero,one,zero,one])
    if charge:
        q[0] = _chg
    data = np.column_stack([x,q,a])
    np.savetxt('input.dat',data,header=f'{data.shape[0]} {data.shape[1]}')



def write_tequila_input(charge,field,epsilon,pbc,fulldiag,zeta,isotropic):

    write_tequila_data(charge,isotropic)

    with open('tequila.inp','w') as file:
        file.write('INPUT input.dat\n')
        if field:
            file.write('FEXT 0.1 0. 0.\n')
        if zeta:
            file.write('RESPONSE\n')
            file.write('DEPOL\n0.333 0. 0.\n0. 0.333 0.\n 0. 0. 0.333\n')
        if epsilon:
            file.write('EPSILON 3.\n')
        if pbc or zeta:
            file.write('PBC 500.\n')
            file.write('BOX\n100. 0. 0.\n0. 100. 0.\n0. 0. 100.\n')
        if fulldiag:
            file.write('FULLDIAG\n')


calculations = ['charge','field','charge_field','charge_field_fulldiag','charge_field_anisotropic']
calculations += ['pbc_' + i for i in calculations]
calculations += ['zeta','zeta_fulldiag','zeta_epsilon']

for c in calculations:

    # Get parameters from calculation name
    charge = 'charge' in c
    field = 'field' in c
    epsilon = 'epsilon' in c
    pbc = 'pbc' in c
    fulldiag = 'fulldiag' in c
    zeta = 'zeta' in c
    isotropic = 'anisotropic' not in c

    # Create directories with Tequila & Mescal files
    if isdir(c):
        rmtree(c)
    os.mkdir(c)
    copyfile('lattice.pdb',join(c,'lattice.pdb'))
    copyfile('lattice.xyz',join(c,'lattice.xyz'))
    os.chdir(c)

    # Tequila inputs
    write_tequila_input(charge,field,epsilon,pbc,fulldiag,zeta,isotropic)

    # Mescal inputs and executions
    if zeta:

        write_mescal_input(charge,field,epsilon,pbc,fulldiag,zeta,field_direction='x')
        os.rename('mescal.inp','mescal_x.inp')
        popen('mescal mescal_x.inp > mescal_x.out')

        write_mescal_input(charge,field,epsilon,pbc,fulldiag,zeta,field_direction='y')
        os.rename('mescal.inp','mescal_y.inp')
        popen('mescal mescal_y.inp > mescal_y.out')

        write_mescal_input(charge,field,epsilon,pbc,fulldiag,zeta,field_direction='z')
        os.rename('mescal.inp','mescal_z.inp')
        popen('mescal mescal_z.inp > mescal_z.out')

        zeta = grep('g_ZETA','mescal_x.out').split()[-4:-1]
        zeta += grep('g_ZETA','mescal_y.out').split()[-4:-1]
        zeta += grep('g_ZETA','mescal_z.out').split()[-4:-1]
        zeta = np.array([float(i) for i in zeta]).reshape((3,3))
        np.savetxt('mescal_zeta.dat',np.array(zeta))



    # Normal case
    else:

        write_mescal_input(charge,field,epsilon,pbc,fulldiag,zeta)

        # Run mescal
        popen('mescal mescal.inp > mescal.out')

        # Get energies into mescal_energy.dat
        energy = [grep('g_ENERGY','mescal.out').split()[-2]]
        energy += [grep('g_ELECTROSTATIC','mescal.out').split()[-2]]
        energy += [grep('g_INDUCTION','mescal.out').split()[-2]]

        with open('mescal_energy.dat','w') as file:
            file.write(' '.join(energy)+'\n')

    os.chdir('../')
