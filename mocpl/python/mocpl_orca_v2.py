# mocpl_orca.py is a python script for the calculation of one-electron (MO) 
# intermolecular electronic couplings.
# Gabriele D'Avino, October 2015
#
from cclib.parser     import ccopen, ORCA
import sys, glob
import os.path
import numpy as np
from numpy.linalg import eig, inv
from numpy        import zeros, dot, matrix, diag, identity

usage="usage:\n python " +sys.argv[0]+" f1_f2 i1_start i1_end i2_start i2_end LEVEL\n\n  where:\n   -dimer and fragments orca output are named: f1_f2.out f1.out f2.out \n   -i1_start i1_end specify the orbital interval for fragment 1\n   -i2_start i2_end specify the orbital interval for fragment 2 \n   -LEVEL=DFT/SE  \n \n Beware of zero-indexed orbital count! \n"
if len(sys.argv)!=7:
    sys.exit(usage)

base_name = os.path.join(str(sys.argv[1])) 
frag_name = base_name.split("_")

def clean_orca_out( fname ):
# this function prevents issues in parsing due to wrong 
# format in ORCA outputs 
    in_scf=0
    in_iter=0
    lines = open(fname).readlines()
    out = open("tmp.out", "w")

    for l in lines:
        
        if l.startswith("SCF ITERATIONS"):
            in_scf=1
        elif l.startswith("TOTAL SCF"):
            in_scf=0

        # check if it the case to modify the line!
        if not len(l.strip()) == 0 : # line is not  empty 

            if in_scf:
                if l.startswith("ITER") and in_iter:
                    in_iter=0
                elif l.startswith("ITER") and not in_iter:
                    in_iter=1

                if in_iter and l[2].isdigit():
                    # this is a line with possible errors!
                    l2=l
                    l=l2[:19]+"  "+l2[21:36]+"  "+l2[38:47]+"  "+l2[49:59]+"  "+l2[61:70]+"  "+l2[72:]

        out.write(l)
    
    out.close()
    return




### READING ORCA OUTPUTS

# Fragment1

#orcaOut = ORCA(os.path.join( frag_name[0] + ".out" ) )
clean_orca_out( frag_name[0] + ".out" )
orcaOut = ORCA('tmp.out')
orcaData= orcaOut.parse()
mo_f1=orcaData.mocoeffs[0].T
sz_f1=mo_f1.shape[0]

# Fragment2
#orcaOut = ORCA(os.path.join( frag_name[1] + ".out" ) )
clean_orca_out( frag_name[1] + ".out" )
orcaOut = ORCA('tmp.out')
orcaData= orcaOut.parse()
mo_f2=orcaData.mocoeffs[0].T
sz_f2=mo_f2.shape[0]

# Dimer
#orcaOut =ORCA(os.path.join( base_name + ".out" ) )
clean_orca_out( base_name + ".out" )
orcaOut = ORCA('tmp.out')
orcaData=orcaOut.parse()
mo_dim=orcaData.mocoeffs[0].T
ene_dim=orcaData.moenergies[0]
S_ao=orcaData.aooverlaps
sz_dim=mo_dim.shape[0]

### DEFINITION OF THE ORBITALS SUBSPACE
i1=np.arange(int(sys.argv[2]) , int(sys.argv[3])+1 )
i2=np.arange(int(sys.argv[4])+sz_f1 , int(sys.argv[5])+sz_f1+1 )
n1=len(i1)
n2=len(i2)
n=n1+n2


### CALCULATION OF ELECTRONIC COUPLINGS

# noninteracting 
mo_f1f2=matrix(zeros([sz_dim,sz_dim]))
mo_f1f2[     :sz_f1 ,      :sz_f1 ]=mo_f1
mo_f1f2[sz_f1:      , sz_f1:      ]=mo_f2


if sys.argv[6]=="SE":

    # H (Fock matrix) on the atomic orbitals basis
    H_ao=dot( dot(mo_dim,diag(ene_dim)) , mo_dim.T )

    # H on the isolated fragments eigenstates basis
    H_fr=dot( dot(mo_f1f2.T,H_ao) ,  mo_f1f2 )
    S_fr=dot( dot(mo_f1f2.T,S_ao) ,  mo_f1f2 )

    # Effective H restricted to the desired subspace
    Heff=matrix(zeros([n,n]))

    Heff[   :n1 ,   :n1 ]=H_fr[ i1[0]:i1[-1]+1 , i1[0]:i1[-1]+1 ] # f1-f1
    Heff[ n1:   , n1:   ]=H_fr[ i2[0]:i2[-1]+1 , i2[0]:i2[-1]+1 ] # f2-f2
    Heff[   :n1 , n1:   ]=H_fr[ i1[0]:i1[-1]+1 , i2[0]:i2[-1]+1 ] # f1-f2
    Heff[ n1:   ,   :n1 ]=H_fr[ i2[0]:i2[-1]+1 , i1[0]:i1[-1]+1 ] # f2-f1

    # S restricted to the desired subspace
    Sres=identity(n)
    Sres[     :n1 , n1:   ]=S_fr[ i1[0]:i1[-1]+1 , i2[0]:i2[-1]+1 ] # f1-f2
    Sres[   n1:   ,   :n1 ]=S_fr[ i2[0]:i2[-1]+1 , i1[0]:i1[-1]+1 ] # f2-f1


elif sys.argv[6]=="DFT":

    # H (Fock matrix) on the atomic orbitals eigenstates basis  
    H_ao= dot( S_ao , dot( mo_dim , dot( diag(ene_dim) , inv(mo_dim) ) ) )

    # H and S matrices on the isoltated fragments basis
    H_fr=dot( dot(mo_f1f2.T,H_ao) ,  mo_f1f2 )
    S_fr=dot( dot(mo_f1f2.T,S_ao) ,  mo_f1f2 )

    # H and S restricted to the desired subspace
    Hres=matrix(zeros([n,n]))
    Hres[   :n1 ,   :n1 ]=H_fr[ i1[0]:i1[-1]+1 , i1[0]:i1[-1]+1 ] # f1-f1
    Hres[ n1:   , n1:   ]=H_fr[ i2[0]:i2[-1]+1 , i2[0]:i2[-1]+1 ] # f2-f2
    Hres[   :n1 , n1:   ]=H_fr[ i1[0]:i1[-1]+1 , i2[0]:i2[-1]+1 ] # f1-f2
    Hres[ n1:   ,   :n1 ]=H_fr[ i2[0]:i2[-1]+1 , i1[0]:i1[-1]+1 ] # f2-f1

    Sres=identity(n)
    Sres[     :n1 , n1:   ]=S_fr[ i1[0]:i1[-1]+1 , i2[0]:i2[-1]+1 ] # f1-f2
    Sres[   n1:   ,   :n1 ]=S_fr[ i2[0]:i2[-1]+1 , i1[0]:i1[-1]+1 ] # f2-f1

    # Effective H between orthogonal states
    (Se,Sv) = eig(Sres)
    Sm12 = dot( Sv , dot( diag(Se**(-0.5)) , Sv.T) )

    Heff=dot( Sm12 , dot( Hres , Sm12) )


else:
    sys.exit("Error: insert DFT for DFT and SE for semiempirical")


# RESULTS OUTPUT 

fout=base_name + '.ovl'
fid= open(fout, 'w')
np.savetxt(fid, Sres , fmt='%16.8f') #,header='comment', comments='% ')
fid.close()
print ''
print ' Overlap matrix wrote to ',fout

fout=base_name + '.cti'
fid= open(fout, 'w')
np.savetxt(fid, Heff , fmt='%16.8f') #,header='comment', comments='% ')
fid.close()
print ''
print ' === Effective Hamiltonian ==='
print ''
np.savetxt(sys.stdout, Heff , fmt='%16.8f')
print ''
print ' Wrote to ',fout















