program mocpl
  implicit none
  character(80)    :: line,bau
  integer          :: i,j,k,dim_a,dim_b,dim_ab,stat,ic,tmp,dim_abg,n_g
  integer          :: n_a,n_b,i_a1,i_a2,i_b1,i_b2,dim_eff,Nl,LWORK,INFO
  real(8)          :: tmp_d,dotp,MOnorm
  real(8), DIMENSION(:,:), ALLOCATABLE :: C_frag,C_ab,F_ab,S_ab,S_abg
  real(8), DIMENSION(:,:), ALLOCATABLE :: H_eff,S_eff,S_m12,tmp_dmat1,tmp_dmat2,H_eff0
  real(8), DIMENSION(:), ALLOCATABLE   :: WORK,S_eig
  integer, DIMENSION(:), ALLOCATABLE   ::  IPIV
  logical  :: is_dft,fix_sign
  real(8),parameter  :: Ha2eV=27.2113845d0
  real(8),parameter  :: tol_w=0.0001 ! tolerance for warnings on MOs normalization or asymmetry
  integer,parameter  :: linemax=100000000
  character(30),parameter    :: fmt_mo="(16x,6(f10.6))"

write(*,*) 
write(*,*) ' This program computes one-electron charge transfer couplings'
write(*,*) ' following the method described in:'
write(*,*) ' Valeev et al., J. Am. Chem. Soc. 128, 9882 (2006)'
write(*,*) ' Baumeier et al., Phys. Chem. Chem. Phys. 12, 11103 (2010)'
write(*,*) 
write(*,*) "   Gabriele D'Avino, January 2019"
write(*,*) "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
write(*,*) 
write(*,*) " This program works for an orbital subspace of arbitrary size "
write(*,*) " and molecules of different type. "
write(*,*)
write(*,*) " Orca output names are hard-coded:"
write(*,*) "   a.out  b.out  ab.out  [ab.g.out]"
write(*,*) " The latter is needed only if signed integrals are required"
write(*,*)
write(*,*) ' Please provide:'
write(*,*) '  - Indices of first and last MO for fragment a* '
write(*,*) '  - Indices of first and last MO for fragment b* '
write(*,*) '  - Logical flag: T/F for ab initio (DFT, HF)/semiempirical (e.g. ZINDO)'
write(*,*) '  - Logical flag: T to compute signed couplings using a phase-probe '
write(*,*) '    s orbital. If enabled, the file ab.g.out must be supplied'
write(*,*)
write(*,*) ' *MO indexing starts from 0 (zero) as in ORCA!!!!' 
write(*,*)

read(*,*) i_a1,i_a2,i_b1,i_b2,is_dft,fix_sign
write(*,*)
write(*,*) ' Values read: '
write(*,*) i_a1,i_a2,i_b1,i_b2,is_dft,fix_sign
write(*,*) 
write(*,*)

if ( (i_a1.gt.i_a2) .or. (i_b1.gt.i_b2) ) then
   write(*,*) ' ERROR: MOs intervals are badly defined! '
   write(*,*) '        Stopping execution!'
   stop
endif

if ( fix_sign .and. (.not.is_dft) ) then
   write(*,*) ' ERROR: The sign fix works only with ab initio methods (is_dft=T)! '
   write(*,*) '        Stopping execution!'
   stop
endif



! switching to one-based MO indexing ;)
i_a1=i_a1+1
i_a2=i_a2+1
i_b1=i_b1+1
i_b2=i_b2+1
dim_eff= 2 + (i_a2-i_a1) + (i_b2-i_b1)

!  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

!   Strategy:
!   1. Parse the ORCA outputs and get the relevant quantities  (the long tedious part)
!   1.1 Fix fragments MOs signs using a phase-probe s orbital
!   2. Reconstruct the Fock matrix of the dimer (F_ab) on the AO basis
!   3. Express the dimer F_ab and S_ab on the fragments MO basis
!   4. Restrict F_ab to the subspace of interest
!   5. Perform the Lowdin orthogonalization, if required 
!  

!  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!   1. Parse the ORCA outputs and get the relevant quantities  
!  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

open(file='a.out',unit=1,status="old")
open(file='b.out',unit=2,status="old")
open(file='ab.out',unit=3,status="old") 
if (fix_sign)  open(file='ab.g.out',unit=4,status="old") 



!  determine dimension and allocate variables
do i=1,linemax
   line=''
   read(1,"(a80)") line
   line=adjustl(trim(line))
   if (line(1:10)=='Basis Dime') then
       read(line,"(43x,i5)") dim_a
      exit
   endif
enddo

do i=1,linemax
   line=''
   read(2,"(a80)") line
   line=adjustl(trim(line))
   if (line(1:10)=='Basis Dime') then
       read(line,"(43x,i5)") dim_b
      exit
   endif
enddo

do i=1,linemax
   line=''
   read(3,"(a80)") line
   line=adjustl(trim(line))
   if (line(1:10)=='Basis Dime') then
       read(line,"(43x,i5)") dim_ab
      exit
   endif
enddo


write(*,*) ' Parsing data from ORCA outputs: '
write(*,*) '   - Dimension fragment a : ',dim_a
write(*,*) '   - Dimension fragment b : ',dim_b
write(*,*) '   - Dimension dimer ab   : ',dim_ab


if (dim_ab.ne.(dim_a+dim_b)) then
   write(*,*) ' ERROR: matrix dimensions mismatch between fragment and dimer!'
   write(*,*) '        Stopping execution!'
   stop   
endif

if (fix_sign) then
   do i=1,linemax
      line=''
      read(4,"(a80)") line
      line=adjustl(trim(line))
      if (line(1:10)=='Basis Dime') then
         read(line,"(43x,i5)") dim_abg
         exit
      endif
   enddo
   write(*,*) '   - Dimension ab + s     : ',dim_abg

   if (dim_abg.lt.(dim_ab+2)) then
      write(*,*) ' ERROR: matrix dimension mismatch! '
      write(*,*) '        Stopping execution!'
      stop   
endif

endif



! variable allocation
allocate(C_frag(dim_ab,dim_ab))
allocate(C_ab(dim_ab,dim_ab))
allocate(F_ab(dim_ab,dim_ab))
allocate(S_ab(dim_ab,dim_ab))
if (fix_sign) allocate(S_abg(dim_abg,dim_abg))


! parsing fragment a MOs
rewind(1)
Nl=ceiling(real(dim_a)/6.0d0) ! number of lines over which Vab and S are written 
C_frag=0.0d0

do i=1,linemax
   line=''
   read(1,"(a80)") line
   line=adjustl(trim(line))
  
   if (line(1:19)=='MOLECULAR ORBITALS') then
      read(1,*) ! skip

      do j=1,Nl
         read(1,*) ! skip
         read(1,*) ! skip
         read(1,*) ! skip
         read(1,*) ! skip

         do k=1,dim_a
!           read(1,"(a80)") line
!            read(line(16:80),*)  ( C_frag(k,ic), ic=(1+6*(j-1)),min(dim_a,6*j) )
            read(1,fmt_mo)  ( C_frag(k,ic), ic=(1+6*(j-1)),min(dim_a,6*j) )
            
         enddo

      enddo
      write(*,*) '   - MOs of fragment a read '  
      exit
   endif

enddo

      

! parsing fragment b MOs
rewind(2)
Nl=ceiling(real(dim_b)/6.0d0) ! number of lines over which Vab and S are written 

do i=1,linemax
   line=''
   read(2,"(a80)") line
   line=adjustl(trim(line))
  
   if (line(1:19)=='MOLECULAR ORBITALS') then
      read(2,*) ! skip

      do j=1,Nl
         read(2,*) ! skip
         read(2,*) ! skip
         read(2,*) ! skip
         read(2,*) ! skip

         do k=1,dim_b
!            read(2,"(a80)") line
!            read(line(16:80),*)  ( C_frag(dim_a+k,dim_a+ic), ic=(1+6*(j-1)),min(dim_b,6*j) )

            read(2,fmt_mo)  ( C_frag(dim_a+k,dim_a+ic), ic=(1+6*(j-1)),min(dim_b,6*j) )
         enddo

      enddo
      write(*,*) '   - MOs of fragment b read '
      exit
   endif

enddo


! parsing dimer MOs
rewind(3)
Nl=ceiling(real(dim_ab)/6.0d0) ! number of lines over which Vab and S are written 
C_ab=0.0d0

do i=1,linemax
   line=''
   read(3,"(a80)") line
   line=adjustl(trim(line))
   
   if (line(1:19)=='MOLECULAR ORBITALS') then
      read(3,*) ! skip

      do j=1,Nl
         read(3,*) ! skip
         read(3,*) ! skip
         read(3,*) ! skip
         read(3,*) ! skip

         do k=1,dim_ab
            !read(3,"(a80)") line
            !read(line(16:80),*)  ( C_ab(k,ic), ic=(1+6*(j-1)),min(dim_ab,6*j) )

!            write(*,*) k, ( ic, ic=(1+6*(j-1)),min(dim_ab,6*j) )

            read(3,fmt_mo)  ( C_ab(k,ic), ic=(1+6*(j-1)),min(dim_ab,6*j) )
            
         enddo

      enddo
      write(*,*) '   - MOs of dimer ab read '
      exit
   endif

enddo


! parsing dimer MO energies
rewind(3)
F_ab=0.0d0

do i=1,linemax
   line=''
   read(3,"(a80)") line
   line=adjustl(trim(line))
  
   if (line(1:17)=='ORBITAL ENERGIES') then
      read(3,*) ! skip
      read(3,*) ! skip
      read(3,*) ! skip

      do k=1,dim_ab
         read(3,*) tmp,tmp_d,F_ab(k,k)
         F_ab(k,k)=F_ab(k,k)*Ha2eV
      enddo
      
      write(*,*) '   - MO energies of dimer ab read '
      exit

   endif
   
enddo


! parsing dimer S matrix
rewind(3)
Nl=ceiling(real(dim_ab)/6.0d0) ! number of lines over which Vab and S are written 
S_ab=0.0d0

do i=1,linemax
   line=''
   read(3,"(a80)") line
   line=adjustl(trim(line))
  
   if (line(1:15)=='OVERLAP MATRIX') then
      read(3,*) ! skip

      do j=1,Nl
         read(3,*) ! skip

         do k=1,dim_ab
            !read(3,"(a80)") line
            !read(line(8:80),*)  ( S_ab(k,ic), ic=(1+6*(j-1)),min(dim_ab,6*j) )
            read(3,*) tmp,( S_ab(k,ic), ic=(1+6*(j-1)),min(dim_ab,6*j) )
         enddo

      enddo
      
      write(*,*) '   - Overlap matrix of dimer ab read '
      exit
   endif

enddo


if (fix_sign) then ! parsing dimer+ghost  S matrix

rewind(4)
Nl=ceiling(real(dim_abg)/6.0d0) ! number of lines over which Vab and S are written 
S_abg=0.0d0

do i=1,linemax
   line=''
   read(4,"(a80)") line
   line=adjustl(trim(line))
  
   if (line(1:15)=='OVERLAP MATRIX') then
      read(4,*) ! skip

      do j=1,Nl
         read(4,*) ! skip

         do k=1,dim_abg
            !read(3,"(a80)") line
            !read(line(8:80),*)  ( S_ab(k,ic), ic=(1+6*(j-1)),min(dim_ab,6*j) )
            read(4,*) tmp,( S_abg(k,ic), ic=(1+6*(j-1)),min(dim_abg,6*j) )
         enddo

      enddo
      
      write(*,*) '   - Overlap matrix of ab + s read '
      exit
   endif

enddo
   

endif



! checking the normalization of MOs 
write(*,*) 
write(*,*)
write(*,*) ' Performing a check on fragments and dimer MO normalization.'
write(*,*) ' Warnings will be printed in case of deviation from unity larger than ',tol_w
write(*,*) 

do k=1,dim_a ! fragment a
   MOnorm=0.0d0

   if (is_dft)  then
      do i=1,dim_a
         do j=1,dim_a      
            MOnorm=MOnorm + C_frag(i,k)*C_frag(j,k)*S_ab(i,j)
         enddo
      enddo

   else
      do i=1,dim_a      
         MOnorm=MOnorm + C_frag(i,k)**2
      enddo
   endif


   if ( abs(1.0d0 - MOnorm).gt.tol_w ) then
      write(*,*) 'Warning: the norm of fragment a MO ',k,' is ',MOnorm
   endif  
enddo

do k=1,dim_b ! fragment b
   MOnorm=0.0d0

   if (is_dft)  then
      do i=1,dim_b
         do j=1,dim_b 
            MOnorm=MOnorm + C_frag(dim_a+i,dim_a+k)*C_frag(dim_a+j,dim_a+k)*S_ab(dim_a+i,dim_a+j)
         enddo
      enddo
   else
      do i=1,dim_b      
         MOnorm=MOnorm + C_frag(dim_a+i,dim_a+k)**2
      enddo
   endif


   if ( abs(1.0d0 - MOnorm).gt.tol_w ) then
      write(*,*) 'Warning: the norm of fragment b MO ',k,' is ',MOnorm
   endif  
enddo

do k=1,dim_ab ! dimer ab
   MOnorm=0.0d0

   if (is_dft)  then
      do i=1,dim_ab
         do j=1,dim_ab
            MOnorm=MOnorm + C_frag(i,k)*C_frag(j,k)*S_ab(i,j)
         enddo
      enddo
   else
      do i=1,dim_ab
         MOnorm=MOnorm + C_frag(i,k)**2
      enddo
   endif

   if ( abs(1.0d0 - MOnorm).gt.tol_w ) then
      write(*,*) 'Warning: the norm of dimer ab MO ',k,' is ',MOnorm
   endif  
enddo




!  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!   1.1 Fix fragments MOs signs using a phase-probe s orbital
!  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (fix_sign) then

   write(*,*)
   write(*,*)
   write(*,*) ' Fixing the sign of MOs againts probe 1s orbitals '
   

   ! fixing the sign of MOs of fragment a
   write(*,*)
   write(*,*) '    Fragment a:'
   
   n_g=dim_ab + 1 ! index of the probe 1s orbital 
   
   do i=i_a1,i_a2

      j=i
      dotp=0.0
      do k=1,dim_ab
         dotp = dotp + C_frag(k,j)*S_abg(k,n_g)         
      enddo

      if (dotp.ge.0) then
         write(*,*) '    MO no. ',i-1,' scalar prod.',dotp,' --> sign maintained '
      else
         write(*,*) '    MO no. ',i-1,' scalar prod.',dotp,' --> sign flipped '
         C_frag(:,j)=-C_frag(:,j)
      endif
   enddo


   ! fixing the sign of MOs of fragment b
   write(*,*)
   write(*,*) '    Fragment b:'
   
   n_g=dim_ab + 1 + (dim_abg-dim_ab) /2 ! index of the probe 1s orbital 
   
   do i=i_b1,i_b2

      j= dim_a + i
      dotp=0.0
      do k=1,dim_ab
         dotp = dotp + C_frag(k,j)*S_abg(k,n_g)         
      enddo

      if (dotp.ge.0) then
         write(*,*) '    MO no. ',i-1,' scalar prod.',dotp,' --> sign maintained '
      else
         write(*,*) '    MO no. ',i-1,' scalar prod.',dotp,' --> sign flipped '
         C_frag(:,j)=-C_frag(:,j)
      endif
      enddo


endif



!  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!   2. Reconstruct the Fock matrix of the dimer (F_ab) on the AO basis
!  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

write(*,*)
write(*,*)
write(*,*) ' Calculation of electronic couplings: '
write(*,*) '   - Reconstructing the dimer Fock matrix in the AO basis '

allocate(tmp_dmat1(dim_ab,dim_ab))
allocate(tmp_dmat2(dim_ab,dim_ab))

if (is_dft) then ! DFT

!   H = S * C * E *C^-1 

   ! C^-1 ---> tmp1
   LWORK=10*dim_ab
   allocate(IPIV(dim_ab))
   allocate(WORK(LWORK))
   
   tmp_dmat1=C_ab
   call DGETRF(dim_ab, dim_ab, tmp_dmat1, dim_ab, IPIV, INFO)
   if (INFO.ne.0) then
      write(*,*) ' ERROR: LU factorization returned INFO=',INFO
      write(*,*) '        Stopping execution!'
      stop
   endif
   
   call DGETRI(dim_ab, tmp_dmat1, dim_ab, IPIV, WORK, LWORK, INFO)
   if (INFO.ne.0) then
      write(*,*) ' ERROR: matrix inversion returned INFO=',INFO
      write(*,*) '        Stopping execution!'
      stop
   endif


   
   ! E * C^-1 --> tmp2
   call DGEMM('n','n',dim_ab,dim_ab,dim_ab,1.0d0,F_ab,dim_ab, &
        tmp_dmat1,dim_ab,0.0d0,tmp_dmat2,dim_ab)
   
   ! C * tmp2 --> tmp1
   call DGEMM('n','n',dim_ab,dim_ab,dim_ab,1.0d0,C_ab,dim_ab, &
        tmp_dmat2,dim_ab,0.0d0,tmp_dmat1,dim_ab)
   
   ! S * tmp1 --> H
   call DGEMM('n','n',dim_ab,dim_ab,dim_ab,1.0d0,S_ab,dim_ab, &
        tmp_dmat1,dim_ab,0.0d0,F_ab,dim_ab)
   
   
else ! semiempirical

   ! H= C * E * C'

   ! E * C' -> tmp1
   call DGEMM('n','t',dim_ab,dim_ab,dim_ab,1.0d0,F_ab,dim_ab, &
        C_ab,dim_ab,0.0d0,tmp_dmat1,dim_ab)

   ! C * tmp1 -> H 
   call DGEMM('n','n',dim_ab,dim_ab,dim_ab,1.0d0,C_ab,dim_ab, &
        tmp_dmat1,dim_ab,0.0d0,F_ab,dim_ab)
   
endif



!  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!   3. Express the dimer F_ab and S_ab on the fragments MO basis
!  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

write(*,*) '   - Expressing F and S on the fragments MO basis '

! Fock matrix  : F = C_frag' * F_ao * C_frag

! F_ao * C_frag -- > tmp1
call DGEMM('n','n',dim_ab,dim_ab,dim_ab,1.0d0,F_ab,dim_ab, &
     C_frag,dim_ab,0.0d0,tmp_dmat1,dim_ab)

! C_frag' * tmp1 --> F
call DGEMM('t','n',dim_ab,dim_ab,dim_ab,1.0d0,C_frag,dim_ab, &
     tmp_dmat1,dim_ab,0.0d0,F_ab,dim_ab)

! Overlap matrix  : S = C_frag' * S_ao * C_frag

! S_ao * C_frag -- > tmp1
call DGEMM('n','n',dim_ab,dim_ab,dim_ab,1.0d0,S_ab,dim_ab, &
     C_frag,dim_ab,0.0d0,tmp_dmat1,dim_ab)

! S_frag' * tmp1 --> S
call DGEMM('t','n',dim_ab,dim_ab,dim_ab,1.0d0,C_frag,dim_ab, &
     tmp_dmat1,dim_ab,0.0d0,S_ab,dim_ab)



!  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!   4. Restrict F_ab to the subspace of interest
!  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

write(*,*) '   - Restricting F and S to the MO subspace of interest'

allocate(H_eff(dim_eff,dim_eff))
allocate(S_eff(dim_eff,dim_eff))


H_eff=0.0d0
S_eff=0.0d0

n_a= 1 + i_a2-i_a1
n_b= 1 + i_b2-i_b1

H_eff( 1:n_a , 1:n_a ) = F_ab( i_a1:i_a2 , i_a1:i_a2 ) ! a-a
H_eff( 1:n_a , n_a+1:n_a+n_b ) = F_ab( i_a1:i_a2 , dim_a+i_b1:dim_a+i_b2 ) ! a-b
H_eff( n_a+1:n_a+n_b , 1:n_a ) = F_ab( dim_a+i_b1:dim_a+i_b2 , i_a1:i_a2 ) ! b-a
H_eff( n_a+1:n_a+n_b , n_a+1:n_a+n_b ) = F_ab( dim_a+i_b1:dim_a+i_b2 , dim_a+i_b1:dim_a+i_b2 ) ! b-b

do i=1,dim_eff
   S_eff(i,i)=1.0d0
enddo
S_eff( 1:n_a , n_a+1:n_a+n_b ) = S_ab( i_a1:i_a2 , dim_a+i_b1:dim_a+i_b2 ) ! a-b
S_eff( n_a+1:n_a+n_b , 1:n_a ) = S_ab( dim_a+i_b1:dim_a+i_b2 , i_a1:i_a2 ) ! b-a



!  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!   5. Perform the Lowdin orthogonalization, if required 
!  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (is_dft) then ! DFT

   allocate(H_eff0(dim_eff,dim_eff))
   H_eff0=H_eff

   write(*,*) '   - Performing Lowdin orthogonalization '

   deallocate(tmp_dmat1)
   deallocate(tmp_dmat2)
   allocate(S_eig(dim_eff))
   allocate(S_m12(dim_eff,dim_eff))
   allocate(tmp_dmat1(dim_eff,dim_eff))
   allocate(tmp_dmat2(dim_eff,dim_eff))

   ! compute S^-1/2
   
   ! diagonalize eigenvalues of S_ab matrix   
   !tmp_dmat1=S_eff
   tmp_dmat1 = 0.50d0 * ( S_eff + transpose(S_eff) )
   call DSYEV('v','u',dim_eff,tmp_dmat1,dim_eff,S_eig,WORK,LWORK,INFO)
   if (INFO.ne.0) then
      write(*,*) ' ERROR: S matrix diagonalization returned INFO=',INFO
      write(*,*) '        Stopping execution!'
      stop
   endif
   
   S_m12=0.0d0
   do i=1,dim_eff
      S_m12(i,i)=S_eig(i)**(-0.50d0)
   enddo

  
   ! S^-1/2 = Sv* Se^(-0.5) * Sv'
   
   ! Se^(-0.5) * Sv' --> tmp2
   call DGEMM('n','t',dim_eff,dim_eff,dim_eff,1.0d0,S_m12,dim_eff, &
        tmp_dmat1,dim_eff,0.0d0,tmp_dmat2,dim_eff)

   ! Sv* tmp2 --> S^-1/2 
   call DGEMM('n','n',dim_eff,dim_eff,dim_eff,1.0d0,tmp_dmat1,dim_eff, &
        tmp_dmat2,dim_eff,0.0d0,S_m12,dim_eff)

   !  perform Lowdin orthogonalization
   ! H_eff= S_m12 * H_eff * S_m12

   ! H_eff * S_m12 --> tmp1
   call DGEMM('n','n',dim_eff,dim_eff,dim_eff,1.0d0,H_eff,dim_eff, &
        S_m12,dim_eff,0.0d0,tmp_dmat1,dim_eff)
   
   ! S_m12 * tmp1 -->  H_eff
   call DGEMM('n','n',dim_eff,dim_eff,dim_eff,1.0d0,S_m12,dim_eff, &
        tmp_dmat1,dim_eff,0.0d0,H_eff,dim_eff)
   
endif


!  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!                  Output results
!  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

write(*,*)
write(*,*)
write(*,*) ' Calculation done! '
write(*,*)
write(*,*)


if (is_dft) then
   open(file='ab0.cti',unit=10,status="replace")

   write(*,*) ' Effective Hamiltonian in eV before orthogonalisation: '
   do i=1,dim_eff
      write(*,"(20(f14.6))")  H_eff(i,:)
      write(10,"(20(f14.6))") H_eff(i,:)
   enddo
   close(10)
   write(*,*)
   write(*,*) ' H before Lowdin orthogonalisation written to file ab0.cti '
   write(*,*)
   write(*,*)
endif


open(file='ab.cti',unit=10,status="replace")

write(*,*) ' Effective Hamiltonian in eV: '
do i=1,dim_eff
   write(*,"(20(f14.6))")  H_eff(i,:)
   write(10,"(20(f14.6))") H_eff(i,:)
enddo
close(10)
write(*,*)


! check on the symmetry of H 
do i=1,dim_eff
   do j=i+1,dim_eff

      if ( abs( H_eff(i,j) - H_eff(j,i) ).gt.tol_w  ) then
         write(*,*) 'Warning: H elements between',i,' and ',j,' differ by ',H_eff(i,j) - H_eff(j,i)
      endif

   enddo
enddo



open(file='ab.ovl',unit=10,status="replace")
write(*,*)
write(*,*) ' Overlap matrix: '
do i=1,dim_eff
   write(*,"(20(f14.6))")  S_eff(i,:)
   write(10,"(20(f14.6))") S_eff(i,:)
enddo
close(10)



write(*,*)
write(*,*) ' H and S were written to file ab.cti and ab.ovl '
write(*,*)


end program mocpl
