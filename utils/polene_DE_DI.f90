program polene

implicit none

integer, parameter :: Nmax=10000
integer          :: i,j,mol1,tmp_i,molx
real(8)          :: DE0,DE,DI,D,dq(Nmax),tmp_r(20),phi,phi0
character(50)    :: rst_neu,rst_chg,fmt

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

write(*,*) 
write(*,*) 'INPUT: '
write(*,*) '  1) MESCal restart file for the NEUTRAL system calculation '
write(*,*) '  2) MESCal restart file for the CHARGED system calculation '
write(*,*) 
write(*,*) '  Note that in the CHARGED system the central molecule bears the      '
write(*,*) '  differential atomic charges (dq = q_ion - q_neu)  and other molecule'
write(*,*) '  have zero atomic charges. '
write(*,*)  
read(*,*)  rst_neu
read(*,*)  rst_chg

write(*,*) 
write(*,*) 'Files read: '
write(*,*) rst_neu
write(*,*) rst_chg
write(*,*) 


open(file=rst_neu,unit=1,status="old")
open(file=rst_chg,unit=2,status="old")
read(1,*) ! skip header
read(2,*) ! skip header




!!! induction energy
! %1.i_mol 2.i_at 3-5.xyz 6.q_0 7.dq  8-10.mu 11.V 12-14.F 15.V0 16-18.F0
read(2,*) mol1,tmp_i,tmp_r(3:5),dq(1),tmp_r(7:10),phi
DI=dq(1)*phi

do i=2,Nmax
   read(2,*) molx,tmp_i,tmp_r(3:5),dq(i),tmp_r(7:10),phi

   if (molx.ne.mol1)  exit

   DI = DI + dq(i)*phi

enddo
close(2)
DI = DI *0.50d0

!!! electrostatic energy
! %1.i_mol 2.i_at 3-5.xyz 6.q_0 7.dq  8-10.mu 11.V 12-14.F 15.V0 16-18.F0
read(1,*) mol1,tmp_i,tmp_r(3:10),phi,tmp_r(12:14),phi0
DE=dq(1)*phi
DE0=dq(1)*phi0

do i=2,Nmax
   read(1,*) molx,tmp_i,tmp_r(3:10),phi,tmp_r(12:14),phi0

   if (molx.ne.mol1)   exit

   DE = DE + dq(i)*phi
   DE0 = DE0 + dq(i)*phi0

enddo
close(1)


fmt='(a15,f10.4)'
write(*,*) 
write(*,*) ' Energies in eV:'
write(*,fmt) 'Delta_E0: ',DE0
write(*,fmt) 'Delta_E : ',DE
write(*,fmt) 'Delta_I : ',DI
write(*,fmt) 'Delta   : ',(DE + DI)
write(*,*) 

open(file='ene.dat',unit=10,status="replace")
write(10,*) '% 1.Delta_E0  2.Delta_E  3.DeltaI 4.Delta'
write(10,'(4(f10.4))') DE0, DE, DI, (DE + DI)
close(10)

write(*,*) ' Polarization energies wrote to ene.dat'
write(*,*) 


end program polene


!old
!h -1.0092
!e -0.8595
