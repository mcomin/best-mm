program polene

implicit none

real(8), parameter :: iu2ev=14.39964485d0
integer            :: i,j,k,tmp_i(20),n1,n2,mol1,molx,stat,i1,i2,i3
real(8)            :: tmp_r(20),V_eh,DE_1,DE_2,DI,Dtot,DI_1,DI_2,Exc_bind
real(8)            :: cell(6),box(3,3),t(3) !,t2(3)
real(8), allocatable :: r1(:,:),r2(:,:),dq1(:),dq2(:),phi1_c1(:),phi2_c1(:),phi1_c2(:),phi2_c2(:)
real(8), allocatable :: phi1_n(:),phi2_n(:)
logical              :: found,cell_read
character(50)        :: rst_neu1,rst_chg1,fmt,rst_neu2,rst_chg2,tmp_s

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

write(*,*) 
write(*,*) 'INPUT: '

write(*,*) '  1) MESCal restart file for the NEUTRAL@mol1 calculation '
write(*,*) '  2) MESCal restart file for the CHARGED@mol1 calculation '
write(*,*) '  3) MESCal restart file for the NEUTRAL@mol2 calculation '
write(*,*) '  4) MESCal restart file for the CHARGED@mol2 calculation '
write(*,*) 
write(*,*) '  In case there the file  pbc.dat is found in the directory' 
write(*,*) '  periodic boudary conditions will be enabled.             '
write(*,*) '  The file pbc.dat consistes of two lines:                 '
write(*,*) '  line 1: CRYST1 record of the pdb                         ' 
write(*,*) '  line 2: line of the MrWolf connectivity log corresponding' 
write(*,*) '          to the dimer under examination                   '  
write(*,*) 
write(*,*) '  Note that in the CHARGED system the central molecule bears the      '
write(*,*) '  differential atomic charges (dq = q_ion - q_neu)  and other molecule'
write(*,*) '  have zero atomic charges. '
write(*,*) 
read(*,*)  rst_neu1
read(*,*)  rst_chg1
read(*,*)  rst_neu2
read(*,*)  rst_chg2

open(file=rst_neu1,unit=1,status="old")
open(file=rst_chg1,unit=2,status="old")
open(file=rst_neu2,unit=3,status="old")
open(file=rst_chg2,unit=4,status="old")
write(*,*) 
write(*,*) 'Files read: '
write(*,*) 
write(*,*) rst_neu1
write(*,*) rst_chg1
write(*,*) rst_neu2
write(*,*) rst_chg2
write(*,*) 


inquire(file='pbc.dat',exist=found)
cell_read=.false.
if (found) then
   cell_read=.true.

   open(file='pbc.dat',unit=5,status="old")
   read(5,*) tmp_s(1:6),cell
   
   write(*,*) 'File pbc.dat found: PBC enabled!'
   write(*,*) 
   write(*,*) 'Cell parameters:'
   write(*,"(6(f12.4))") cell

   box=cell2box(cell)
   write(*,*)
   write(*,*) 'Cell vectors (by rows):'
   do i=1,3
      write(*,"(6(f12.4))") box(i,:)
   enddo
   write(*,*) 

   read(5,*) tmp_i(1:2),i1,i2,i3
   t=i1*box(1,:) + i2*box(2,:) + i3*box(3,:)
   write(*,"(a40,3(i4))") 'Molecule 2 is in the periodic replica: ',i1,i2,i3
   write(*,"(a40,3(f10.3))") 'Translation vector: ',t
   write(*,*)   

   close(5)

endif




! count the number of atoms for each molecule
read(1,*) ! skip header
read(1,*,iostat=stat) mol1 
n1=1
do while(stat==0)
   read(1,*,iostat=stat) molx 
   if (molx.ne.mol1)  exit
   n1= n1 + 1 
enddo

read(3,*) ! skip header
read(3,*,iostat=stat) mol1 
n2=1

do while(stat==0)
   read(3,*,iostat=stat) molx 
   if (molx.ne.mol1)  exit
   n2= n2 + 1
enddo

!write(*,*) 'n1,n2  ',n1,n2


! allocate variables

allocate(r1(n1,3),r2(n2,3))
allocate(dq1(n1),phi1_c1(n1),phi1_c2(n1),phi1_n(n1))
allocate(dq2(n2),phi2_c1(n2),phi2_c2(n2),phi2_n(n2))



! read easy stuff

rewind(1)
rewind(2)
read(1,*) ! skip header
read(2,*) ! skip header
do i=1,n1

   ! %1.i_mol 2.i_at 3-5.xyz 6.q_0 7.dq  8-10.mu 11.V 12-14.F 15.V0 16-18.F0
   read(1,*) tmp_i(1:2),tmp_r(3:10),phi1_n(i)
   read(2,*) tmp_i(1:2),r1(i,1:3),dq1(i),tmp_r(7:10),phi1_c1(i)

enddo


rewind(3)
rewind(4)
read(3,*) ! skip header
read(4,*) ! skip header
do i=1,n2

   ! %1.i_mol 2.i_at 3-5.xyz 6.q_0 7.dq  8-10.mu 11.V 12-14.F 15.V0 16-18.F0
   read(3,*) tmp_i(1:2),tmp_r(3:10),phi2_n(i)
   read(4,*) tmp_i(1:2),r2(i,1:3),dq2(i),tmp_r(7:10),phi2_c2(i)

enddo



! read less easy stuff


! search m2 in c1 
found=.false.
rewind(2) 
read(2,*) ! skip header
stat=0
do while(stat==0)

   read(2,*,iostat=stat) tmp_i(1:2),tmp_r(3:11)

   if ( Norm3( r2(1,:) - tmp_r(3:5) + t ).lt.0.010d0 ) then  ! check ioppp
      phi2_c1(1)=tmp_r(11)
!      write(*,*) ' m2@c1 ',1,tmp_r(3:5),phi2_c1(1)
      found=.true.
      do j=2,n2
         read(2,*) tmp_i(1:2),tmp_r(3:10),phi2_c1(j)
      enddo

      exit
   endif
   
enddo



!! search m2 in c1 in the original cell
!found=.false.
!rewind(2) 
!read(2,*) ! skip header
!stat=0
!do while(stat==0)
!
!   read(2,*,iostat=stat) tmp_i(1:2),tmp_r(3:11)
!
!   if ( Norm3( r2(1,:) - tmp_r(3:5) ).lt.0.010d0 ) then
!      phi2_c1(1)=tmp_r(11)
!!      write(*,*) ' m2@c1 ',1,tmp_r(3:5),phi2_c1(1)
!      found=.true.
!      t2=0.0d0
!      do j=2,n2
!         read(2,*) tmp_i(1:2),tmp_r(3:10),phi2_c1(j)
!
!      enddo
!
!      exit
!   endif
!   
!enddo
!
!! search m2 in c1 in the periodic replica
!if (.not.found) then
!   do i3=-1,1
!   do i2=-1,1
!   do i1=-1,1
!   if ( .not. ( (i1.eq.0) .and. (i2.eq.0) .and. (i3.eq.0) ) ) then
!   
!      t=i1*box(1,:) + i2*box(2,:) + i3*box(3,:)
!      
!      
!      rewind(2) 
!      read(2,*) ! skip header
!      stat=0
!      do while(stat==0)
!
!         read(2,*,iostat=stat) tmp_i(1:2),tmp_r(3:11)
!
!         if ( Norm3( r2(1,:) - tmp_r(3:5) -t ).lt.0.010d0 ) then
!
!            t2=t
!            phi2_c1(1)=tmp_r(11)
!            found=.true.
!
!            do j=2,n2
!               read(2,*) tmp_i(1:2),tmp_r(3:10),phi2_c1(j)
!
!            enddo
!
!            exit
!            exit
!            exit
!            exit
!            
!         endif
!   
!      enddo
!   
!   endif
!   enddo
!   enddo
!   enddo   
!endif


if (found) then
   write(*,*) 'INFO: molecule 2 read from the 1st restart!'      
   write(*,*)
else
   write(*,*) 'ERROR: molecule 2 not found in the 1st restart!'
   call stop_exec
endif


! remove the potential of the fixed charges
! ioppp      
do j=1,n2
   do i=1,n1
      phi2_c1(j)= phi2_c1(j) - iu2eV* dq1(i)/Norm3( r1(i,:) - r2(j,:) - t )
   enddo
enddo

!write(*,*)




! search m1 in c2 
found=.false.
rewind(4)
read(4,*) ! skip header
stat=0

do while(stat==0)
   
   read(4,*,iostat=stat) tmp_i(1:2),tmp_r(3:11)
   
   if ( Norm3( r1(1,:) - tmp_r(3:5) -t ).lt.0.010d0 ) then 
      phi1_c2(1)=tmp_r(11)
      found=.true.
      do j=2,n1
         read(4,*) tmp_i(1:2),tmp_r(3:10),phi1_c2(j)
      enddo

      exit
   endif
   
enddo



!! search m1 in c2 in the original cell
!found=.false.
!rewind(4)
!read(4,*) ! skip header
!stat=0
!
!do while(stat==0)
!   
!   read(4,*,iostat=stat) tmp_i(1:2),tmp_r(3:11)
!   
!   if ( Norm3( r1(1,:) - tmp_r(3:5) ).lt.0.010d0 ) then 
!      phi1_c2(1)=tmp_r(11)
!      found=.true.
!      t2=0.0d0
!      do j=2,n1
!         read(4,*) tmp_i(1:2),tmp_r(3:10),phi1_c2(j)
!      enddo
!
!      exit
!   endif
!   
!enddo
!
!
!! search m1 in c2 in the periodic replica
!if (.not.found) then
!   do i3=-1,1
!   do i2=-1,1
!   do i1=-1,1
!   if ( .not. ( (i1.eq.0) .and. (i2.eq.0) .and. (i3.eq.0) ) ) then
!   
!      t=i1*box(1,:) + i2*box(2,:) + i3*box(3,:)
!
!      rewind(4)
!      read(4,*) ! skip header
!      stat=0
!      
!      do while(stat==0)
!   
!         read(4,*,iostat=stat) tmp_i(1:2),tmp_r(3:11)
!   
!         if ( Norm3( r1(1,:) - tmp_r(3:5) -t ).lt.0.010d0 ) then
!
!            t2=t
!            phi1_c2(1)=tmp_r(11)
!            found=.true.
!      
!            do j=2,n1
!               read(4,*) tmp_i(1:2),tmp_r(3:10),phi1_c2(j)
!            enddo
!
!            exit
!            exit
!            exit
!            exit
!            
!         endif
!   
!      enddo
!   endif
!   enddo
!   enddo
!   enddo
!endif


if (found) then
   write(*,*) 'INFO: molecule 1 read from the 2nd restart!'      
   write(*,*)
else
   write(*,*) 'ERROR: molecule 1 not found in the 2nd restart!'
   call stop_exec
endif

! remove the potential of the fixed charges 
! ioppp      
do i=1,n1
   do j=1,n2
      phi1_c2(i)= phi1_c2(i) - iu2eV * dq2(j)/Norm3( r1(i,:) -r2(j,:) - t )
   enddo
enddo

close(1)
close(2)
close(3)
close(4)




! let's do the calculations 

V_eh=0.0d0
do i=1,n1
   do j=1,n2
      V_eh = V_eh + dq1(i)*dq2(j)/Norm3( r1(i,:) - r2(j,:) - t )  ! ioppp      
   enddo
enddo
V_eh = V_eh*iu2ev

DE_1=0.0d0
DE_2=0.0d0
DI_1=0.0d0
DI_2=0.0d0
DI=0.0d0


do i=1,n1
   DE_1=  DE_1 +  dq1(i) * phi1_n(i)
   DI_1= DI_1 + dq1(i) * phi1_c1(i)
   DI= DI + dq1(i) * ( phi1_c1(i) + phi1_c2(i) )   
enddo

do i=1,n2
   DE_2=  DE_2 +  dq2(i) * phi2_n(i)
   DI_2= DI_2 + dq2(i) * phi2_c2(i)
   DI= DI + dq2(i) * ( phi2_c1(i) + phi2_c2(i) )
enddo

DI_1= DI_1 * 0.50d0
DI_2= DI_2 * 0.50d0
DI =  DI * 0.50d0

Dtot=DE_1 + DE_2 + DI + V_eh
Exc_bind=DI + V_eh - ( DI_1 + DI_2 ) ! exciton binding energy

fmt='(a15,f10.4)'
write(*,*) 
write(*,*) ' Energies in eV:'
write(*,fmt) 'Delta_E1 : ',DE_1
write(*,fmt) 'Delta_E2 : ',DE_2
write(*,fmt) 'Delta_I  : ',DI
write(*,fmt) 'V_eh     : ',V_eh
write(*,fmt) 'Delta    : ',Dtot
write(*,fmt) 'Exc_bind      : ',Exc_bind

write(*,*) 



open(file='ene.dat',unit=10,status="replace")
write(10,*) '   Delta_E1  Delta_E2  Delta_I  Veh       Delta     Exc_bind'
write(10,'(8(f10.4))') DE_1, DE_2, DI, V_eh, Dtot, Exc_bind
close(10)

write(*,*) ' Polarization energies wrote to ene.dat'
write(*,*) 



contains

  
subroutine stop_exec
  real(8)  :: nan

 
  open(file='ene.dat',unit=10,status="replace")
  write(10,*) 'Delta_E1  Delta_E2  Delta_I  Veh  Delta  Exc_bind'
  write(10,'(a30)') "NaN  NaN  NaN  NaN  NaN  NaN" 
  close(10)
    
  write(*,*) 
  write(*,*) "  ...stopping the execution!  "
  write(*,*) 
  stop " ERROR: Something went wrong! "
end subroutine stop_exec


function cell2box(c) result(box)
  implicit none
  real(8)    :: c(6),cell(6),box(3,3)
  real(8),parameter :: deg2rad =0.0174532925199433 ! pi/180.0_rk
  
  cell=c
  cell(4:6)=deg2rad*cell(4:6)

  box=0.0d0

  box(1,1)=cell(1)

  box(2,1)=cell(2)*cos(cell(6));
  box(2,2)=cell(2)*sin(cell(6));

  box(3,1)=cell(3)*cos(cell(5));
  box(3,2)=cell(3)*(cos(cell(4))-cos(cell(6))*cos(cell(5)))/sin(cell(6));
  box(3,3)=sqrt(cell(3)**2-box(3,1)**2-box(3,2)**2);
  
end function cell2box


function Norm3(r)
  implicit none
  real(8)  :: r(3)
  real(8)  :: Norm3

  Norm3=sqrt(r(1)**2+r(2)**2+r(3)**2)
end function Norm3


end program polene


!old
!h -1.0092
!e -0.8595
