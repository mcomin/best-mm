program  cube
  implicit none
  real(8)           :: r0(3),v(3,3),p(3),Vol,Nel,Nel_Z,correction
  real(8)           :: rho1(6),rho2(6),dV,sum1,sum2,sum3
  integer           :: i,j,k,ix,iy,iz,Nat,Nxyz(3),ii,ff,iter
  real(8), DIMENSION(:,:), ALLOCATABLE :: atoms(:,:)

!!!!!!!!!!!!!!!!!!!!

write(*,*)
write(*,*) ' ------ Cube density rounding  ------'  
write(*,*) " G. D'Avino Oct. 2021 "
write(*,*)
write(*,*) ' INFO: The code will read the file inp.cube (supposed to be a density) and '
write(*,*) '       performs rounding of its integral to the nearest integer, written to out.cube. ' 
write(*,*)
write(*,*)


open(file="inp.cube",unit=1,status="old")
open(file="out.cube",unit=3,status="replace")



! the first iter the input file is read and the integral computed
! The second iter the rounded output is written
do iter=1,2 

   if (iter.eq.2) rewind(1)
   
   read(1,*) 
   read(1,*) 
   read(1,*) Nat,r0

   if (Nat.le.0) then
      write(*,*) ' Error: MO cube must have Nat>0, check!!!'
      error stop
   endif

   if (iter.eq.1)  then
      allocate(atoms(Nat,4))
   else    
      write(3,*) 'Rounded cube file'
      write(3,*) ' '
      write(3,"(i5,3(f12.6))") Nat,r0
   endif

   do i=1,3
      read(1,*) Nxyz(i),v(i,:)
      if (iter.eq.2)  write(3,"(i5,3(f12.6))") Nxyz(i),v(i,:)   
   enddo

   dV=v(1,1) *v(2,2) *v(3,3) 


   do i=1,Nat
      read(1,*) j,atoms(i,4),atoms(i,1:3)
      if (iter.eq.2) write(3,"(i5,4(f12.6))") j,atoms(i,4),atoms(i,1:3)
   enddo

   if (iter.eq.1) then
      write(*,"(a20,1x,i8)") 'Number of atoms', Nat
      write(*,"(a20,1x,3(i8))") 'Number of points', Nxyz
      write(*,"(a20,1x,f16.8)") 'Volume element',  dV
      write(*,"(a20,1x,3(i8))") 'MO number ',j
      write(*,*)
      sum1=0.0d0
      sum2=0.0d0
   endif



   do ix=1,Nxyz(1)
   do iy=1,Nxyz(2)
   do j=1,Nxyz(3),6 
  
      ff=6
      if ( (Nxyz(3)-j).lt.6) ff=Nxyz(3)-j+1

      read(1,*)  rho1(1:ff) 

      if (iter.eq.1) then
         sum1= sum1 + sum( rho1(1:ff) )

      else

         
         write(3,"(6(e14.5))") rho1(1:ff) - correction
         sum2= sum2 + sum( rho1(1:ff) - correction )
      endif
      
   enddo
   enddo
   enddo

   if (iter.eq.1)  correction=( dV*sum1 - nint(dV*sum1) ) /( dV *Nxyz(1)*Nxyz(2)*Nxyz(3) )
   

enddo ! loop on iter

close(1)
close(3)


write(*,"(a30,1x,f16.8)") 'Integral inp.cube  ',  dV*sum1
write(*,"(a30,1x,f16.8)") 'Integral out.cube ',  dV*sum2
write(*,*)
write(*,*)


end program cube






  
