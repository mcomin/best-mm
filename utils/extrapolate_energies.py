#!/usr/bin/env python3
"""
Author: Massimiliano Comin 2020

Extrapolates GW Ionization Potentials and Electron Affinities to the infinite basis limit.
It is assumed to be a decreasing exponential: A + B exp(-x)

# 0 arguments : search recursively for `fiesta.log` in the current directory
# 1 argument : search recursively for `fiesta.log` in the given path
# 2 & more arguments : read `arg[i]/fiesta.log` or `arg[i]`

"""
import os,re,sys
import numpy as np
from scipy.optimize import curve_fit

join = os.path.join


def read_fiesta(file):
    "Parse Fiesta output: returns HOMO,LUMO at maximum iteration"

    homo = {}
    lumo = {}
    gap = {}

    with open(file,'r') as f:

        for line in f:

            if re.match(r'<it   \d>',line):

                i = int(re.search(r'\d',line).group())
                data = re.findall(r'[+-]?\d+\.\d+',line)

                if 'HOMO' in line:
                    homo[i] = float(data[-1])

                elif 'LUMO' in line:
                    lumo[i] = float(data[-1])

            elif 'it_dCOHSEX' in line:

                i = int(re.search(r'\d',line).group())
                data = re.findall(r'[+-]?\d+\.\d+',line)

                if 'HOMO' in line:
                    homo[i] = float(data[-1])

                elif 'LUMO' in line:
                    lumo[i] = float(data[-1])

    idx = np.max([i for i in homo.keys()])

    return homo[idx],lumo[idx]


def main(*args):
    "Extrapolate to the infinite basis limit the given IP/EA from Fiesta calculation"

    homos = []
    lumos = []

    # Search recursively for `fiesta.log`
    if len(args) == 1:
        abs_path = args[0]
        for (path, dirs, files) in os.walk(args[0]):
            if 'fiesta.log' in files:
                print('Reading ',join(path,'fiesta.log'))
                data = read_fiesta(join(path,'fiesta.log'))
                print('Found \t HOMO={0} \t LUMO={1}'.format(*data))

                homos += [data[0]]
                lumos += [data[1]]

    # Read files, or read `fiesta.log` from paths
    else:
        abs_path = './'
        for x in args:
            if os.path.isfile(x):
                print('Reading ',x)
                data = read_fiesta(x)
            else:
                print('Reading ',join(x,'fiesta.log'))
                data = read_fiesta(join(x,'fiesta.log'))

            print('Found \t HOMO={0} \t LUMO={1}'.format(*data))
            homos += [data[0]]
            lumos += [data[1]]

    # Sort HOMOs & LUMOs in descending order
    homos = np.sort(np.array(homos))[::-1]
    lumos = np.sort(np.array(lumos))[::-1]

    assert len(homos) == len(lumos), "Parsing Error: Found different number of HOMOs and LUMOs"
    assert len(homos) >= 2, "Fit Error: Cannot fit from less than 2 values"

    # Fit function
    if len(homos) == 2:
        fn = lambda x, *p: p[0] + p[1] * np.exp(-x)
        p_initial = [1.,1.]
    else:
        fn = lambda x, *p: p[0] + p[1] * np.exp(-p[2] * x)
        p_initial = [1.,1.,1.]

    # X positions corresponding to the basis size
    X = np.arange(1,len(homos)+1)

    # Fit HOMO
    p_homo, _ = curve_fit(fn,X,homos,p0=p_initial)
    homo_inf = p_homo[0]

    # Stats
    residuals = homos - fn(X,*p_homo)
    ss_res = np.sum(residuals**2)
    ss_tot = np.sum((homos - homos.mean())**2)
    R = 1 - ss_res / ss_tot
    print('HOMO fitted at R = ', R*100, '%:', homo_inf)

    # Fit LUMO
    p_lumo, _ = curve_fit(fn,X,lumos,p0=p_initial)
    lumo_inf = p_lumo[0]

    residuals = lumos - fn(X,*p_lumo)
    ss_res = np.sum(residuals**2)
    ss_tot = np.sum((lumos - lumos.mean())**2)
    R = 1 - ss_res / ss_tot
    print('LUMO fitted at R = ', R*100, '%: ',lumo_inf)


    with open(join(abs_path,'homo.dat'),'w') as file1, open(join(abs_path,'lumo.dat'),'w') as file2:
        for x,y in zip(homos,lumos):
            file1.write('{0}\n'.format(x))
            file2.write('{0}\n'.format(y))
        file1.write('{0}\n'.format(homo_inf))
        file2.write('{0}\n'.format(lumo_inf))

    print('Saved homos and lumos to homo.dat and lumo.dat')



# Paths to fiesta log files
# 0 arguments : search recursively for `fiesta.log` in the current directory
# 1 argument : search recursively for `fiesta.log` in the given path
# 2 & more arguments : read `arg[i]/fiesta.log` or `arg[i]`
args = sys.argv[1:] if len(sys.argv) > 1 else '.'
main(*args)
