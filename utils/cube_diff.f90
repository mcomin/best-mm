program  cube
  implicit none
  real(8)           :: r0(3),v(3,3),p(3),Vol,Nel,Nel_Z
  real(8)           :: n1(6),n2(6),dV,sum1,sum2,sum3
  integer           :: i,j,k,ix,iy,iz,Nat,Nxyz(3),ii,ff,Nat2
  real(8), DIMENSION(:,:), ALLOCATABLE :: atoms(:,:)
!!!!!!!!!!!!!!!!!!!!

write(*,*)
write(*,*) ' ------ Cube file difference ------'  
write(*,*) " G. D'Avino Oct. 2021 "
write(*,*)
write(*,*) ' INFO: The code will read files one.cube and two.cube and  '
write(*,*) '       and will write to diff.cube the difference one-two.' 
write(*,*)
write(*,*)

open(file="one.cube",unit=1,status="old")
open(file="two.cube",unit=2,status="old")  
open(file="diff.cube",unit=3,status="replace")



read(1,*) 
read(1,*) 
read(1,*) Nat,r0
read(2,*) 
read(2,*) 
read(2,*) Nat2

if ( (Nat.lt.1) .or.(Nat2.lt.1) .or.(Nat.ne.Nat2) ) then
   write(*,*) ' Error: something wrong in cube files (atom number), check!!!'
   error stop
endif

allocate(atoms(Nat,4))

write(3,*) 'Cube file difference'
write(3,*) 
write(3,"(i5,3(f12.6))") Nat,r0
do i=1,3
   read(1,*) Nxyz(i),v(i,:)
   write(3,"(i5,3(f12.6))") Nxyz(i),v(i,:)  
enddo

dV=v(1,1) *v(2,2) *v(3,3) 

write(*,"(a24,1x,i8)")    'Number of atoms', Nat
write(*,"(a24,1x,3(i8))") 'Number of grid points', Nxyz
write(*,"(a24,1x,f16.8)") 'Volume element',  dV
write(*,*)


do i=1,Nat
   read(1,*) j,atoms(i,4),atoms(i,1:3)
   write(3,"(i5,4(f12.6))") j,atoms(i,4),atoms(i,1:3)
enddo

! skip lines
do i=1,Nat+3
   read(2,*)
enddo



sum1=0.0d0
sum2=0.0d0
sum3=0.0d0

do ix=1,Nxyz(1)
   do iy=1,Nxyz(2)

      do j=1,Nxyz(3),6  !(1+ Nxyz(3)/6)
  
         ff=6
         if ( (Nxyz(3)-j).lt.6) ff=Nxyz(3)-j+1

         read(1,*)  n1(1:ff) 
         read(2,*)  n2(1:ff) 

         sum1= sum1 + sum( n1(1:ff) )
         sum2= sum2 + sum( n2(1:ff) )
         sum3= sum3 + sum( n1(1:ff) - n2(1:ff) )
         
         write(3,"(6(e14.5))") n1(1:ff) - n2(1:ff) 

         
      enddo

   enddo
   
enddo


close(1)
close(2)
close(3)


write(*,"(a30,1x,f16.8)") 'Integral one.cube  ',  dV*sum1
write(*,"(a30,1x,f16.8)") 'Integral two.cube  ',  dV*sum2
write(*,"(a30,1x,f16.8)") 'Integral diff.cube ',  dV*sum3
write(*,*)
write(*,*)



end program cube






  
