#!/usr/bin/env python3
"""
Modified by Massimiliano Comin (Jan. 31 2020)
---------------------------------------------

# Adapted to python3

# Parses recursively the current directory or the directory given as argument

# Any directory containing a .pdb file and a consistent file named Vc_creac.mat will be used

"""

import os,sys
import numpy as np

D = sys.argv[1] if len(sys.argv) > 1 else '.'

print('Extrapolating Reaction Field Matrix from directory: ', D)

n_atoms = []
rfm = []
matrix_size = 0

# Parse subdirs
for (path, dirs, files) in os.walk(D + '/'):

    # Find pdb files
    pdb = [i for i in files if i[-4:] == '.pdb']

    if len(pdb) > 1:
        print('Warning: Found many pdb files found in {0}: {1}'.format(path,pdb))
        print('This could lead to inconsistencies!')

    # Check that PDB file and Vc_creac.mat exist
    if pdb and 'Vc_creac.mat' in files:

        # # Load Reaction Field Matrix
        R = np.loadtxt(path+'/Vc_creac.mat',skiprows=1)

        # Read first line for integrity checks
        with open(path+'/Vc_creac.mat','r') as f:
            line = f.readline()
            dims = [float(i) for i in line.split()]


        # Read number of atoms from first hit pdb
        N = 0
        with open(path+'/'+pdb[0],'r') as f:
            for line in f:
                if 'ATOM' in line:
                    N += 1


        # Read orbital numbers
        l_orbit = np.loadtxt(path+'/l_orbit.data',skiprows=1)[:,1]


        # Check 1: Integrity of RFM
        if len(R) != int(np.prod(dims)):
            print('Error: Inconsistent dims in: {0}. Ignoring...'.format(path+'/Vc_creac.mat'))
            break
        else:
            matrix_size = len(R)

        # Check 2: Consistency between RFMs
        if matrix_size != 0 and len(R) != matrix_size:
            print('Error: RFM {0} has inconsistent dimensions with the previous RFMs found: \
            {1} instead of {2}. Ignoring...'.format(path+'/Vc_creac.mat',len(R),matrix_size))
            break

        # Check 3: Consistency of pdb and classical_atoms.dat ---> To be removed when classical_atoms will no longer be generated
        if 'classical_atoms.dat' in files:
            with open(path+'/classical_atoms.dat','r') as f:
                n = int(f.readlines()[0].split()[0])
            if n != N:
                print('Warning: Found {0} atoms in classical_atoms.dat (not used) and {1} in pdb file {2} (used)'.format(n,N,pdb[0]))
                print('This could lead to inconsistencies!')


        # All checks passed: store RFM !
        rfm += [R]
        n_atoms += [N]


# Fit and Write extrapolated RFM
with open('Vc_creac.mat','w') as f:

    n = int(np.sqrt(matrix_size))

    rfm = np.array(rfm).reshape(len(rfm),n,n)

    f.write('{0} {1}\n'.format(n,n))

    for i in range(n):
        for j in range(n):

            x = n_atoms ** (- (1 + l_orbit[i] + l_orbit[j]) / 3 )

            z = np.polyfit(x,rfm[:,i,j],1)[1]

            f.write('{}\n'.format(z))

print('Extrapolated Reaction Field Matrix written to Vc_creac.mat')
