program  cube
  implicit none
  real(8)           :: r0(3),v(3,3),p(3),Vol,Nel,Nel_Z
  real(8)           :: rho1(6),rho2(6),dV,sum1,sum2,sum3
  integer           :: i,j,k,ix,iy,iz,Nat,Nxyz(3),ii,ff
  real(8), DIMENSION(:,:), ALLOCATABLE :: atoms(:,:)

!!!!!!!!!!!!!!!!!!!!

write(*,*)
write(*,*) ' ------ Cube file MO squared  ------'  
write(*,*) " G. D'Avino Oct. 2021 "
write(*,*)
write(*,*) ' INFO: The code will read MO from file mo.cube and will    '
write(*,*) '       calculate its squared modulus, written to mo2.cube. ' 
write(*,*)
write(*,*)


open(file="mo.cube",unit=1,status="old")
open(file="mo2.cube",unit=3,status="replace")



read(1,*) 
read(1,*) 
read(1,*) Nat,r0

if (Nat.ge.0) then
   write(*,*) ' Error: MO cube should have Nat<0, check!!!'
   error stop
else
   Nat=-Nat
endif


allocate(atoms(Nat,4))

write(3,*) 'Cube file square'
write(3,*) ' '
write(3,"(i5,3(f12.6))") Nat,r0

do i=1,3
   read(1,*) Nxyz(i),v(i,:)
   write(3,"(i5,3(f12.6))") Nxyz(i),v(i,:)   
enddo
dV=v(1,1) *v(2,2) *v(3,3) 


do i=1,Nat
   read(1,*) j,atoms(i,4),atoms(i,1:3)
   write(3,"(i5,4(f12.6))") j,atoms(i,4),atoms(i,1:3)
enddo
read(1,*) i,j

write(*,"(a20,1x,i8)") 'Number of atoms', Nat
write(*,"(a20,1x,3(i8))") 'Number of points', Nxyz
write(*,"(a20,1x,f16.8)") 'Volume element',  dV
write(*,"(a20,1x,3(i8))") 'MO number ',j
write(*,*)

sum1=0.0d0
sum2=0.0d0


do ix=1,Nxyz(1)
  
   do iy=1,Nxyz(2)
      
      do j=1,Nxyz(3),6 
  
         ff=6
         if ( (Nxyz(3)-j).lt.6) ff=Nxyz(3)-j+1

         read(1,*)  rho1(1:ff) 

         write(3,"(6(e14.5))") rho1(1:ff)**2

         sum1= sum1 + sum( rho1(1:ff) )
         sum2= sum2 + sum( rho1(1:ff)**2 )
         
      enddo

   enddo
   
enddo


close(1)
close(3)


write(*,"(a30,1x,f16.8)") 'Integral mo.cube  ',  dV*sum1
write(*,"(a30,1x,f16.8)") 'Integral mo2.cube ',  dV*sum2
write(*,*)
write(*,*)


end program cube






  
