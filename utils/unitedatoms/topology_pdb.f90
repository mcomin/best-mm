
module glob
  implicit none
  real(8),parameter :: pi         = 3.14159265358979_8
  real(8),parameter :: twopi      = 2._8*pi
  real(8),parameter :: pi43       = pi*4._8/3._8
  real(8),parameter :: deg_to_rad = pi/180._8
  real(8),parameter :: rad_to_deg = 1._8/deg_to_rad


  integer,parameter :: maxatom=10359,i_unit=10
  integer :: N!number of atoms
  integer :: Z(maxatom)
  character(4)::Zchar(maxatom)
  real(8) :: r(maxatom,3)
 
  real(8),parameter :: cutoff=2.,cutoffH=1.2,cutoffHeavy=2.4
  character(60) :: pdbfile
  character(120):: line
  character(120):: help

  integer, allocatable:: nbonds(:)!,topology(:,:)
  logical, allocatable:: topology(:,:)
  logical, allocatable:: visited(:)
  real(8) :: Xi(3),Yi(3),Zi(3)

  character(20) :: fmtcoor ="(30x,3(f8.3))" 
  character(20) :: fmtname ="(12x,a4)"
  character(20) :: linefmt="(a)" 
  character(40),parameter:: cryfmt   = "('CRYST1',3(1x,f8.4),3(1x,f6.2),5x,'P1')"
  character(40),parameter:: cellfmt  = "('REMARK CELL AXIS ', 3(1x,f8.3) )"


  character(60),parameter:: pdbfmt   = "('ATOM  ',a5,1x,a4,1x,'UNK ',i5,4x,3(f8.3),a)"
 

  real(8):: CO(3,3),OC(3,3)
  real(8) :: sides(3),angles(3)
contains

  subroutine Read_coords
    implicit none
    integer:: stat
    character(4):: aname

    stat=0
    line=""
    sides=999.
    angles=90.
    N = 0

    do while(stat==0 .and. line(1:3)/="TER" .and. line(1:3)/="END" )
       read(i_unit,linefmt,iostat=stat) line
       if (stat==0 .and. ( line(1:3)=="ATO" .or. line(1:3)=="HET" )) then

          N = N + 1
          if (N> maxatom ) stop "Too many atoms : increase maxatom!"

          read(line,fmtcoor) r(N,:)
          read(line,fmtname) aname
          call atomicnumber(aname,Z(N),Zchar(N))
           
          !write(*,*) N,aname,Z(N),Zchar(N)
       else
          if (line(1:3)=='CRY')  then
             read(line(7:120),*) sides,angles
          endif
       end if
    enddo

   
    CO= COMatrix(sides,angles)
    OC= inverse3(CO)     
     
  end subroutine Read_coords
 
 
  subroutine Write_TpgCharmm(united,pbc) 
    
    implicit none
    logical,intent(in):: united,pbc
    
    integer :: i,j,heavy(maxatom),max,nb
    real(8) :: pos(3),rij(3),dist,cut
    character(6) :: numi,numj
    character(4),allocatable :: aname(:)
    integer:: cshort,clong,i1,i2,i3,i4

    max=N
    heavy=0
    if (united) then
       max=0
       do i=1,N
          if (Z(i)/=1) then
             max=max+1
             heavy(max)=i
          endif
       enddo
    else
       do i=1,N
          heavy(i)=i
       enddo
    end if
   
    open (unit=10,file='ua.tpc',status='unknown') 
  
    write(10,'(a30)') '* Charmm tpg file from unitedatom.f90'
    write(10,'(a)') 
    write(10,'(a)') 'AUTOgenerate ANGLES DIHEDRAL'
    write(10,'(a)')
    write(10,'(a)') 'MASS 1  C  12.0110  C'
    write(10,'(a)') 'MASS 2  H   1.0079  H'
    write(10,'(a)') 'MASS 3  N  14.0067  N'
    write(10,'(a)') 'MASS 4  O  15.9994  O'
    write(10,'(a)') 'MASS 5  S  32.0655  S'
    write(10,'(a)') 'MASS 6  Si 28.0855  Si'
    write(10,'(a)')
    write(10,'(a)') 'RESIDUE UNK  0.0'
    write(10,'(a)')
 
    open (unit=11,file='ua.pdb',status='unknown')
    write(11,cryfmt) sides,angles

    allocate(aname(max))

    cshort=0
    clong=0
    do i=1,max
       if (mod(i,10)==1) write(10,"(1x,a)") "group"
       numi=""
       if (max<100) then      
          write(numi,'(i2)') i        
       else
          if (len_trim(Zchar(heavy(i)))==1) then                        
             cshort=cshort+1
             
             if (cshort<1000) then
                write(numi,'(i3)') cshort
             end if
            
             if (cshort>=1000 .and. cshort<7760) then
                i1=cshort-1000               
                i2=i1/26**2
                i3=(i1-i2*26**2)/26
                i4=i1-i2*26**2-i3*26
                numi(1:1)=char(ichar("0")+i2)
                numi(2:2)=char(ichar("A")+i3)
                numi(3:3)=char(ichar("A")+i4)               
             end if

             if (cshort>7760 .and. cshort<7760+2600) then
                i1=cshort-7760
                i2=i1/100
                i3=(i1-100*i2)/10
                i4=i1-i2*100-i3*10
                numi(1:1)=char(ichar("0")+i3)
                numi(2:2)=char(ichar("A")+i2)
                numi(3:3)=char(ichar("0")+i4)         
             end if

          else
             clong=clong+1
             if (clong<100) then
                write(numi,'(i2)') clong
             end if
             if (cshort>=100 .and. cshort<776) then
                i1=cshort-100               
                i2=i1/26
                i3=i1-i2*26              
                numi(1:1)=char(ichar("0")+i2)
                numi(2:2)=char(ichar("A")+i3)                       
             end if
          endif

       end if
       aname(i)=adjustl( adjustr(Zchar(heavy(i)))//adjustl(numi)) 

       write(*,*) i,Z(i),aname(i)
       write(10,'(2x,a4,x,a6,x,a2,x,f7.4)') "atom",aname(i),&
            Zchar(heavy(i)),0.

       numi=""
       if (i<100000) then 
          write(numi(1:5),'(i5)') i
       else
          i1=(i-100000)/10000
          numi(1:1)=char(ichar("A")+i1)
          i2=i-100000-i1*10000      
          write(numi(2:5),'(i4)') i2
          do j=2,5
             if (numi(j:j)==" ") numi(j:j)="0"
          end do
       end if
       write(11,pdbfmt) numi,aname(i),1,r(heavy(i),:),"  1.00  1.00      UNK"
    enddo
    write(11,'(a3)') "END"
    close(11)
    
    write(10,'(a)' ) 
    nb=0
    do i=1,max-1
       pos(:)=r(heavy(i),:)
       do j=i+1,max
          if (PBC) then
             rij=PBC_distance(pos(:),r(heavy(j),:))
          else
             rij(:)=pos(:)-r(heavy(j),:)
          end if

          dist=sqrt(dot_product(rij,rij))

          cut = 0.
          select case (Z(heavy(i)))
          case (1)
             cut=cut+0.5*cutoffH
          case (2:9)
             cut=cut+0.5*cutoff
          case default
             cut=cut+0.5*cutoffHeavy
          end select
          select case (Z(heavy(j)))
          case (1)
             cut=cut+0.5*cutoffH
          case (2:9)
             cut=cut+0.5*cutoff
          case default
             cut=cut+0.5*cutoffHeavy
          end select


          if (dist<cut) then
             nb    = nb + 1           
             !numi(1:3) = Zchar(heavy(i))
             !numj(1:3) = Zchar(heavy(j))
             !write(numi(4:6),'(i3)') i
             !write(numj(4:6),'(i3)') j
             !call Remove_Blank(numi)
             !call Remove_Blank(numj)
         
             write(10,"(1x,a4,1x,a13)") "bond",aname(i)//" "//aname(j)

          endif
       enddo
    enddo
    write(10,'(a)') 
    write(10,'(a)') 'end'

    deallocate(aname)
    close(unit=10) 

    write(*,*) "Charmm topology file ua.tpc successfully generated "
    write(*,*) "Found ",nb," bonds: CHECK in case of big / 4th row atoms !!!"

  end subroutine Write_TpgCharmm
  
  subroutine Reorder_Mol(PBC)
    
    !reorders the coordinates of the atoms according to the topology (???),
    !starting from an atom with only one bond
  
    implicit none
    logical,intent(in) :: PBC
    integer, allocatable:: neworder(:)
    real(8), allocatable:: swapmat(:,:)
    integer :: i,j,zero
    real(8) :: rij(3),dist,cut
   
    character(4),allocatable:: swapchar(:)

    allocate(topology(N,N))
    allocate(nbonds(N))
    allocate(neworder(N))
    allocate(swapchar(N))

    topology=.false.!0.
    nbonds=0.   
    
    do i=1,N-1
       do j=i+1,N
          if (PBC) then
             rij=PBC_distance(r(i,:),r(j,:))
          else
             rij(:)=r(j,:)-r(i,:)
          end if
          
          dist=sqrt(dot_product(rij,rij))

          cut = 0.
          select case (Z(i))
          case (1)
             cut=cut+0.5*cutoffH
          case (2:9)
             cut=cut+0.5*cutoff
          case default
             cut=cut+0.5*cutoffHeavy
          end select
          select case (Z(j))
          case (1)
             cut=cut+0.5*cutoffH
          case (2:9)
             cut=cut+0.5*cutoff
          case default
             cut=cut+0.5*cutoffHeavy
          end select



          if (dist<cut) then
             topology(i,j)=.true.!1
             topology(j,i)=.true.!1
          endif
       enddo
    enddo

    do i=1,N
       nbonds(i)=count(topology(i,:))
    enddo
    
    write(*,*)" Singly Connected Atoms :"
    do i=1,N
       if (nbonds(i)==1) then
          write(*,'(i3,x,i3,3(x,f10.5))') i,Z(i),r(i,:)
       endif
       if (nbonds(i)==0) then
          write(*,*)  i,Z(i),r(i,:)
          stop "atom with no bonds!!!"
       end if
    enddo

   
    i=-1   
    do while ((i<0).or.(i>N))  
       write(*,*) 'choose one to reorder atomic indeces (0 for no reordering):'
       read(*,*) i
       if (i/=0) then
          write(*,*)"Nbonds= ",nbonds(i)
          if (i>0) then        
             if (nbonds(i)/=1) i=-1
          endif
       end if
    enddo

    if (i/=0) then
       write(*,'(a12,2(x,i3),3(x,f10.5))') 'selected:',i,Z(i),r(i,:)
      

       allocate(visited(N))
       visited(:)=.false.
       zero=0
      
       call Visit(i,zero,Neworder)
       write(*,'(10(x,i3)/)') Neworder
    
       !reordering 
       allocate(swapmat(N,4))
       swapmat=0.

       do i=1,N       
          j=Neworder(i)       
          swapmat(i,:)=(/r(j,1:3),real(Z(j),8)/)
          swapchar(i)=Zchar(j)                
       enddo
     
       r(1:N,1:3) = swapmat(1:N,1:3)
       Z(1:N)=nint(swapmat(1:N,4))
       Zchar(1:N)=swapchar(1:N)

       deallocate(swapmat)
       deallocate(visited)
       deallocate(topology)
       deallocate(Neworder)
       deallocate(nbonds)
       deallocate(swapchar)
    endif
       
  end subroutine Reorder_Mol
  
  
  recursive subroutine Visit(atom,done,list)
   
    implicit none
   
    integer,intent(in):: atom
    integer,intent(inout):: done
    integer,intent(inout):: list(N)
    
    integer,parameter::max=6
    integer:: next,i,j
    real(8):: weight,neighb(max,2),swap(2)
    
    !write(*,*)'called',atom,done,nbonds(atom)
    if (.not.(visited(atom))) then     
       visited(atom)=.true.
       done=done+1
       list(done)=atom
       select case (nbonds(atom))
       case(0)
          return
       case(1)
          next=1
          do while (.not.topology(atom,next))
             next=next+1
          enddo
          call Visit(next,done,list)
          return
       case default
          i=1
          next=0
          do while (next<nbonds(atom))          
             if (topology(atom,i)) then 
                next=next+1
                neighb(next,1)=i
                call Weight3(i,atom,weight)           
                neighb(next,2)=weight
             endif
             i=i+1
          enddo
          do i=1,next-1  !ordering weights
             do j=i+1,next
                if (neighb(j,2)<neighb(i,2)) then
                   swap(:)=neighb(j,:)
                   neighb(j,:)=neighb(i,:)
                   neighb(i,:)=swap(:)
                endif
             enddo
          enddo
          do i=1,next
             call Visit(int(neighb(i,1)),done,list)
          enddo
       end select
    endif
  end subroutine Visit
  
  
  subroutine   Weight3(atom,backatom,weight)   
    ! weights (counts the number of connections left)  atom, its subtrees
    ! and their leaves       
    !W=nbonds(atom)+10*nbonds(alpha_atoms)+100*nbonds(beta_atoms)+f(r_atom-r_backatom)

    implicit none
    integer, intent(in):: atom,backatom
    real(8), intent(out):: weight
    integer ::i,j
    real(8) :: cost,sint,bond(3),norm,f,w1
    real(8),parameter :: Pi=6.28318
    
    
    if (.not.(visited(atom))) then
       ! calculating f(r_atom-r_backatom)
       ! 0<f<1 is important when the other contributions to the weight are the same
       ! f*Pi is the angle of the projection of the bond r_atom-r_backatom on the plane
       ! defined by  inertial x and y axes
       bond(:)=r(atom,:)-r(backatom,:) 
       cost=dot_product(bond,xi)
       sint=dot_product(bond,yi)
       norm=sqrt(cost**2+sint**2)
       cost=cost/norm
       sint=sint/norm
       f=acos(cost)
       if (sint<0.) f=Pi-f
       f=f/Pi
       w1=nbonds(atom)+f
       do i=1,N
          if (topology(atom,i)) then 
             if (.not.visited(i)) then
                w1=w1+nbonds(i)*10
                do j=1,N
                   if (topology(i,j)) then  
                      if (.not.visited(j)) w1=w1+nbonds(j)*100
                   endif
                enddo
             endif
          endif
       enddo
       weight=w1
    else
       weight=0.
    endif
  end subroutine Weight3
   
  real(8) function Mass (Z)
    
    ! pops out the atomic mass in U.M.A. according the atomic number
    implicit none
    !-----ARGUMENTS---------------- 
    integer,intent(in):: Z
  
    select case (Z)
    case(1)
       Mass=1.008
    case(6)
       Mass=12.011   
    case(7)
       Mass=14.007 
    case(8)
       Mass=15.999
    case(9)
       Mass=18.9984032 
    case(14)
       Mass=28.08553
    case(15)
       Mass=30.974000
    case(16)
       Mass=32.06 
    case(17)
       Mass=35.453
    case(34)
       Mass=78.96 
    case(35)
       Mass=79.904

    case default
       write(*,*)"Atomic mass not known for element Z= ",Z
       stop
    end select

  end function Mass
  
  function PBC_distance(r1,r2) result(r12)
    ! calculates a distance vector using the minimum image convention
 
    real(8),intent(in)      ::  r1(3),r2(3)    ! distance between mol1 and mol2
    real(8):: r12(3),delta(3) 
    
    r12(:)=r2-r1
    r12(:)=matmul(OC,r12)
    Delta(:)=2._8*NINT(0.5_8*r12(:))   
    r12(:)=r12(:)-Delta(:)
    r12(:)=matmul(CO,r12)
  end function PBC_distance

  function inverse3(A) result(inv)
    real(8):: A(3,3), inv(3,3),det

    inv(1,:)= (/  A(3,3)*A(2,2)-A(3,2)*A(2,3) , -(A(3,3)*A(1,2)-A(3,2)*A(1,3)),   A(2,3)*A(1,2)-A(2,2)*A(1,3) /) 
    inv(2,:)= (/-(A(3,3)*A(2,1)-A(3,1)*A(2,3)),   A(3,3)*A(1,1)-A(3,1)*A(1,3) , -(A(2,3)*A(1,1)-A(2,1)*A(1,3))/)
    inv(3,:)= (/  A(3,2)*A(2,1)-A(3,1)*A(2,2) , -(A(3,2)*A(1,1)-A(3,1)*A(1,2)),   A(2,2)*A(1,1)-A(2,1)*A(1,2) /)

    det =  A(1,1)*(A(3,3)*A(2,2)-A(3,2)*A(2,3))-A(2,1)*(A(3,3)*A(1,2)-A(3,2)*A(1,3))+A(3,1)*(A(2,3)*A(1,2)-A(2,2)*A(1,3))
    inv = inv/det

  end function inverse3

 function COMatrix(sides,angles) result(CO)
    real(8),intent(in):: sides(3),angles(3)
    real(8):: CO(3,3)
    real(8):: ax,bx,by,cx,cy,cz,cang(3),qt

    co(2,1)=0._8
    co(3,1)=0._8
    co(3,2)=0._8
   
    ax=sides(1)

    cang(1)=COS(deg_to_rad*angles(1))
    cang(2)=COS(deg_to_rad*angles(2))
    cang(3)=COS(deg_to_rad*angles(3))
   
    qt=SIN(deg_to_rad*angles(3))
  
    bx=sides(2)*cang(3)
    by=sides(2)*qt
    cx=sides(3)*cang(2)
    cy=sides(3)*(cang(1)-cang(2)*cang(3))/qt
    cz=sqrt(sides(3)**2-cx*cx-cy*cy)

    co(1,1)=ax
    co(1,2)=bx
    co(1,3)=cx
    co(2,2)=by
    co(2,3)=cy
    co(3,3)=cz
    
    co=co/2._8
  
  end function COMatrix

  subroutine atomicnumber(pdbname,Z,newname) 
    integer,intent(out) :: Z
    character(4),intent(in):: pdbname
    character(4),intent(out):: newname
    character(4):: help
    integer:: i,j

    newname=pdbname
    do  i=1,4
       if( newname(i:i)<="z" .and. newname(i:i)>="a") then
          newname(i:i)=char(ichar(newname(i:i)) + ichar("A")-ichar("a"))  
       end if
       if (newname(i:i)>="0" .and. newname(i:i)<="9") then
          newname(i:i)=" "
       end if
    end do
    j=0
    help=""
    do i=1,4   
       if (newname(i:i)/=" ") then
          j=j+1
          help(j:j)=newname(i:i)
       end if
    end do
    newname=help

    select case (newname)
    case ('H   ')
       Z=1
    case ('C   ')
       Z=6         
    case ('N   ') 
       Z=7
    case ('O   ') 
       Z=8
    case ('F   ')
       Z=9
    case ('SI  ')
       Z=14
    case ('P   ')
       Z=15
    case ('S   ')
       Z=16
    case ('CL  ')
       Z=17
    case ('TI  ')
       Z=22
    case ('CU  ')
       Z=29
    case ('BR  ') 
       Z=35
    case ('I   ') 
       Z=53
    case ('BA  ') 
       Z=56
    case default
       write(*,*) "atom name "//newname//" not present in the database"
       stop "error"
    end select
    
  end subroutine atomicnumber

  subroutine Remove_Blank(string)
    character(*),intent(inout):: string
    integer :: u
    u=1
    do while  (u<len_trim(string))
       if (string(u:u)==' ') then
          string=string(1:u-1)//string(u+1:)//' '
       else
          u=u+1
       endif
    enddo
  end subroutine Remove_Blank
  
end module glob

program pdb_to_tpc
  use glob
  implicit none

  logical :: united,PBC,ex
  
  write(*,*) 'Insert pdb  file name:' 
  read (*,*)  pdbfile
  write(*,*)  pdbfile
  inquire(FILE=pdbfile,EXIST=ex)
  write(*,*) 'WARNING: the coordinates are read as belonging to a single molecule'
  if (.not.ex) then
     write(*,*) "File "//pdbfile//" does not exist!"
     stop
  endif
  open(unit=i_unit,file=pdbfile,status='old')
  call Read_coords
  close(unit=i_unit)  
  write(*,*) "United atom topology? (.true./.false.)"
  read (*,*) United
  write(*,*) United
  write(*,*) "Use periodic boundary conditions in finding bonds? (.true./.false.)"
  read (*,*) PBC
  write(*,*) PBC

  call Reorder_Mol(PBC)

 
  call Write_TpgCharmm(united,PBC)
 

end program pdb_to_tpc

