
!  gfortran -O4 -o exe rs.f topology_qm.f90

module glob
  implicit none
  integer,parameter :: maxatom=2000,i_unit=10
  integer :: N!number of atoms
  integer :: Z(maxatom)
  character(2)::Zchar(maxatom)
  real(8) :: r(maxatom,3)
  real(8) :: qesp(maxatom)=0.,qmull(maxatom)=0.
  real(8) :: qesp_ua(maxatom)=0.
  real(8),parameter :: cutoff=2.,cutoffH=1.2,cutoffHeavy=2.4
  character(60) :: inpfile,outtpc,outpdb,outpol
  character(120):: line
  character(120):: help
  
  integer, allocatable:: nbonds(:)!,topology(:,:)
  logical, allocatable:: topology(:,:)
  logical, allocatable:: visited(:)
  real(8) :: Xi(3),Yi(3),Zi(3)
  

contains
  
 
  subroutine Read_coords_Gaussian
    implicit none
    
    line=''
    do while (index(line,'Standard orientation')==0 &
         .and.index(line,'Z-Matrix orientation')==0 &
         .and.index(line,'Input orientation')==0)
       read(i_unit,'(a120)') line
    enddo
    read(i_unit,*) 
    read(i_unit,*) 
    read(i_unit,*) 
    read(i_unit,*)
    
    N=0
    read(i_unit,'(a120)') line
    
    do while (index(line,"-----")==0)       
       
       N=N+1   
       
       if (N> maxatom ) stop "Too many atoms : increase maxatom!"
       
       read(line(10:20),*) Z(N)
       read(line(31:),*)   r(N,1:3)    
       write(*,"(a,i4,a,i3,a,(3(1x,f7.3)))")  "Atom ",N,"    Z ", z(N),"    xyz ",r(N,1:3)
       
       read(i_unit,'(a120)') line
    enddo
    write(*,*) 'Coordinates read.'
  end subroutine Read_coords_Gaussian

  subroutine Read_charges_Gaussian()
    
    implicit none
    integer :: i
    
    line=""
    write(*,*) "- Reading Mulliken ... "
    do while (index(line,"Mulliken")==0 )
       read(i_unit,'(a100)') line
       
    enddo
    read(i_unit,*) 
    do i=1,N
       read(i_unit,'(a100)') line
       read(line(12:),*) qmull(i)
    enddo
    write(*,*)"Mulliken charges read"
    
    write(*,*) "- Reading ESP ... "
    do while (index(line," Charges from ESP fit")==0)
       read(i_unit,'(a120)') line
    enddo
    read(i_unit,*)
    read(i_unit,*)
    
    do i=1,N
       read(i_unit,'(a120)') line
       write(*,*)line,i
       read(line(12:),*) qesp(i)
       write(*,*) i,qesp(i)
    enddo
    write(*,*)"ESP/CHELPG charges read"
  end subroutine Read_charges_Gaussian
  
  
  function LineNumberLast(string,unit) result(start)
    integer, intent(in):: unit 
    character(*):: string
    integer :: io,count,start
    character(200):: line

    write(*,*) string,len(string)
    io    = 0
    count = 0
    start = 0
    rewind(unit)
    do while (io==0)
       read(unit,'(a200)',iostat=io) line
       count = count + 1
       if (index(line,string)/=0) start=count
    end do
    if (start==0) then
       write(*,*) "string "//string//" not found in unit ",unit
    end if
    rewind(unit)
  end function LineNumberLast


  subroutine Read_coords_Orca
    implicit none
    integer:: io,start,count
    
    start= LineNumberLast("CARTESIAN COORDINATES (ANGSTROEM)",i_unit) + 1
    do io=1,start
       read(i_unit,*)
    enddo

    read(i_unit,'(a120)') line
    N=0
    r=0.
    do while (line/="")
       N=N+1
       if (N> maxatom ) stop "Too many atoms : increase maxatom!"
       Zchar(N)=line(3:4)
       read(line(5:50),*)  r(N,1:3) 
       z(N)=AtomNumber(Zchar(N))
       write(*,"(a,i4,a,i3,a,(3(1x,f7.3)))")  "Atom ",N,"    Z ", z(N),"    xyz ",r(N,1:3) 
       read(i_unit,'(a120)') line
    end do
    write(*,*) 'Coordinates read.'
  end subroutine Read_coords_Orca
  
  
  subroutine Read_charges_Orca()
    implicit none

    integer :: i,start
    
  
    write(*,*) "- Reading Mulliken ... "
    start = LineNumberLast("MULLIKEN ATOMIC CHARGES",i_unit) + 1 
    write(*,*) start
    do i=1,start
       read(i_unit,*)
    enddo

    do i=1,N
       read(i_unit,'(a)') line
       write(*,*) line
       read(line(9:),*) qmull(i)
    end do
    write(*,*)"Mulliken charges read"
    
    write(*,*) "- Reading ESP CHELPG ... " 
    start = LineNumberLast("CHELPG Charges   ",i_unit) + 1
    do i=1,start
       read(i_unit,*)
    enddo  

    do i=1,N
       read(i_unit,'(a120)') line  
       read(line(12:),*) qesp(i)
       write(*,*) i,qesp(i)
    enddo
    write(*,*)"ESP/CHELPG charges read"
    
    
  end subroutine Read_charges_Orca
  
  subroutine Calc_Dipole(dipmom,q_center)
    
    implicit none
    character(4):: TAG
    real(8):: dipmom(3),q_center(3),q! output  dipole moment\
    
    real(8),parameter:: conv=4.803 ! 1.6022 e-29 C m = 4.803 D
    real(8) :: qhelp(maxatom),ri(3),aq
    integer i
    
    
    qhelp=qmull 
    TAG="Mull"
    
    q_center(:)=0.
    aq=0
    do i=1,N
       aq=aq+abs(qhelp(i))
       q_center(:)=q_center(:)+abs(qhelp(i))*r(i,:)
    enddo
    q_center=q_center/aq 
    dipmom=0.
    q=0.
    
    do i=1,N
       q=q+qhelp(i)
       ri(:)=r(i,:)-q_center(:)
       dipmom=dipmom+qhelp(i)*ri(:)
    enddo
    dipmom=dipmom*conv 
    
    write(*,'(a5,a5,3f10.6)') 'm',TAG,sqrt(dot_product(dipmom,dipmom))
    write(*,'(a5,a5,3f10.6,a6,f10.6)') 'dip',TAG,dipmom,' q=',q
    write(*,'(a10,3f10.6,a6,f10.6)') 'qcent=',q_center,' absq=',aq
    write(*,*)
    
    qhelp=qesp
    TAG="ESP "
    
    q_center(:)=0.
    aq=0
    do i=1,N
       aq=aq+abs(qhelp(i))
       q_center(:)=q_center(:)+abs(qhelp(i))*r(i,:)
    enddo
    q_center=q_center/aq 
    dipmom=0.
    q=0.
    
    do i=1,N
       q=q+qhelp(i)
       ri(:)=r(i,:)-q_center(:)
       dipmom=dipmom+qhelp(i)*ri(:)
    enddo
    dipmom=dipmom*conv 
    
    write(*,'(a5,a5,3f10.6)') 'm',TAG,sqrt(dot_product(dipmom,dipmom))
    write(*,'(a5,a5,3f10.6,a6,f10.6)') 'dip',TAG,dipmom,' q=',q
    write(*,'(a10,3f10.6,a6,f10.6)') 'qcent=',q_center,' absq=',aq
    
    
  end subroutine Calc_Dipole
  
  
  subroutine Calc_ESPua_1rn(expo)
    
    implicit none
    integer,intent(in) :: expo ! exponent in 1/r^expo
    integer :: heavy(maxatom), light(maxatom),NH,NL
    integer :: i,j
    real(8) :: sum1rn  ! Sum 1/r_heavy
    real(8) :: diff(3),d,qi,ratio
    
    heavy=0
    light=0
    NH=0
    NL=0
    do i=1,N
       if (Z(i)==1) then
          NL=NL+1
          light(NL)=i
          qesp_ua(i)=0.
       else
          NH=NH+1
          heavy(NH)=i
          qesp_ua(i)=qesp(i)        
       endif
    enddo
    if (expo/=0) then
       do i=1,NL
          sum1rn=0.
          qi=qesp(light(i))
          do j=1,NH
             diff=r(light(i),:)-r(heavy(j),:)
             d=sqrt(dot_product(diff,diff))
             sum1rn=sum1rn+1/d**expo
          enddo
          do j=1,NH
             diff=r(light(i),:)-r(heavy(j),:)
             d=sqrt(dot_product(diff,diff))
             ratio=(1/d**expo)*(1/sum1rn)
             
             qesp_ua(heavy(j))=qesp_ua(heavy(j))+&
                  qesp(light(i))*ratio
          enddo
       enddo
    endif
    
  end subroutine Calc_ESPua_1rn
  
  
  subroutine Write_Charges(TAG,dipole,qcenter,Drude)
    
    implicit none
    character(4),intent(in):: TAG
    real(8),     intent(in):: dipole(3),qcenter(3)
    logical,     intent(in):: Drude
    
    real(8)      :: qhelp(maxatom)
    character(80):: remark
    integer      :: i,heavy(maxatom),max,extra
    real(8)      :: normd
    character(3) :: numi

    
    max  = N
    heavy= 0
    
    select case (TAG)
    case('mull')
       qhelp=qmull
       write(remark,"(a,3(1x,f8.4))")'REMARK  mulliken charges  dipole ',dipole
       do i=1,N
          heavy(i)=i
       enddo
    case('esp ')   
       qhelp=qesp
       write(remark,"(a,3(1x,f8.4))")'REMARK  ESP charges       dipole ',dipole
       
       do i=1,N
          heavy(i)=i
          write(*,*) i,qhelp(heavy(i))
       enddo
    case('espu')
       qhelp=qesp_ua
       write(remark,"(a,3(1x,f8.4))")'REMARK  ESPua q 1/r^3     dipole ',dipole
       max=0
       do i=1,N
          if (Z(i)/=1) then
             max=max+1
             heavy(max)=i
          endif
       enddo      
    end select
    
    ! writing pdb file
    open (unit=10,file=outpdb,status='unknown')
    !       0---x----01---x----2---x----3---x----4---x----
    write(10,'(a,1x,a60)') 'REMARK  coordinates and charges from file',inpfile
    write(10,'(a80)')  remark
    do i=1,max
       Zchar(heavy(i))=AtomSymbol(Z(heavy(i)))
    enddo
    extra=0
    do i=1,max
       write(numi,'(i3)') i
       write(10,100) 'ATOM  ',i+extra,adjustl(adjustr(Zchar(heavy(i)))//adjustl(numi)),&
            'UNK ',1,r(heavy(i),:),qhelp(heavy(i)),1.,'UNK '
       if (Drude .and. Z(heavy(i))/=1) then
          write(10,100) 'ATOM  ',i+extra+1, adjustl( "D"//adjustl( adjustr(Zchar(heavy(i)))//adjustl(numi) ) ),&
               'UNK ',1,r(heavy(i),:),0.,1.,'UNK '
          extra = extra + 1
       end if
    enddo
    write(10,'(a3)')'TER'
    close(unit=10)

    ! writing pol file for drude_psfgen
    if (Drude) then
 
       open(unit=11,file=outpol,status="unknown")
       write(11,"(a)") "# list of drude core atoms, generated by united_atom  for drude_psfgen   "
       write(11,"(a)") "# 1 atomname  2 resname  3 q0 (e)  4  polarizability (A^3) 5 thole factor"  
       do i=1,max
          if (Z(heavy(i))/=1) then
             write(numi,'(i3)') i
             
!mica  mod thole 0.55        
             write(11,"(a4,1x,a4,3(1x,f8.5))") adjustl(adjustr(Zchar(heavy(i)))//adjustl(numi)),'UNK ',&
                  qhelp(heavy(i)), Polarizability(Z(heavy(i))) , 0.55
!mica          
          end if
       end do
       close(11)
    end if

    ! writing pdb file with dipole orientation
    ! rewind(10)
    ! open (unit=11,file='uad.pdb',status='unknown')
    ! do i=1,max+2
    !    read (10,"(a80)") remark
    !    write(11,"(a80)") remark
    ! enddo
    ! normd=sqrt(dot_product(dipole,dipole))
    ! write(11,100) 'HETATM',max+1,"O", 'DIP ',2,qcenter+dipole*9./normd,0.
    ! write(11,100) 'HETATM',max+2,"O", 'DIP ',2,qcenter+dipole*5./normd,0.
    ! write(11,100) 'HETATM',max+3,"S", 'DIP ',2,qcenter,0.
    ! write(11,100) 'HETATM',max+4,"N", 'DIP ',2,qcenter-dipole*5./normd,0.
    ! write(11,100) 'HETATM',max+5,"N", 'DIP ',2,qcenter-dipole*9./normd,0.
    ! write(11,'(a3)')'TER'
    ! 
    ! close(unit=11)

    
100 format(a6,1x,i4,1x,a4,1x,a4,1x,i4,4x,3(f8.3),2(f6.2),6x,a4) 
  end subroutine Write_Charges
  
  subroutine Write_Ato(TAG,dipole,qcenter)
    
    implicit none
    character(4),intent(in):: TAG
    real(8),     intent(in):: dipole(3),qcenter(3)
    
    real(8)       :: qhelp(maxatom)
    character(80) :: remark
    integer       :: i,heavy(maxatom), max
    real(8)       :: radius(maxatom)
    character(3)  :: numi
    
    max  = N
    heavy= 0
    
    
    select case (TAG)
    case('mull')
       qhelp=qmull
       write(remark,"(a,3(1x,f8.4))")'REMARK  mulliken charges  dipole ',dipole
       do i=1,N
          heavy(i)=i
       enddo
    case('esp ')   
       qhelp=qesp
       write(remark,"(a,3(1x,f8.4))")'REMARK  ESP charges       dipole ',dipole
       
       do i=1,N
          heavy(i)=i
       enddo
    case('espu')
       qhelp=qesp_ua
       write(remark,"(a,3(1x,f8.4))")'REMARK  ESPua q 1/r^3     dipole ',dipole
       max=0
       do i=1,N
          if (Z(i)/=1) then
             max=max+1
             heavy(max)=i
          endif
       enddo
    end select
    
    
    open (unit=10,file='ua.ato',status='unknown')
    write(10,"(a)") "REMARK  0.0 0.0 0.0"
    write(10,"(a)") "REMARK"    
    write(10,"(a)") "REMARK ato file generated by /autofs/y.people/zannoni/luca/_prg/_pot/ua"
    write(10,"(a)") "REMARK"    
    write(10,"(a)") "REMARK" 
    !       0---x----01---x----2---x----3---x----4---x----
    write(10,'(a,1x,a)') 'REMARK  coordinates and charges from file',inpfile
    write(10,'(a80)')  remark
    do i=1,max
       Zchar(heavy(i))=AtomSymbol(Z(heavy(i)))
       
       select case (Z(heavy(i))) 
       case(1)
          radius(i)= 1.20 
       case(6)
          radius(i)=  1.70
       case(7)
          radius(i)= 1.55
       case(8)
          radius(i)= 1.52
       case(9)
          radius(i)= 1.47
       case(14)
          radius(i)= 2.10
       case(15)
          radius(i)= 1.80
       case(16)
          radius(i)= 1.80
       case default
          radius(i)= 2.00
       end select
    enddo
    do i=1,max
       write(numi,'(i3)') i
       write(10,100) adjustl(adjustr(Zchar(heavy(i)))),r(heavy(i),:),radius(i),qhelp(heavy(i)),0.0
    enddo
    close(unit=10)
    
    ! writing xyz file
    open(unit=10,file='ua.xyz',status='unknown')
    write(10,"(i5)") max
    write(10,"(a50)") "coordinates from file "//inpfile(1:30)
    do i=1,max
       write(10,"(a,3(1x,f10.5))")  adjustl(adjustr(Zchar(heavy(i)))), r(heavy(i),:)
    end do
    close(10)
    
    
100 format(a4,1x,6(f8.3)) 
  end subroutine Write_Ato
  

  subroutine Write_TpgCharmm(TAG,drude) 
    
    implicit none
    character(4),intent(in):: TAG
    logical,intent(in):: drude

    real(8) :: qhelp(maxatom)
    integer :: heavy(maxatom),max
    real(8) :: pos(3),rij(3),dist,cut
    character(6)::numi,numj,numk,numl
    
    integer :: nbonds,ndbonds,angle,dihed
    integer :: i,j,k,l, a1, a2, a3, a4
    integer,allocatable :: M1(:,:),M2(:,:),M3(:,:)

    max=N
    heavy=0
    select case (TAG)
    case('mull')
       qhelp=qmull
       do i=1,N
          heavy(i)=i
       enddo
    case('esp ')
       qhelp=qesp
       write(*,*)"ESP"
       do i=1,N
          heavy(i)=i
       enddo
    case('espu')
       qhelp=qesp_ua
       max=0
       do i=1,N
          if (Z(i)/=1) then
             max=max+1
             heavy(max)=i
          endif
       enddo
    end select

    if (drude) then
       allocate( M1(max,max), M2(max,max), M3(max,max) )
       M1=0
       M2=0
       M3=0
    end if

    do i=1,max
       Zchar(heavy(i))=AtomSymbol(Z(heavy(i))) 
    enddo
    
    open (unit=10,file=outtpc,status='unknown') 
    write(10,'(a30)') '* Charmm topology file from unitedatom.f90'
    write(10,'(a)') 
    if (.not.Drude)    write(10,'(a)') 'AUTOgenerate ANGLES DIHEDRAL'
    write(10,'(a)')
    write(10,'(a)') 'MASS 1  C  12.0110  C'
    write(10,'(a)') 'MASS 2  H   1.0079  H'
    write(10,'(a)') 'MASS 3  N  14.0067  N'
    write(10,'(a)') 'MASS 4  O  15.9994  O'
    write(10,'(a)') 'MASS 3  F  18.5984  F'
    write(10,'(a)') 'MASS 6  Si 28.0855  Si'
    write(10,'(a)') 'MASS 7  S  32.0655  S' 
    write(10,'(a)') 'MASS 8  Cl 35.4527  Cl'

    if (drude) then 
       write(10,'(a)') 'MASS 4  DF 00.4000  DF'
       write(10,'(a)') 'MASS 5  DC3 00.4000  DC3'
       write(10,'(a)') '*** WARNING: CORRECT ATOM MASSES TO ACCOUNT FOR DRUDE PARTICLE MASS'
       write(10,'(a)') '*** WARNING: there could be more than a drude atom type...         '
    end if
    write(10,'(a)')
    write(10,'(a)') 'RESIDUE unk  0.0'
    write(10,'(a)')
   
    write(10,"(1x,a)") "group"
    do i=1,max
       write(numi,'(i3)') i
       write(10,'(1x,a4,2x,a6,2x,a2,2x,f7.4)') "atom",adjustl( adjustr(Zchar(heavy(i)))//adjustl(numi) ),&
            Zchar(heavy(i)),qhelp(heavy(i))
       if (drude .and. Z(heavy(i))/=1) then
          numj= adjustl( "D"//adjustl( adjustr(Zchar(heavy(i)))//adjustl(numi) ) )
          write(10,'(1x,a4,2x,a6,2x,a2,2x,f7.4)') "atom", numj , numj(1:2), 0.
       end if
    enddo
    write(10,'(a)')
    
    nbonds=0
    if (drude) M1=0
    do i=1,max-1
       pos(:)=r(heavy(i),:)
       do j=i+1,max
          rij(:)=pos(:)-r(heavy(j),:)
          dist=sqrt(dot_product(rij,rij))
          
          cut = 0.
          select case (Z(heavy(i)))
          case (1)
             cut=cut+0.5*cutoffH
          case (2:9)
             cut=cut+0.5*cutoff
          case default
             cut=cut+0.5*cutoffHeavy
          end select
          select case (Z(heavy(j)))
          case (1)
             cut=cut+0.5*cutoffH
          case (2:9)
             cut=cut+0.5*cutoff
          case default
             cut=cut+0.5*cutoffHeavy
          end select
          
          
          if (dist<cut) then
             nbonds    = nbonds + 1
             
             
             numi(1:3) = Zchar(heavy(i))
             numj(1:3) = Zchar(heavy(j))
             write(numi(4:6),'(i3)') i
             write(numj(4:6),'(i3)') j
             call Remove_Blank(numi)
             call Remove_Blank(numj)
             
             write(10,"(1x,a4,1x,a13)") "bond",adjustl(numi)//' '//adjustl(numj)
             if (drude) then
                M1(i,j)=1
                M1(j,i)=1
             end if
          endif
       enddo
    enddo
    if (drude) then
       ndbonds=0
       do i=1,max
          if (Z(heavy(i))/=1) then
             ndbonds = ndbonds + 1
             numi(1:3) = Zchar(heavy(i))
             write(numi(4:6),'(i3)') i
             call Remove_Blank(numi)
             write(10,"(1x,a4,1x,a13)") "bond",&
                  adjustl(numi)//' D'//adjustl(numi)
          end if
       end do
    end if
    write(10,'(a)')

    if (drude) then
        
       M2=matmul(M1,M1)
       M3=matmul(M2,M1)
       
       angle = 0
       do i=1,max-1
          do j=i+1,max
             if (M2(i,j)==1) then
                
                angle=angle+1
                
                a1=i
                a3=j
                
                a2=0
                k=0
                
                do while(a2==0)
                   k=k+1
                   if (k> max) stop "great error!!!"
                   if (M1(k,i)>=1 .and. M1(k,j)>=1) a2=k
                enddo

                numi(1:3) = Zchar(heavy(a1))
                numj(1:3) = Zchar(heavy(a2))
                numk(1:3) = Zchar(heavy(a3))
                
                write(numi(4:6),'(i3)') a1
                write(numj(4:6),'(i3)') a2
                write(numk(4:6),'(i3)') a3

                call Remove_Blank(numi)
                call Remove_Blank(numj)
                call Remove_Blank(numk)
                
                write(10,"(1x,a4,1x,a20)") "angl",adjustl(numi)//' '//adjustl(numj)//" "//adjustl(numk)
             end if
          end do
       end do
       write(10,'(a)')

       dihed = 0
       do i=1,max-1
          do j=i+1,max
             if (M3(i,j)>=1) then
                a1=i
                a4=j

                do k=1,max
                   do l=1,max-1

                      
                      if (k/=i .and. k/=j .and. l/=i .and. l/=j .and. M1(i,k)>=1 .and. M1(k,l)>=1 .and. M1(l,j)>=1) then
                         
                         dihed  = dihed  + 1
                      
                         a2 = k
                         a3 = l

                         numi(1:3) = Zchar(heavy(a1))
                         numj(1:3) = Zchar(heavy(a2))
                         numk(1:3) = Zchar(heavy(a3))
                         numl(1:3) = Zchar(heavy(a4))
                         
                         write(numi(4:6),'(i3)') a1
                         write(numj(4:6),'(i3)') a2
                         write(numk(4:6),'(i3)') a3
                         write(numl(4:6),'(i3)') a4
                         
                         call Remove_Blank(numi)
                         call Remove_Blank(numj)
                         call Remove_Blank(numk)
                         call Remove_Blank(numl)
                         write(10,"(1x,a4,1x,a30)") &
                              "dihe",adjustl( adjustl(numi)//' '//adjustl(numj)//" "//adjustl(numk)//" "//adjustl(numl) )

                      endif
                   enddo
                enddo

             end if
          end do
       end do
       write(10,'(a)')
    end if
 
    write(10,'(a)') 'end'
    close(unit=10) 
    write(*,*) "Charmm topology file "//outtpc//" successfully generated "
    write(*,*) "Found ",nbonds," bonds between atoms: CHECK in case heavy atoms are present !!!"

    if (drude)  then
       write(*,*) "Found ",ndbonds," bonds between Drude particles and atoms: CHECK !!!"
    end if

    if (drude) deallocate( M1, M2, M3)
  end subroutine Write_TpgCharmm
  
  
  
  subroutine Reorder_Mol
    
    !reorders the coordinates of the atoms according to the topology (???),
    !starting from an atom with only one bond
    
    implicit none
    integer, allocatable:: neworder(:)
    real(8), allocatable:: swapmat(:,:)
    integer :: i,j,zero
    real(8) :: rij(3),dist,cut
    real(8) :: Tens(3,3),Frame(3,3),com(3),a,b,c,Rot(3,3)
    integer :: hBondcount, siBondcount
    real(8) :: hCharge
    
    allocate(topology(N,N))
    allocate(nbonds(N))
    allocate(neworder(N))
    
    
    topology=.false.!0.
    nbonds=0.
    
    ! inertial frame
    call Inertia(Tens,Frame,com)  
    Xi(:)=Frame(:,3)  
    Yi(:)=Frame(:,2)
    Zi(:)=Frame(:,1)
    write(*,*) 'Center of Mass'
    write(*,'(3f10.5)') com
    write(*,*) 'Inertial Frame'
    write(*,'(a5,3f10.5)')'x  ', xi
    write(*,'(a5,3f10.5)')'y  ', yi
    write(*,'(a5,3f10.5)')'z  ', zi
    write(*,'(a5,3f12.3)')'eigv ', Tens(1,1),Tens(2,2),Tens(3,3)
    
    ! rotating the molecule in the inertial frame
    
    Frame(1,:)=Xi(:)
    Frame(2,:)=Yi(:)
    Frame(3,:)=Zi(:)
    
    
    ! call Euler_Angles(Frame,a,b,c)
    ! Rot= ROTMATRIX(a,b,c) 
    
    !do i=1,N
    !    r(i,:)=r(i,:)-com
    !   r(i,:)=matmul(Rot,r(i,:))
    !enddo
    
    
    do i=1,N-1
       do j=i+1,N
          rij(:)=r(j,:)-r(i,:)
          dist=sqrt(dot_product(rij,rij))
          
          cut = 0.
          select case (Z(i))
          case (1)
             cut=cut+0.5*cutoffH
          case (2:9)
             cut=cut+0.5*cutoff
          case default
             cut=cut+0.5*cutoffHeavy
          end select
          select case (Z(j))
          case (1)
             cut=cut+0.5*cutoffH
          case (2:9)
             cut=cut+0.5*cutoff
          case default
             cut=cut+0.5*cutoffHeavy
          end select
          
          
          
          if (dist<cut) then
             topology(i,j)=.true.!1
             topology(j,i)=.true.!1
          endif
       enddo
    enddo
    
!!!!!!!!!!!!!!!!!!!!
    ! Silicon nanoparticle hCharge = -0.138021
    ! Silicon nanoparticle do i=1,N
    ! Silicon nanoparticle    hBondcount = 0
    ! Silicon nanoparticle    siBondcount = 0
    ! Silicon nanoparticle    if (Z(i) == 14) then ! Si
    ! Silicon nanoparticle       do j=1,N
    ! Silicon nanoparticle          if (topology(i,j) == .true.) then
    ! Silicon nanoparticle             if (Z(j) == 14) siBondcount = siBondcount+1
    ! Silicon nanoparticle             if (Z(j) == 1) hBondcount = hBondcount+1
    ! Silicon nanoparticle          endif
    ! Silicon nanoparticle       enddo
    ! Silicon nanoparticle       if (siBondcount == 2 .and. hBondcount == 2) qesp(i) = -hCharge*2.0
    ! Silicon nanoparticle       if (siBondcount == 3 .and. hBondcount == 1) qesp(i) = -hCharge     
    ! Silicon nanoparticle       if (siBondcount == 4 .and. hBondcount == 0) qesp(i) = 0.0
    ! Silicon nanoparticle    endif       
    ! Silicon nanoparticle    if (Z(i) == 1) qesp(i) = hCharge
    ! Silicon nanoparticle enddo
!!!!!!!!!!!!!!!!!!!!
    
    do i=1,N
       nbonds(i)=count(topology(i,:))
    enddo
    
    write(*,*)" Singly Connected Atoms :"
    do i=1,N
       if (nbonds(i)==1) then
          write(*,'(i3,1x,i3,3(1x,f10.5))') i,Z(i),r(i,:)
       endif
    enddo
    
    i=-1   
    do while ((i<0).or.(i>N))  
       write(*,*) 'choose one to reorder atomic indeces (0 for no reordering):'
       read(*,*) i
       if (i>0) then
          write(*,*)" Bonds for selected atom: ",nbonds(i)
          if (nbonds(i)/=1) i=-1
       endif
    enddo
    
    if (i/=0) then
       write(*,'(a12,2(1x,i3),3(1x,f10.5))') 'selected:',i,Z(i),r(i,:)
       
       
       allocate(visited(N))
       visited(:)=.false.
       zero=0
       
       call Visit(i,zero,Neworder)
       write(*,'(10(1x,i3)/)') Neworder
       
       
       !reordering 
       allocate(swapmat(N,6))
       swapmat=0.
       do i=1,N
          j=Neworder(i)
          swapmat(i,:)=(/r(j,1:3),qmull(j),qesp(j),real(Z(j),8)/)
       enddo
       
       
       r(:,:)  = swapmat(:,1:3)
       qmull(:)= swapmat(:,4)
       qesp(:) = swapmat(:,5)
       Z(:)=nint(swapmat(:,6))
       
       deallocate(swapmat)
       deallocate(visited)
       deallocate(topology)
       deallocate(Neworder)
       deallocate(nbonds)
    endif
    
  contains
    function  ROTMATRIX(alpha,beta,gamma) result(matrix)
      implicit none
      real(8),intent(in) :: alpha,beta,gamma !Euler angles in rad
      real(8)            :: matrix(3,3)
      real(8)            :: ca,cb,cg
      real(8)            :: sa,sb,sg
      
      ca = cos(alpha)
      cb = cos( beta)
      cg = cos(gamma)
      sa = sin(alpha)
      sb = sin( beta)
      sg = sin(gamma)     
      !     costruisco la matrice di rotazione matrix nella convenzione
      !     di Rose
      matrix(1,1) =  ca * cb * cg - sa * sg
      matrix(1,2) =  sa * cb * cg + ca * sg
      matrix(1,3) = -sb * cg
      matrix(2,1) = -ca * cb * sg - sa * cg
      matrix(2,2) = -sa * cb * sg + ca * cg
      matrix(2,3) =  sb * sg
      matrix(3,1) =  ca * sb
      matrix(3,2) =  sa * sb
      matrix(3,3) =  cb
    end function ROTMATRIX
    
  end subroutine Reorder_Mol
  
  
  
  recursive subroutine Visit(atom,done,list)
    
    implicit none
    
    integer,intent(in):: atom
    integer,intent(inout):: done
    integer,intent(inout):: list(N)
    
    integer,parameter::max=6
    integer:: next,i,j
    real(8):: weight,neighb(max,2),swap(2)
    
    !write(*,*)'called',atom,done,nbonds(atom)
    if (.not.(visited(atom))) then     
       visited(atom)=.true.
       done=done+1
       list(done)=atom
       select case (nbonds(atom))
       case(0)
          return
       case(1)
          next=1
          do while (.not.topology(atom,next))
             next=next+1
          enddo
          call Visit(next,done,list)
          return
       case default
          i=1
          next=0
          do while (next<nbonds(atom))          
             if (topology(atom,i)) then 
                next=next+1
                neighb(next,1)=i
                call Weight3(i,atom,weight)           
                neighb(next,2)=weight
             endif
             i=i+1
          enddo
          do i=1,next-1  !ordering weights
             do j=i+1,next
                if (neighb(j,2)<neighb(i,2)) then
                   swap(:)=neighb(j,:)
                   neighb(j,:)=neighb(i,:)
                   neighb(i,:)=swap(:)
                endif
             enddo
          enddo
          do i=1,next
             call Visit(int(neighb(i,1)),done,list)
          enddo
       end select
    endif
  end subroutine Visit
  
  
  subroutine   Weight3(atom,backatom,weight)   
    ! weights (counts the number of connections left)  atom, its subtrees
    ! and their leaves       
    !W=nbonds(atom)+10*nbonds(alpha_atoms)+100*nbonds(beta_atoms)+f(r_atom-r_backatom)
    
    implicit none
    integer, intent(in):: atom,backatom
    real(8), intent(out):: weight
    integer ::i,j
    real(8) :: cost,sint,bond(3),norm,f,w1
    real(8),parameter :: Pi=6.28318
    
    
    if (.not.(visited(atom))) then
       ! calculating f(r_atom-r_backatom)
       ! 0<f<1 is important when the other contributions to the weight are the same
       ! f*Pi is the angle of the projection of the bond r_atom-r_backatom on the plane
       ! defined by  inertial x and y axes
       bond(:)=r(atom,:)-r(backatom,:) 
       cost=dot_product(bond,xi)
       sint=dot_product(bond,yi)
       norm=sqrt(cost**2+sint**2)
       cost=cost/norm
       sint=sint/norm
       f=acos(cost)
       if (sint<0.) f=Pi-f
       f=f/Pi
       w1=nbonds(atom)+f
       do i=1,N
          if (topology(atom,i)) then 
             if (.not.visited(i)) then
                w1=w1+nbonds(i)*10
                do j=1,N
                   if (topology(i,j)) then  
                      if (.not.visited(j)) w1=w1+nbonds(j)*100
                   endif
                enddo
             endif
          endif
       enddo
       weight=w1
    else
       weight=0.
    endif
  end subroutine Weight3
  
  
  subroutine Inertia (Tens,Frame,com)
    ! computes molecular inertial tensor and frame
    
    implicit none
    
    !-----ARGUMENTS--------------   
    real(8),intent(out):: Tens(3,3),Frame(3,3) ! output
    real(8),intent(out):: com(3)               ! output
    !-----LOCAL VBS---------------
    integer j
    real(8):: tmass
    real(8):: dx,dy,dz,ma,mdx,mdz
    real(8):: ixx,iyy,izz,ixy,ixz,iyz 
    real(8):: eva(3),w1(3),w2(3)
    integer error
    
    com(:)=0.
    tmass=0.
    do j=1,N
       com(:)=com(:)+AtomMass(Z(j))*R(j,:)
       tmass=tmass+AtomMass(Z(j))
    enddo
    com=com/tmass
    Tens=0.       
    ixx=0.
    iyy=0.
    izz=0.
    ixy=0.
    ixz=0.
    iyz=0.
    do j=1,N
       ma=AtomMass(Z(j))
       dx=com(1)-R(j,1)
       dy=com(2)-R(j,2)
       dz=com(3)-R(j,3)
       mdx=ma*dx
       mdz=ma*dz
       ixx=ixx+mdx*dx
       iyy=iyy+ma*dy*dy
       izz=izz+mdz*dz
       ixy=ixy+mdx*dy
       ixz=ixz+mdx*dz
       iyz=iyz+mdz*dy
    enddo
    Tens(1,1)=iyy+izz
    Tens(2,2)=ixx+izz
    Tens(3,3)=ixx+iyy
    Tens(1,2)=-ixy
    Tens(2,1)=-ixy
    Tens(1,3)=-ixz        
    Tens(3,1)=-ixz
    Tens(2,3)=-iyz
    Tens(3,2)=-iyz
    call RS (3,3,Tens,eva,1,Frame,w1,w2,error)  !complib.sgimath
    Tens=0.
    do j=1,3
       Tens(j,j)=eva(j)
    enddo
  end subroutine Inertia
  
  
  real(8) function AtomMass (Z)
    
    ! pops out the atomic mass in U.M.A. according the atom label
    implicit none
    !-----ARGUMENTS---------------- 
    integer,intent(in):: Z
    
    select case (Z)
    case(1 ) 
       AtomMass=1.00794    
    case(2 ) 
       AtomMass=4.002602   
    case(3 ) 
       AtomMass=6.941 	    
    case(4 ) 
       AtomMass=9.012182   
    case(5 ) 
       AtomMass=10.811     
    case(6 ) 
       AtomMass=12.0107    
    case(7 ) 
       AtomMass=14.0067    
    case(8 ) 
       AtomMass=15.9994    
    case(9 ) 
       AtomMass=18.9984032 
    case(10) 
       AtomMass=20.1797    
    case(11) 
       AtomMass=22.98976928
    case(12) 
       AtomMass=24.3050    
    case(13) 
       AtomMass=26.9815386 
    case(14) 
       AtomMass=28.0855    
    case(15) 
       AtomMass=30.973762  
    case(16) 
       AtomMass=32.065     
    case(17) 
       AtomMass=35.453     
    case(18) 
       AtomMass=39.948     
    case(19) 
       AtomMass=39.0983    
    case(20) 
       AtomMass=40.078     
    case(21) 
       AtomMass=44.955912  
    case(22) 
       AtomMass=47.867     
    case(23) 
       AtomMass=50.9415    
    case(24) 
       AtomMass=51.9961    
    case(25) 
       AtomMass=54.938045  
    case(26) 
       AtomMass=55.845     
    case(27) 
       AtomMass=58.933195  
    case(28) 
       AtomMass=58.6934    
    case(29) 
       AtomMass=63.546     
    case(30) 
       AtomMass=65.39 	    
    case(31) 
       AtomMass=69.723     
    case(32) 
       AtomMass=72.64 	    
    case(33) 
       AtomMass=74.92160   
    case(34) 
       AtomMass=78.96 	    
    case(35) 
       AtomMass=79.904     
    case(36) 
       AtomMass=83.798     
    case(37) 
       AtomMass=85.4678    
    case(38) 
       AtomMass=87.62 	    
    case(39) 
       AtomMass=88.90585   
    case(40) 
       AtomMass=91.224     
    case(41) 
       AtomMass=92.906     
    case(42) 
       AtomMass=95.94 	    
    case(43) 
       AtomMass=97.9072        
    case(44) 
       AtomMass=101.07     
    case(45) 
       AtomMass=102.905        
    case(46) 
       AtomMass=106.42     
    case(47) 
       AtomMass=107.8682   
    case(48) 
       AtomMass=112.411    
    case(49) 
       AtomMass=114.818    
    case(50) 
       AtomMass=118.710    
    case(51) 
       AtomMass=121.760    
    case(52) 
       AtomMass=127.60     
    case(53) 
       AtomMass=126.904        
    case(54) 
       AtomMass=131.293    
    case(55) 
       AtomMass=132.9054519
    case(56) 
       AtomMass=137.327    
    case(57) 
       AtomMass=138.90547  
    case(58) 
       AtomMass=140.116    
    case(59) 
       AtomMass=140.90765  
    case(60) 
       AtomMass=144.242    
    case(61) 
       AtomMass=144.9127   
    case(62) 
       AtomMass=150.36     
    case(63) 
       AtomMass=151.964    
    case(64) 
       AtomMass=157.25     
    case(65) 
       AtomMass=158.92535  
    case(66) 
       AtomMass=162.500    
    case(67) 
       AtomMass=164.930    
    case(68) 
       AtomMass=167.259    
    case(69) 
       AtomMass=168.93421  
    case(70) 
       AtomMass=173.04     
    case(71) 
       AtomMass=174.967    
    case(72) 
       AtomMass=178.49     
    case(73) 
       AtomMass=180.94788  
    case(74) 
       AtomMass=183.84     
    case(75) 
       AtomMass=186.207    
    case(76) 
       AtomMass=190.23     
    case(77) 
       AtomMass=192.217    
    case(78) 
       AtomMass=195.084    
    case(79) 
       AtomMass=196.966569 
    case(80) 
       AtomMass=200.59     
    case(81) 
       AtomMass=204.3833   
    case(82) 
       AtomMass=207.2 	    
    case(83) 
       AtomMass=208.98040  
    case(84) 
       AtomMass=208.9824   
    case(85) 
       AtomMass=209.9871   
    case(86) 
       AtomMass=222.0176   
    case(87) 
       AtomMass=223.0197   
    case(88) 
       AtomMass=226.0254   
    case(89) 
       AtomMass=227.0277   
    case(90) 
       AtomMass=232.03806  
    case(91) 
       AtomMass=231.03588  
    case(92) 
       AtomMass=238.02891  
    case(93) 
       AtomMass=237.0482   
    case(94) 
       AtomMass=244.0642   
    case(95) 
       AtomMass=243.0614   
    case(96) 
       AtomMass=247.0704   
    case(97) 
       AtomMass=247.0703   
    case(98) 
       AtomMass=251.0796   
    case(99) 
       AtomMass=252.0830   
    case default
       write(*,*)"Atomic mass not known for element Z= ",Z
       stop
    end select
    
  end function AtomMass
  
  subroutine Euler_Angles(molframe,alpha,beta,gamma)
    !     original program by Marco Cecchini  THANKS!
    !
    !     Il programma EULERO, noti i versori che definiscono il sdR del Lab,
    !     legge come input i versori che definiscono il sdR della molecola
    !     restituendo i tre angoli di Eulero (ALPHA,BETA,GAMMA) definiti
    !     secondo le convenzioni di Rose (in ``Elementary Theory of Angular
    !     Momentum'', pag 65) che esprimono l'orientazione relativa della
    !     molecola rispetto al sdR del Lab.
    !     
    !     from the elements of the rotation matrix obtained using Euler/Rose
    !     conventions we calculate the alpha,beta,gamma angles 
    !
    implicit none
    real(8):: EPSILON 
    parameter(EPSILON=1.0e-6)
    
    !-----PARAMETERS
    real(8),intent(in)  :: molframe(3,3)      ! molecular frame  (axis = ROW !!!!!!!!)
    real(8),intent(out) :: ALPHA,BETA,GAMMA   ! Euler angles
    
    real(8):: TETA,PHI,CHI
    real(8):: SENPHI,SENCHI,arg_chi,arg_phi
    
    real(8):: ref(3,3)=reshape(&
         (/1.0,0.0,0.0,&
         0.0,1.0,0.0,&
         0.0,0.0,1.0/),(/3,3/))
    
    TETA=acos(molframe(3,1)*ref(3,1)+molframe(3,2)*ref(3,2)+molframe(3,3)*ref(3,3))
    
    if (sin(TETA).lt.EPSILON) then
       PHI=0.0
       SENCHI=molframe(1,1)*ref(2,1)+molframe(1,2)*ref(2,2)+molframe(1,3)*ref(2,3)
       arg_chi=molframe(1,1)*ref(1,1)+molframe(1,2)*ref(1,2)+molframe(1,3)*ref(1,3)
       if (abs(arg_chi).gt.1.) arg_chi=arg_chi/abs(arg_chi)         
       if (cos(TETA).gt.0.0) then
          if (SENCHI.ge.0.0) then
             CHI= acos(arg_chi)
          else
             CHI=-acos(arg_chi)       
          endif
       else
          if (SENCHI.ge.0.0) then
             CHI= acos(-arg_chi)
          else
             CHI=-acos(-arg_chi)
          endif
       endif
    else
       SENPHI=(molframe(3,1)*ref(2,1)+molframe(3,2)*ref(2,2)+&
            molframe(3,3)*ref(2,3))/sin(TETA)
       SENCHI=(molframe(2,1)*ref(3,1)+molframe(2,2)*ref(3,2)+&
            molframe(2,3)*ref(3,3))/sin(TETA)  
       
       arg_phi=(molframe(3,1)*ref(1,1)+molframe(3,2)*ref(1,2)&
            +molframe(3,3)*ref(1,3))/sin(TETA)
       if (abs(arg_phi).gt.1.) arg_phi=arg_phi/abs(arg_phi)
       if (SENPHI.ge.0.0) then
          PHI= acos(arg_phi)
       else
          PHI=-acos(arg_phi)
       endif
       arg_chi=(molframe(1,1)*ref(3,1)+molframe(1,2)*ref(3,2)+&
            molframe(1,3)*ref(3,3))/(-sin(TETA))
       if (abs(arg_chi).gt.1.) arg_chi=arg_chi/abs(arg_chi) 
       if (SENCHI.ge.0.0) then            
          CHI= acos(arg_chi)
       else
          CHI=-acos(arg_chi)
       endif
    endif
    ALPHA = PHI
    BETA  = TETA
    GAMMA = CHI
  end subroutine Euler_Angles
  
  
  subroutine Remove_Blank(string)
    character(*),intent(inout):: string
    integer :: u
    u=1
    do while  (u<len_trim(string))
       if (string(u:u)==' ') then
          string=string(1:u-1)//string(u+1:)//' '
       else
          u=u+1
       endif
    enddo
  end subroutine Remove_Blank
  
  
  function AtomSymbol(Z) result(sym)
    integer,intent(in):: Z
    character(2):: sym
    
    select case (Z) 
    case( 1)  
       sym="H " ! - Hydrogen
    case( 2)  
       sym="He" !  - Helium
    case( 3)  
       sym="Li" !  - Lithium
    case( 4)  
       sym="Be" !  - Beryllium
    case( 5)  
       sym="B " ! - Boron
    case( 6)  
       sym="C " ! - Carbon
    case( 7)  
       sym="N " ! - Nitrogen
    case( 8)  
       sym="O " ! - Oxygen
    case( 9)  
       sym="F " ! - Fluorine
    case(10)  
       sym="Ne" !  - Neon
    case(11)  
       sym="Na" !  - Sodium
    case(12)  
       sym="Mg" !  - Magnesium
    case(13)  
       sym="Al" !  - Aluminum, Aluminium
    case(14)  
       sym="Si" !  - Silicon
    case(15)  
       sym="P " ! - Phosphorus
    case(16)  
       sym="S " ! - Sulfur
    case(17)  
       sym="Cl" !  - Chlorine
    case(18)  
       sym="Ar" !  - Argon
    case(19)  
       sym="K " ! - Potassium
    case(20)  
       sym="Ca" !  - Calcium
    case(21)  
       sym="Sc" !  - Scandium
    case(22)  
       sym="Ti" !  - Titanium
    case(23)  
       sym="V " ! - Vanadium
    case(24)  
       sym="Cr" !  - Chromium
    case(25)  
       sym="Mn" !  - Manganese
    case(26)  
       sym="Fe" !  - Iron
    case(27)  
       sym="Co" !  - Cobalt
    case(28)  
       sym="Ni" !  - Nickel
    case(29)  
       sym="Cu" !  - Copper
    case(30)  
       sym="Zn" !  - Zinc
    case(31)  
       sym="Ga" !  - Gallium
    case(32)  
       sym="Ge" !  - Germanium
    case(33)  
       sym="As" !  - Arsenic
    case(34)  
       sym="Se" !  - Selenium
    case(35)  
       sym="Br" !  - Bromine
    case(36)  
       sym="Kr" !  - Krypton
    case(37)  
       sym="Rb" !  - Rubidium
    case(38)  
       sym="Sr" !  - Strontium
    case(39)  
       sym="Y " ! - Yttrium
    case(40)  
       sym="Zr" !  - Zirconium
    case(41)  
       sym="Nb" !  - Niobium
    case(42)  
       sym="Mo" !  - Molybdenum
    case(43)  
       sym="Tc" !  - Technetium
    case(44)  
       sym="Ru" !  - Ruthenium
    case(45)  
       sym="Rh" !  - Rhodium
    case(46)  
       sym="Pd" !  - Palladium
    case(47)  
       sym="Ag" !  - Silver
    case(48)  
       sym="Cd" !  - Cadmium
    case(49)  
       sym="In" !  - Indium
    case(50)  
       sym="Sn" !  - Tin
    case(51)  
       sym="Sb" !  - Antimony
    case(52)  
       sym="Te" !  - Tellurium
    case(53)  
       sym="I " ! - Iodine
    case(54)  
       sym="Xe" !  - Xenon
    case(55)  
       sym="Cs" !  - Cesium
    case(56)  
       sym="Ba" !  - Barium
    case(57)  
       sym="La" !  - Lanthanum
    case(58)  
       sym="Ce" !  - Cerium
    case(59)  
       sym="Pr" !  - Praseodymium
    case(60)  
       sym="Nd" !  - Neodymium
    case(61)  
       sym="Pm" !  - Promethium
    case(62)  
       sym="Sm" !  - Samarium
    case(63)  
       sym="Eu" !  - Europium
    case(64)  
       sym="Gd" !  - Gadolinium
    case(65)  
       sym="Tb" !  - Terbium
    case(66)  
       sym="Dy" !  - Dysprosium
    case(67)  
       sym="Ho" !  - Holmium
    case(68)  
       sym="Er" !  - Erbium
    case(69)  
       sym="Tm" !  - Thulium
    case(70)  
       sym="Yb" !  - Ytterbium
    case(71)  
       sym="Lu" !  - Lutetium
    case(72)  
       sym="Hf" !  - Hafnium
    case(73)  
       sym="Ta" !  - Tantalum
    case(74)  
       sym="W " ! - Tungsten
    case(75)  
       sym="Re" !  - Rhenium
    case(76)  
       sym="Os" !  - Osmium
    case(77)  
       sym="Ir" !  - Iridium
    case(78)  
       sym="Pt" !  - Platinum
    case(79)  
       sym="Au" !  - Gold
    case(80)  
       sym="Hg" !  - Mercury
    case(81)  
       sym="Tl" !  - Thallium
    case(82)  
       sym="Pb" !  - Lead
    case(83)  
       sym="Bi" !  - Bismuth
    case(84)  
       sym="Po" !  - Polonium
    case(85)  
       sym="At" !  - Astatine
    case(86)  
       sym="Rn" !  - Radon
    case(87)  
       sym="Fr" !  - Francium
    case(88)  
       sym="Ra" !  - Radium
    case(89)  
       sym="Ac" !  - Actinium
    case(90)  
       sym="Th" !  - Thorium
    case(91)  
       sym="Pa" !  - Protactinium
    case(92)  
       sym="U " ! - Uranium
    case(93)  
       sym="Np" !  - Neptunium
    case(94)  
       sym="Pu" !  - Plutonium
    case(95)  
       sym="Am" !  - Americium
    case(96)  
       sym="Cm" !  - Curium
    case(97)  
       sym="Bk" !  - Berkelium
    case(98)  
       sym="Cf" !  - Californium
    case(99)  
       sym="Es" !  - Einsteinium
    case default
       write(*,*) "unknown atomic number",Z
       stop "ERROR"
    end select
    
  end function AtomSymbol
  
  
  function AtomNumber(sym) result(Z)
    integer:: Z
    character(2),intent(in):: sym
    
    select case (sym)
    case("H ")
       Z= 01! - Hydrogen
    case("He")
       Z= 02!  - Helium
    case("Li")
       Z= 03!  - Lithium
    case("Be")
       Z= 04!  - Beryllium
    case("B ")
       Z= 05! - Boron
    case("C ")
       Z= 06! - Carbon
    case("N ")
       Z= 07! - Nitrogen
    case("O ")
       Z= 08! - Oxygen
    case("F ")
       Z= 09! - Fluorine
    case("Ne")
       Z= 10!  - Neon
    case("Na")
       Z= 11!  - Sodium
    case("Mg")
       Z= 12!  - Magnesium
    case("Al")
       Z= 13!  - Aluminum, Aluminium
    case("Si")
       Z= 14!  - Silicon
    case("P ")
       Z= 15! - Phosphorus
    case("S ")
       Z= 16! - Sulfur
    case("Cl")
       Z= 17!  - Chlorine
    case("Ar")
       Z= 18!  - Argon
    case("K ")
       Z= 19! - Potassium
    case("Ca")
       Z= 20!  - Calcium
    case("Sc")
       Z= 21!  - Scandium
    case("Ti")
       Z= 22!  - Titanium
    case("V ")
       Z= 23! - Vanadium
    case("Cr")
       Z= 24!  - Chromium
    case("Mn")
       Z= 25!  - Manganese
    case("Fe")
       Z= 26!  - Iron
    case("Co")
       Z= 27!  - Cobalt
    case("Ni")
       Z= 28!  - Nickel
    case("Cu")
       Z= 29!  - Copper
    case("Zn")
       Z= 30!  - Zinc
    case("Ga")
       Z= 31!  - Gallium
    case("Ge")
       Z= 32!  - Germanium
    case("As")
       Z= 33!  - Arsenic
    case("Se")
       Z= 34!  - Selenium
    case("Br")
       Z= 35!  - Bromine
    case("Kr")
       Z= 36!  - Krypton
    case("Rb")
       Z= 37!  - Rubidium
    case("Sr")
       Z= 38!  - Strontium
    case("Y ")
       Z= 39! - Yttrium
    case("Zr")
       Z= 40!  - Zirconium
    case("Nb")
       Z= 41!  - Niobium
    case("Mo")
       Z= 42!  - Molybdenum
    case("Tc")
       Z= 43!  - Technetium
    case("Ru")
       Z= 44!  - Ruthenium
    case("Rh")
       Z= 45!  - Rhodium
    case("Pd")
       Z= 46!  - Palladium
    case("Ag")
       Z= 47!  - Silver
    case("Cd")
       Z= 48!  - Cadmium
    case("In")
       Z= 49!  - Indium
    case("Sn")
       Z= 50!  - Tin
    case("Sb")
       Z= 51!  - Antimony
    case("Te")
       Z= 52!  - Tellurium
    case("I ")
       Z= 53! - Iodine
    case("Xe")
       Z= 54!  - Xenon
    case("Cs")
       Z= 55!  - Cesium
    case("Ba")
       Z= 56!  - Barium
    case("La")
       Z= 57!  - Lanthanum
    case("Ce")
       Z= 58!  - Cerium
    case("Pr")
       Z= 59!  - Praseodymium
    case("Nd")
       Z= 60!  - Neodymium
    case("Pm")
       Z= 61!  - Promethium
    case("Sm")
       Z= 62!  - Samarium
    case("Eu")
       Z= 63!  - Europium
    case("Gd")
       Z= 64!  - Gadolinium
    case("Tb")
       Z= 65!  - Terbium
    case("Dy")
       Z= 66!  - Dysprosium
    case("Ho")
       Z= 67!  - Holmium
    case("Er")
       Z= 68!  - Erbium
    case("Tm")
       Z= 69!  - Thulium
    case("Yb")
       Z= 70!  - Ytterbium
    case("Lu")
       Z= 71!  - Lutetium
    case("Hf")
       Z= 72!  - Hafnium
    case("Ta")
       Z= 73!  - Tantalum
    case("W ")
       Z= 74! - Tungsten
    case("Re")
       Z= 75!  - Rhenium
    case("Os")
       Z= 76!  - Osmium
    case("Ir")
       Z= 77!  - Iridium
    case("Pt")
       Z= 78!  - Platinum
    case("Au")
       Z= 79!  - Gold
    case("Hg")
       Z= 80!  - Mercury
    case("Tl")
       Z= 81!  - Thallium
    case("Pb")
       Z= 82!  - Lead
    case("Bi")
       Z= 83!  - Bismuth
    case("Po")
       Z= 84!  - Polonium
    case("At")
       Z= 85!  - Astatine
    case("Rn")
       Z= 86!  - Radon
    case("Fr")
       Z= 87!  - Francium
    case("Ra")
       Z= 88!  - Radium
    case("Ac")
       Z= 89!  - Actinium
    case("Th")
       Z= 90!  - Thorium
    case("Pa")
       Z= 91!  - Protactinium
    case("U ")
       Z= 92! - Uranium
    case("Np")
       Z= 93!  - Neptunium
    case("Pu")
       Z= 94!  - Plutonium
    case("Am")
       Z= 95!  - Americium
    case("Cm")
       Z= 96!  - Curium
    case("Bk")
       Z= 97!  - Berkelium
    case("Cf")
       Z= 98!  - Californium
    case("Es")
       Z= 99!  - Einsteinium
    case default
       write(*,*) "unknown atomic number",Z
       stop "ERROR"
    end select
    
  end function AtomNumber

  function Polarizability(atomnumber) result(pol)
    ! atomic isotropic polarizability in A^3, tabulated by Thomas Miller
    integer, intent(in):: atomnumber
    real(8):: pol

    pol = -100.
    
    select case(atomnumber)   
    case(1)  !H
       pol = 0.666793
    case(5)  !B
       pol = 3.03
    case(6)  !C
       pol = 1.76
    case(7)  !N
       pol = 1.10
    case(8)  !O
       pol = 0.802
    case(9)  !F
       pol = 0.557
    case(14) !Si
       pol = 5.38
    case(15) !P
       pol = 3.63
    case(16) !S
       pol = 2.90
    case(17) !Cl
       pol = 2.18
    case(34) !Se
       pol = 3.77 
    case(35) !Br
       pol = 3.05
    case(53) !I
       pol = 5.35
    case default
       write(*,*) "WARNING: unknown polarizability for atom number Z=",atomnumber
    end select
    
  end function Polarizability
  
  subroutine File_Ext(newfile,rootfile,extension)
    character(*),intent(out) :: newfile
    character(*),intent(in)  :: rootfile,extension
    integer                  :: i,j
    
    i=index(rootfile,'.',.true.) !  get the position of "." starting from the end of the string.
    j=index(rootfile,'/',.true.) 
    if (i==0) i=len_trim(rootfile)
    j=j+1
    newfile=adjustl(rootfile)
    newfile=newfile(j:i)//extension
    call Remove_Blank(newfile)
  end subroutine File_Ext
  
end module glob

program UA
  
  use glob
  implicit none
  real(8)    :: dipole(3),qcenter(3)
  logical    :: united,ex,drude
  character(4)   :: code,qmodel
  integer :: io
  
  !
  ! reading input filename
  
  write(*,*) 'Insert GAUSSIAN or ORCA output file name (full):' 
  read(*,*)   inpfile
  inquire(FILE=inpfile,EXIST=ex)
  if (.not.ex) then
     write(*,*) "File "//inpfile//" does not exist!"
     stop
  else
     call File_Ext(outtpc,inpfile,"tpc")
     call File_Ext(outpdb,inpfile,"pdb")
     call File_Ext(outpol,inpfile,"pol")
   end if
  !
  ! a quick test about the quantum chemistry code 
  
  open(unit=i_unit,file=inpfile,status='old')
  io=0
  do while (io==0)
     read(i_unit,"(a)",iostat=io) line
     if ( index(line,"* O   R   C   A *")/=0 ) code="ORCA"
     if ( index(line,"Gaussian, Inc.")/=0)     code="GAUS"
  end do
  rewind(i_unit)
  
  !
  ! reading coords and charges
  
  select case (code)
  case ("GAUS")
     write(*,*) "source: Gaussian"
     call Read_coords_Gaussian()    
     call Read_charges_Gaussian()
  case ("ORCA")  
     write(*,*) "source: Orca"
     call Read_coords_Orca()    
     call Read_charges_Orca()
  case default
     write(*,*) "file format not recognized: exiting"
    stop
  end select
  close(unit=i_unit) 
  
  !
  ! processing data
  
  call Reorder_Mol()
  call Calc_Dipole(dipole,qcenter)
  
  write(*,*) "Do you want united atom charges? (.true./.false.)"
  read (*,*) United
  
  if (United) then
     qmodel='espu'
     call Calc_ESPua_1rn(3)
  else
     qmodel='esp '
  end if
  
  write(*,*) "Do you want to add Drude particles on heavy atoms? (.true./.false.)"
  read(*,*) Drude
 
     
  call Write_Charges(qmodel,dipole,qcenter,Drude)
  !     call Write_Ato(qmodel,dipole,qcenter)
  call Write_TpgCharmm(qmodel, Drude)
  
  
end program UA
