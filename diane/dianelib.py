import re
import numpy as np
from os.path import splitext
from collections import Counter
from time import localtime, strftime
from itertools import product

#TODO: print(line), line='-------'. Increase Formula size

atomic_numbers = {
'H':1,'He':2,
'Li':3,'Be':4,'B':5,'C':6,'N':7,'O':8,'F':9,'Ne':10,
'Na':11,'Mg':12,'Al':13,'Si':14,'P':15,'S':16,'Cl':17,'Ar':18,
'K':19,'Ca':20,'Sc':21,'Ti':22,'V':23,'Cr':24,'Mn':25,'Fe':26,'Co':27,'Ni':28,
'Cu':29,'Zn':30,'Ga':31,'Ge':32,'As':33,'Se':34,'Br':35,'Kr':36
}

# Atomic masses in au
atomic_masses = {
'H':1.008,'He':4.0026,
'Li':6.94,'Be':9.0122,'B':10.81,'C':12.011,'N':14.007,'O':15.999,'F':18.998,'Ne':20.180,
'Na':22.9897,'Mg':24.305,'Al':26.9815,'Si':28.0855,'P':30.9738,'S':32.065,'Cl':35.453,'Ar':39.948,
'K':39.0983,'Ca':40.078,'Sc':44.9559,'Ti':47.867,'V':50.9415,'Cr':51.9961,'Mn':54.938,'Fe':55.845,'Co':58.9332,'Ni':58.6934,
'Cu':63.546,'Zn':65.39,'Ga':69.723,'Ge':72.64,'As':74.9216,'Se':78.96,'Br':79.904,'Kr':83.8
}

# Van der Waals radii in Angstroms
vdw_radii = {
'H':1.10, 'He':1.40,
'Li':1.82,'Be':1.53,'B':1.92,'C':1.70,'N':1.55,'O':1.52,'F':1.47,'Ne':1.54,
'Na':2.27,'Mg':1.73,'Al':1.84,'Si':2.10,'P':1.80,'S':1.80,'Cl':1.75,'Ar':1.88,
}

# GAFF 2010 FF parameters | {Element: sigma (Angstrom), eps (kJ/mol)}
ljparams = {'H': (2.5996, 0.0628), 'C': (3.2596, 0.3598),
            'F': (3.1181, 0.2552), 'N': (3.2500, 0.7113),
            'S': (3.5636, 1.0460), 'O': (2.9599, 0.8786),
            'Br': (3.9555, 1.3389), 'Cl': (3.4709,1.1088)}


# Useful things
line1 = 80*'-'
line2 = 60*'-'
ifnone = lambda a, b: b if a is None else a


def mass(elements):
    return np.array([atomic_masses[e] for e in elements])

def vdw_radius(elements):
    return np.array([vdw_radii[e] for e in elements])

def center_of_mass(x,m):
    return np.sum(m[:,None] * x, axis=0) / m.sum()

def inertia_tensor(x,m):
    A = x[:,None,:] * x[:,:,None]
    B = np.trace(A,axis1=1,axis2=2)[:,None,None] * np.eye(3)[None,:,:]
    return np.sum(m[:,None,None]*(B - A), axis=0)

def dfs(graph, start, visited=None):
    "graph {id:set([neighbors...])}. start = id. Returns the cluster's ids."
    if visited is None:
        visited = set()
    visited.add(start)
    for next in graph[start] - visited:
        dfs(graph, next, visited)
    return visited

def distance(x,y=None,L=None):

    if x.ndim == 1:

        if y is None:
            s = x
        else:
            s = x - y

    else:

        if y is None:
            y = x

        s = x[None,...] - y[:,None,...]

    if L is not None:

        s -= L * np.rint(s/L)

    return np.linalg.norm(s,axis=-1)

def cluster_xyz(xyz, pdb, cutoff=1.9):
    "Cluster atoms into molecules with Depth-First-Search."

    # Small systems
    if xyz.n_atm < 10000:

        # Distance and Adjacency Matrix
        D = np.linalg.norm(xyz.all_atoms[None,:,:] - xyz.all_atoms[:,None,:],axis=-1)
        A = D < cutoff * (1 - np.eye(*D.shape))

        # Graph = {id : set(neighbours)}
        G = {i+1: set([j+1 for j in range(xyz.n_atm) if A[i,j]]) for i in range(xyz.n_atm)}

    # Loop to avoid memory fault
    else:

        print('Diane detected a system with > 10 000 atoms. Switching to iterative algorithm.')

        G = dict()
        p = 0

        print('Done 0 %')

        for i in range(xyz.n_atm):

            # Distance and Adjacency Matrix
            D = np.linalg.norm(xyz.all_atoms - xyz.atom(i),axis=-1)
            A = D < cutoff
            A[i] = False

            # Graph = {id : set(neighbours)}
            G[i+1] = set([j+1 for j in range(xyz.n_atm) if A[j]])

            if i % (xyz.n_atm//10) == 0:
                print('Done {} %'.format(p))
                p += 10

    # Depth-first search clustering
    visited = set()
    mol_id = 1
    atm_id = 1

    for i in range(xyz.n_atm):

        if i not in visited:

            c = dfs(G,i+1)
            visited = visited.union(c)

            # Set PDB attributes
            pdb.atm_in_mol[mol_id] = []
            for k in c:
                pdb.atoms[atm_id] = xyz.atom(k)
                pdb.elements[atm_id] = xyz.element(k)
                pdb.atm_in_mol[mol_id] += [atm_id]
                atm_id += 1

            pdb.molecules[mol_id] = np.vstack([xyz.atom(k) for k in c])
            pdb.mol_id += [mol_id]
            pdb.atm_id += pdb.atm_in_mol[mol_id]

            mol_id += 1



    # Make species names iterator
    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    names = product(alphabet,alphabet,alphabet)

    # If only one molecule, override
    if pdb.n_mol == 1:
        pdb.species[1] = ifnone(xyz.specie_name,''.join(next(names)))
    # Give same molecules the same specie name
    else:

        species = {}

        for i in pdb.mol_id:

            chemical_formula = pdb.molecule_formula(i)

            if chemical_formula in species:

                pdb.species[i] = species[chemical_formula]

            else:

                species[chemical_formula] = ''.join(next(names))

                pdb.species[i] = species[chemical_formula]






class AttrDict(dict):
    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self




class Structure:


    def __init__(self):

        self.file = ''
        self.atoms = {}
        self.elements = {}
        self.molecules = {}
        self.species = {}
        self.atm_id = []
        self.mol_id = []
        self.atm_in_mol = {}
        self.cell = np.zeros((3,3))
        self.header = str()

    def atom(self,i):
        if i in self.atm_id:
            return self.atoms[i]
        else:
            print(f'Cannot find atom ID {i}')

    def element(self,i):
        if i in self.atm_id:
            return self.elements[i]
        else:
            print(f'Cannot find atom ID {i}')

    def molecule(self,i):
        if i in self.mol_id:
            return self.molecules[i]
        else:
            print(f'Cannot find molecule ID {i}')

    def specie(self,i):
        if i in self.mol_id:
            return self.species[i]
        else:
            print(f'Cannot find molecule ID {i}')

    @property
    def chemical_formula(self):
        x = dict(Counter(self.all_elements))
        y = np.sort([''.join(str(i)+str(j)) for i,j in x.items()])
        return ' '.join(y.tolist())

    def molecule_formula(self,mol_id):
        x = dict(Counter(self.elements_in_molecule(mol_id)))
        y = np.sort([''.join(str(i)+str(j)) for i,j in x.items()])
        return ' '.join(y.tolist())

    @property
    def all_atoms(self): return np.array(list(self.atoms.values()))

    @property
    def all_molecules(self): return self.all_atoms

    @property
    def all_elements(self): return list(self.elements.values())

    @property
    def all_species(self): return list(self.species.values())

    @property
    def unique_elements(self):
        return set(self.elements.values())

    @property
    def unique_species(self):
        return set(self.species.values())

    @property
    def n_atm(self): return len(self.atm_id)

    @property
    def n_mol(self): return len(self.mol_id)

    @property
    def title(self):
        return 'AUTHOR Diane, {0}'.format(strftime("%a, %d %b %Y %X", localtime()))

    # def __isub__(self,x): return NotImplementedError()

    # def __len__(self): return len(self.atoms)

    # def __eq__(self,x): return x is self

    # def __lt__(self,other): return NotImplementedError()

    # def __gt__(self,other): return NotImplementedError()

    # def __le__(self,other): return NotImplementedError()

    # def __ge__(self,other): return NotImplementedError()














class PDB(Structure):
    """
    PDB - Class for managing PDB files and structures
    -------------------------------------------------

    Constructor
    -----------

    ### No arguments ###

        Construct an empty PDB class.
        Molecules can be added/deleted from the `add_molecule` / `delete_molecule` methods.

    ### One argument ###

        Can be either
            `filename` : str : Path to a PDB file to read
            `XYZ_structure` : XYZ : An XYZ instance that will be converted to PDB
            `PDB_structure` : PDB : A PDB instance that will be copied

    ### Two arguments ###

        `coordinates`
        `elements`


    Methods / Parameters
    --------------------

    `atoms` : dict : Dictionary of atom coordinates {atom_id: array([x,y,z])}
    `elements`: dict : Dictionary of elements {atom_id: element}
    `atm_id` : list : List of atom IDs

    `molecules` : dict : Dictionary of molecule coordinates {mol_id: array(coords)}
    `species` : dict : Dictionary of molecule's species {mol_id: specie_name}
    `mol_id` : list : List of molecule IDs

    `atm_in_mol` : dict : Dictionary of atom IDs of molecule {mol_id: [atm_ids_in_mol]}

    `all_atoms` : array : NumPy array of all coordinates
    `all_molecules` : array : Same as `all_atoms`
    `all_elements` : array : Array of all the elements in PDB
    `all_species` : array : Array of all the species in PDB

    `unique_elements` : array : Unique elements in PDB
    `unique_species` : array : Unique species in PDB

    `n_atm` : int : Total number of atoms
    `n_mol` : int : Total number of molecules

    `atoms_in_molecule(mol_id)`
    `elements_in_specie(spc)`
    `elements_in_molecule(mol_id)`
    `atoms_in_specie(spc)`

    `add_atom(mol_id,coordinates,element=None,specie='AAA')`
    `delete_atom(atm_id)`

    `add_molecule(coordinates,elements=None,specie='AAA')`
    `delete_molecule(mol_id)`

    `+` : merge
    `[i]` : return molecule(s) i
    `len(pdb)` : number of molecules

    """
    def __init__(self,*args,cluster=True,specie=None):

        super().__init__()

        # Empty initialization
        if len(args) == 0:
            pass
        elif len(args) == 1:
            args = args[0]

            # Input is already a PDB: do nothing
            if isinstance(args,PDB):
                self.__dict__.update(args.__dict__)
            # Input is a XYZ: conversion
            elif isinstance(args,XYZ):

                args.specie_name = ifnone(specie,args.specie_name)

                if cluster:
                    cluster_xyz(args,self)
                else:
                    self.mol_id += [1]
                    self.species[1] = ifnone(args.specie_name,'AAA')
                    self.molecules[1] = args.all_atoms
                    self.atm_in_mol[1] = []

                    for i,j in zip(range(1,args.n_atm+1),args.atm_id):
                        self.atm_id += [i]
                        self.atoms[i] = args.atoms[j]
                        self.elements[i] = args.elements[j]
                        self.atm_in_mol[1] += [i]

            # Input is a file: read
            elif isinstance(args,str):
                self.file = args
                name, ext = splitext(args.lower())
                if ext == '.pdb':
                    self.read(args)
                elif ext == '.xyz':
                    self.__init__(XYZ(args))
                else:
                    raise ValueError("Cannot read file {}: unknown extension.".format(args))
            else:
                raise ValueError("Invalid argument type: must be `XYZ`, `PDB` or `str`.")

        # Two arguments: coordinates, elements
        elif len(args) == 2:
            xyz = XYZ(*args)
            xyz.specie_name = ifnone(specie,xyz.specie_name)

            if cluster:
                cluster_xyz(xyz,self)
            else:
                self.mol_id += [1]
                self.species[1] = ifnone(xyz.specie_name,'AAA')
                self.molecules[1] = xyz.all_atoms
                self.atm_in_mol[1] = []

                for i,j in zip(range(1,xyz.n_atm+1),xyz.atm_id):
                    self.atm_id += [i]
                    self.atoms[i] = xyz.atoms[j]
                    self.elements[i] = xyz.elements[j]
                    self.atm_in_mol[1] += [i]

        # Raise error
        else:
            raise ValueError(f'Invalid number of arguments given to PDB: {len(args)}')

        # If initialization went fine, convert to arrays
        self.atoms = {i:np.array(self.atoms[i]) for i in self.atm_id}
        self.molecules = {i:np.array(self.molecules[i]) for i in self.mol_id}


    def read(self,file):
        "Import data from a PDB file"

        with open(file, 'r') as pdb:
            for line in pdb:

                split = line.split()
                if not split: continue

                if split[0] == 'ATOM' or split[0] == 'HETATM':

                    i = int(line[6:11])
                    s = re.sub(r'\s+', '', line[17:20]).upper()
                    j = int(line[22:26])
                    x = float(line[30:38])
                    y = float(line[38:46])
                    z = float(line[46:54])
                    e = re.findall(r'\s[a-zA-Z][a-z0-9\s]',line)[0]
                    e = re.sub(r'[0-9]+','',e.strip())

                    self.atm_id += [i]
                    self.atoms[i] = [x,y,z]
                    self.elements[i] = e

                    if j not in self.mol_id:
                        self.mol_id += [j]
                        self.species[j] = s
                        self.molecules[j] = [[x,y,z]]
                        self.atm_in_mol[j] = [i]
                    else:
                        self.molecules[j] += [[x,y,z]]
                        self.atm_in_mol[j] += [i]


                if split[0] == 'CRYST1':

                    a = float(split[1])
                    b = float(split[2])
                    c = float(split[3])
                    alpha = np.pi / 180 * float(split[4])
                    beta = np.pi / 180 * float(split[5])
                    gamma = np.pi / 180 * float(split[6])

                    x = [a, 0., 0.]
                    y = [b*np.cos(gamma), b*np.sin(gamma), 0.]
                    z1 = c*np.cos(beta)
                    z2 = (b*c*np.cos(alpha) - y[0]*z1)/y[1]
                    z3 = np.sqrt(c**2 - z1**2 - z2**2)
                    z = [z1,z2,z3]

                    self.cell = np.array([x,y,z])

                if split[0] == 'AUTHOR':
                    self.header += line



    def write(self,filename,header=None):
        "Write PDB file"

        title = ifnone(header,self.title)

        with open(filename,'w') as file:

            file.write(title+'\n')
            i = 0

            for mol_id in self.mol_id:
                for j,atm_id in enumerate(self.atm_in_mol[mol_id]):
                    i += 1
                    line = 'ATOM'
                    line += str(i).rjust(7)
                    line += self.elements[atm_id].rjust(3)
                    line += self.species[mol_id].rjust(6)
                    line += str(mol_id%10000).rjust(6)
                    line += '{:8.3f}'.format(self.molecules[mol_id][j,0]).rjust(12)
                    line += '{:8.3f}'.format(self.molecules[mol_id][j,1]).rjust(8)
                    line += '{:8.3f}'.format(self.molecules[mol_id][j,2]).rjust(8)
                    line += '1.00'.rjust(6)
                    line += '0.00'.rjust(6)
                    line += self.species[mol_id].rjust(9)
                    line += self.elements[atm_id].rjust(3)
                    line += '\n'
                    file.write(line)


    def add_atom(self,mol_id,coordinates,element=None,specie='AAA'):

        if len(coordinates) != 3 :
            raise ValueError(f'Cannot add atom: More than 3 coordinates are given:\
             {len(coordinates)}')

        # Add atom
        if len(self.atm_id) != 0:
            atm_id = np.max(self.atm_id) + 1
        else:
            atm_id = 1

        self.atm_id += [atm_id]
        self.atoms[atm_id] = np.array(coordinates)
        self.elements[atm_id] = element


        # Add to molecule
        self.atm_in_mol[mol_id] += [atm_id]

        if mol_id in self.mol_id:
            self.molecule[mol_id] = np.vstack([self.molecule[mol_id],np.array(coordinates)])
        else:
            self.mol_id += [mol_id]
            self.molecule[mol_id] = np.array(coordinates)
            self.specie[mol_id] = specie

        print(f'Added atom {atm_id} to molecule {mol_id}.')
        print(f'Molecule {mol_id} has now {len(self.molecule[mol_id])} atoms.')

        return self

    def delete_atom(self,atm_id):
        if atm_id not in self.atm_id:
            print(f'Cannot delete atom {atm_id}: ID not found')
        else:
            self.atm_id.remove(atm_id)
            del self.atoms[atm_id]
            del self.elements[atm_id]

            # Check molecule
            mol_id = next(i for i in self.mol_id if atm_id in self.atm_in_mol[i])
            atoms = [self.atoms[i] for i in self.atm_in_mol[mol_id]]
            if len(atoms) == 0:
                del self.molecules[mol_id]
                self.atm_id.remove(mol_id)
                print(f'Removed last atom {atm_id} of molecule {mol_id}. Deleted molecule.')
            else:
                self.molecules[mol_id] = np.vstack(atoms)
                print(f'Removed atom {atm_id} from molecule {mol_id}.')

        return self




    def add_molecule(self,coordinates,elements=None,specie='AAA',silent=False):

        if len(coordinates) != len(elements):
            raise ValueError(f'Cannot add molecule: Inconsistent shapes\
             of coordinates and elements {len(coordinates)} & {len(elements)}')

        if elements is None:
            elements = np.array(['X']*len(coordinates))
        else:
            elements = np.array(elements)

        # Add molecule
        if len(self.mol_id) != 0:
            mol_id = np.max(self.mol_id) + 1
        else:
            mol_id = 1

        self.molecules[mol_id] = np.array(coordinates)
        self.species[mol_id] = specie
        self.mol_id += [mol_id]


        # Add atoms
        if len(self.atm_id) != 0:
            start_id = np.max(self.atm_id) + 1
        else:
            start_id = 1

        self.atm_in_mol[mol_id] = []

        for i in range(len(coordinates)):
            atm_id = start_id + i
            self.atm_id += [atm_id]
            self.atm_in_mol[mol_id] += [atm_id]
            self.atoms[atm_id] = np.array(coordinates[i,:])
            self.elements[atm_id] = elements[i]


        if not silent:
            print(f'Added molecule {mol_id} of specie {specie} with {len(coordinates)} atoms.')

        return self

    def delete_molecule(self,mol_id,silent=False):
        if mol_id not in self.mol_id:
            print(f'Cannot delete molecule {mol_id}: ID not found')
        else:
            _spc = self.species[mol_id]
            _size = len(self.molecules[mol_id])

            self.mol_id.remove(mol_id)
            del self.molecules[mol_id]
            del self.species[mol_id]

            for atm_id in self.atm_in_mol[mol_id]:
                self.atm_id.remove(atm_id)
                del self.atoms[atm_id]
                del self.elements[atm_id]

        if not silent:
            print(f'Deleted molecule {mol_id} of specie {_spc} with {_size} atoms.')

        return self


    def elements_in_molecule(self,mol_id):
        return [self.elements[i] for i in self.atm_in_mol[mol_id]]

    def elements_in_specie(self,specie):
        mol_id = next(i for i in self.mol_id if self.specie[i] == specie)
        return self.elements_in_molecule(mol_id)

    def atoms_in_molecule(self,mol_id):
        return list(self.atm_in_mol[mol_id])

    def atoms_in_specie(self,specie):
        mol_id = next(i for i in self.mol_id if self.specie[i] == specie)
        return self.atoms_in_molecule(mol_id)

    @property
    def new(self):
        return PDB()

    @property
    def id(self):
        return self.mol_id

    def __str__(self):
        "Print by print()"

        string = '------------------------------------------\n'
        if self.file:
            string += f'Loaded file: {self.file}\n'
        string += f'  Number of atoms: {self.n_atm}\n'
        string += f'  Number of molecules: {self.n_mol}\n'

        string += '\n  Specie   Number    Size        Formula\n'
        string += '  --------------------------------------\n'

        counter = dict(Counter(self.all_species))

        for s, n in counter.items():
            size = next(len(self.atm_in_mol[i]) for i in self.atm_in_mol if self.specie(i) == s)
            mol_id = next(i for i in self.mol_id if self.specie(i) == s)
            string += '  {0:<3}{1:>12}{2:>8}{3:>15}\n'.format(s,n,size,self.molecule_formula(mol_id))

        string += '  --------------------------------------\n'

        if np.any(self.cell):
            string += '\n  Cell vectors:\n'
            string += '    {:.4f}  {:.4f}  {:.4f}\n'.format(*self.cell[0])
            string += '    {:.4f}  {:.4f}  {:.4f}\n'.format(*self.cell[1])
            string += '    {:.4f}  {:.4f}  {:.4f}\n'.format(*self.cell[2])
        string += '------------------------------------------'

        return string

    def __add__(self,x):
        "+ operator: merge"

        if x is self: print('Warning: Adding PDB to itself.')

        pdb = PDB(x)
        sum = PDB()

        n = 1

        # Add self to sum
        for i,j in zip(range(1,self.n_mol+1),self.mol_id):
            sum.mol_id += [i]
            sum.species[i] = self.species[j]
            sum.molecules[i] = self.molecules[j]

            for k,l in zip(range(n,len(self.atm_in_mol[j])+n),self.atm_in_mol[j]):
                sum.atm_id += [k]
                sum.atoms[k] = self.atoms[l]
                sum.elements[k] = self.elements[l]

            sum.atm_in_mol[i] = np.arange(n,len(self.molecules[j])+n)
            n += len(self.atm_in_mol[j])

        # Add pdb to sum
        for i,j in zip(range(self.n_mol+1,pdb.n_mol+self.n_mol+1),pdb.mol_id):

            sum.mol_id += [i]
            sum.species[i] = pdb.species[j]
            sum.molecules[i] = pdb.molecules[j]

            for k,l in zip(range(n,len(pdb.atm_in_mol[j])+n),pdb.atm_in_mol[j]):
                sum.atm_id += [k]
                sum.atoms[k] = pdb.atoms[l]
                sum.elements[k] = pdb.elements[l]

            sum.atm_in_mol[i] = np.arange(n,len(pdb.molecules[j])+n)
            n += len(pdb.atm_in_mol[j])

        return sum

    def __getitem__(self,i):
        "Return a copy of molecule i"
        pdb = PDB()

        if isinstance(i,int):
            i = [i]
        elif isintance(i,list):
            pass
        elif isinstance(i,slice):
            i = list(range(ifnone(i.start,0), ifnone(i.stop,self.n_mol+1), ifnone(i.step,1)))
        else:
            raise TypeError(f'PDB indices must be integers, lists or slices, not {type(i)}')

        n = 1

        for m,j in enumerate(i):
            mol_id = m+1
            pdb.mol_id += [mol_id]
            pdb.molecules[mol_id] = self.molecules[j]
            pdb.species[mol_id] = self.species[j]
            pdb.atm_in_mol[mol_id] = list(range(n,len(self.atm_in_mol[j])+1))
            n += len(self.atm_in_mol[j])

            for a,k in enumerate(self.atm_in_mol[j]):
                atm_id = a+1
                pdb.atm_id += [atm_id]
                pdb.atoms[atm_id] = self.atoms[k]
                pdb.elements[atm_id] = self.elements[k]

        return pdb


















class XYZ(Structure):
    """
    XYZ - Class for managing XYZ files and structures
    -------------------------------------------------

    Constructor
    -----------

    ### No arguments ###

        Construct an empty XYZ class.
        Atoms can be added/deleted from the `add_atom` / `delete_atom` methods.

    ### One argument ###

        Can be either
            `filename` : str : Path to a PDB file to read
            `XYZ_structure` : XYZ : An XYZ instance that will be copied
            `PDB_structure` : PDB : A PDB instance that will be converted to XYZ

    ### Two arguments ###

        `coordinates` : array / list : The array of xyz (float) positions
        `elements` : array / list : The array/list of (str) elements


    Methods / Parameters
    --------------------

    `atoms` : dict : Dictionary of atom coordinates {atom_id: array([x,y,z])}
    `elements`: dict : Dictionary of elements {atom_id: element}
    `atm_id` : list : List of atom IDs

    `all_atoms` : array : NumPy array of all coordinates
    `all_elements`

    `unique_elements`

    `n_atm`

    `add_atom(atm_id,element,xyz)`
    `delete_atom(atm_id)`

    """
    def __init__(self,*args,specie=None):

        super().__init__()

        self.specie_name = specie

        # Empty initialization
        if len(args) == 0:
            pass

        # One argument: file name, XYZ or PDB
        elif len(args) == 1:

            args = args[0]

            # Input is already a PDB: do nothing
            if isinstance(args,XYZ):
                self.__dict__.update(args.__dict__)
            # Input is a XYZ: conversion
            elif isinstance(args,PDB):
                self.atm_id = args.atm_id
                self.atoms = args.atoms
                self.elements = args.elements
            # Input is a file: read
            elif isinstance(args,str):
                self.file = args
                name, ext = splitext(args.lower())
                if ext == '.xyz':
                    self.read(args)
                elif ext == '.pdb':
                    self.__init__(PDB(args))
                else:
                    raise ValueError(f'Cannot read file {args}: unknown extension')
            else:
                raise ValueError('Invalid argument type: must be `XYZ`, `PDB` or `str`')

        # Two arguments: coordinates and elements
        elif len(args) == 2:
            atoms = args[0]
            elements = args[1]

            self.atm_id = list(range(1,len(atoms)+1))
            self.atoms = {i+1:np.array(j) for i,j in enumerate(atoms)}
            self.elements = {i+1:j for i,j in enumerate(elements)}

            n1 = len(self.atoms.keys())
            n2 = len(self.elements.keys())
            if n1 != n2 :
                raise ValueError(f'Inconsistent sizes of atoms and elements given to XYZ: {n1} {n2}')

        # Raise error
        else:
            raise ValueError(f'Invalid number of arguments given to XYZ: {len(args)}')

    def read(self,file):
        "Import data from an XYZ file"

        for i in range(3):
            try:
                all_atoms = np.loadtxt(file,skiprows=i,usecols=(1,2,3),dtype='float64')
                all_elements = np.loadtxt(file,skiprows=i,usecols=0,dtype=str)
                with open(file,'r') as f:
                    for j in range(i):
                        self.header += str(next(f))

                for j in range(len(all_atoms)):
                    self.atm_id += [j+1]
                    self.atoms[j+1] = all_atoms[j,:]
                    self.elements[j+1] = all_elements[j]
            except:
                if i == 2:
                    raise ValueError(f'Cannot read file: {file}')
                else:
                    continue



    def write(self,filename,header=None):
        "Write XYZ file"

        format = "{:>3}\t{:< 10.8f}\t{:< 10.8f}\t{:< 10.8f}\n"
        title = ifnone(header, self.title)

        with open(filename,'w') as file:
            file.write("{}\n{}\n".format(self.n_atm, title))

            for i in self.atm_id:
                file.write(format.format(self.element(i),*self.atom(i)))

    def add_atom(atm_id,element,xyz):

        if atm_id in self.atm_id:
            print(f'Cannot add atom {atm_id} to XYZ: ID already exists')
            i = 1
            while True:
                if i in self.atm_id:
                    continue
                else:
                    atm_id = i
                    break
            print(f'The ID has been changed to {atm_id}')

        self.atm_id += [atm_id]
        self.atoms[atm_id] = np.array(xyz)
        self.elements[atm_id] = element



    def delete_atom(atm_id):
        if atm_id not in self.atm_id:
            print(f'Cannot delete atom {atm_id}: ID not found')
        else:
            self.atm_id.remove(atm_id)
            del self.atoms[atm_id]
            del self.elements[atm_id]

    @property
    def new(self):
        return XYZ()

    @property
    def id(self):
        return self.atm_id


    def __str__(self):
        "Print by print()"

        string = '------------------------------------------\n'
        string += 'Loaded file: ' + str(self.file) + '\n'
        if self.header:
            string += '  Header: ' + self.header + '\n'
        string += f'  Number of atoms: {self.n_atm}\n'
        string += '  Elements: ' + ', '.join(self.unique_elements) + '\n'
        string += '------------------------------------------'
        return string



    def __add__(self,x):

        if x is self: print('Warning: Adding XYZ to itself.')

        xyz = XYZ(x)
        sum = XYZ()

        # Add self to sum
        for i,j in zip(range(1,self.n_atm+1),self.atm_id):
            sum.atm_id += [i]
            sum.atoms[i] = self.atoms[j]
            sum.elements[i] = self.elements[j]

        # Add xyz to sum
        for i,j in zip(range(self.n_atm+1,xyz.n_atm+self.n_atm+1),xyz.atm_id):
            sum.atm_id += [i]
            sum.atoms[i] = xyz.atoms[j]
            sum.elements[i] = xyz.elements[j]

        sum.header = self.header + xyz.header

        return sum


    def __getitem__(self,i):
        "Return copy of this part of the system"
        xyz = XYZ()

        if isinstance(i,int):
            i = [i]
        elif isinstance(i,list):
            pass
        elif isinstance(i,slice):
            i = list(range(ifnone(i.start,0), ifnone(i.stop,self.n_atm+1), ifnone(i.step,1)))
        else:
            raise TypeError(f'XYZ indices must be integers, lists or slices, not {type(i)}')

        for a,j in enumerate(i):
            atm_id = a+1
            xyz.atm_id += [atm_id]
            xyz.atoms[atm_id] = self.atoms[j]
            xyz.elements[atm_id] = self.elements[j]

        return xyz
